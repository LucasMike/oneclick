﻿using Npgsql;

namespace Projeto.Interfaces
{
    interface IDataReaderVisitor
    {
        void Visit(NpgsqlDataReader visitor);
    }
}
