﻿using Npgsql;
using System;
using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;
using static System.Convert;

namespace Projeto.Classes
{
    public class Sicredi : BancoBase
    {
        #region Variaveis Privadas

        protected string _sPosto = string.Empty;

        #endregion       

        #region Construtor

        public Sicredi(NpgsqlDataReader reader) : base(reader)
        {
            _sAgencia = reader.GetInt32(reader.GetOrdinal("agencia")).ToString();
            _sConta = reader.GetString(reader.GetOrdinal("conta"));
            _sCodCedente = reader.GetInt32(reader.GetOrdinal("cedente")).ToString();
            _sAgencia = reader.GetInt32(reader.GetOrdinal("agencia")).ToString();
            _sPosto = reader.GetString(reader.GetOrdinal("posto"));

            if (_sConta.Length != 6)
            {
                /*            MessageBox.Show(
                                "A conta '" + _sConta + "' é inválida. Os boletos bancários não serão gerados corretamente.",
                                "Informações Bancárias Inválidas"
                            );*/
            }

            //codigo do cedente
            if (_sCodCedente.Length != 5)
            {
                MessageBox.Show(
                    "O código do cedente '" + _sCodCedente + "' é inválido. Os boletos bancários não serão gerados corretamente.",
                    "Informações Bancárias Inválidas"
                );
            }

            //posto
            if (_sPosto.Length != 2)
            {
                MessageBox.Show(
                    "O posto '" + _sPosto + "' é inválido. Os boletos bancários não serão gerados corretamente.",
                    "Informações Bancárias Inválidas"
                );
            }
        }

        #endregion

        #region Metodos Publicos

        //Gera o próximo nosso numero conforme o Manual CNAB 400.
        public override string ProximoNossoNumero()
        {
            /**
             * FORMACAO DO NOSSO NUMERO
             * AA = Ano da Geração do Titulo
             * B = Geração do Nosso Numero (1-Cooperativa, 2a9 -Cedente)
             * NNNNN = Número Sequencial por Cedente
             * D = Digito Verificador, calculado pelo modulo 11
             */

            string sNossoNumero = _sUltimoNossoNumero.Replace("/", "");
            DateTime dtData = DateTime.Now;
            string sAno = dtData.ToString("yy");
            string sNumero = string.Empty;

            if (string.IsNullOrEmpty(sNossoNumero) || sAno != sNossoNumero.Substring(0, 2))
            {
                sNumero = "200000";
            }
            else
            {
                sNumero = sNossoNumero.Remove(0, 2);
            }

            _sUltimoNossoNumero = sAno + "/" + (ToInt32(sNumero) + 1).ToString("000000");
            return _sUltimoNossoNumero;
        }

        //Valida o numero da carteira.
        public int CodCarteira(string sCarteira)
        {
            switch (sCarteira)
            {
                case "A": return 1;
                default:
                    MessageBox.Show(
                       "A carteira " + sCarteira + " não está configurada no sistema.\n" +
                       "Verifique se a carteira informada está correta e após tenta novamente.",
                       "Carteira Inválida");
                    break;
            }
            return 0;
        }

        #endregion

        #region Metodos Gerar Remessa e impressão de boletos

        //Gerar arquivos de remessa unindo o retorno das funções ArquivoRemessa_GerarHeader, ArquivoRemessa_GerarDetalhe e ArquivoRemessa_GerarTrailler
        public override void GerarRemessa(params int[] idsContasGerar)
        {
            using (FolderBrowserDialog fbd = new FolderBrowserDialog())
            {
                if (fbd.ShowDialog() == DialogResult.OK)
                {
                    int iSequencial = 1;
                    StringBuilder strArquivo = new StringBuilder();

                    ArquivoRemessa_GerarHeader(strArquivo, ref iSequencial);

                    for (int i = 0; i < idsContasGerar.Length; ++i)
                    {
                        ArquivoRemessa_GerarDetalhe(strArquivo, ref iSequencial, idsContasGerar[i]);
                    }

                    ArquivoRemessa_GerarTrailler(strArquivo, ref iSequencial);

                    string sMes = (DateTime.Now.Month < 10 ? DateTime.Now.Month.ToString() : "OND"[DateTime.Now.Month - 10].ToString());
                    File.WriteAllText(Path.Combine(fbd.SelectedPath, $"{_sCodCedente}{sMes}{DateTime.Now.ToString("dd")}.crm"), strArquivo.ToString());
                }
            }
        }

        //Gera cabeçalho do arquivo de remessa.
        protected override void ArquivoRemessa_GerarHeader(StringBuilder strArquivo, ref int iSequencial)
        {
            string sLinha0 =
                AjustaValor(001, 001, TipoCampo.Texto, "0") + //Identificação do registro header "0"
                AjustaValor(002, 002, TipoCampo.Texto, "1") + //Identificação do arquivo remessa "1"
                AjustaValor(003, 009, TipoCampo.Texto, "REMESSA") + //Literal remessa "REMESSA"
                AjustaValor(010, 011, TipoCampo.Texto, "01") + //Código do serviço de cobrança "01"
                AjustaValor(012, 026, TipoCampo.Texto, "COBRANCA") + //Literal cobrança "COBRANCA"
                AjustaValor(027, 031, TipoCampo.Numero, _sCodCedente) + //Código do cedente
                AjustaValor(032, 045, TipoCampo.Numero, null) + //CIC/CGC do cedente
                AjustaValor(046, 076, TipoCampo.Texto, "") + //Filler Brancos
                AjustaValor(077, 079, TipoCampo.Texto, "748") + //Número do SICREDI "748"
                AjustaValor(080, 094, TipoCampo.Texto, "SICREDI") + //Literal "SICREDI"
                AjustaValor(095, 102, TipoCampo.Data, "yyyyMMdd", DateTime.Now) + //Data de gravação do arquivo	AAAAMMDD
                AjustaValor(103, 110, TipoCampo.Texto, "") + //Filler Brancos
                AjustaValor(111, 117, TipoCampo.Numero, ++_iUltimoSequencialRemessa) + //Número da remessa
                AjustaValor(118, 390, TipoCampo.Texto, "") + //Filler Brancos
                AjustaValor(391, 394, TipoCampo.Texto, "2.00") + //Versão do sistema "2.00" (o ponto deve ser colocado)
                AjustaValor(395, 400, TipoCampo.Numero, iSequencial); //Seqüencial do Registro: "000001"
            strArquivo.AppendLine(sLinha0);
            iSequencial++;
        }

        //Gera arquivo detalhe, que contem as informações dos boletos do arquivo de remessa.
        protected override void ArquivoRemessa_GerarDetalhe(StringBuilder strArquivo, ref int iSequencial, int idConta)
        {
            DataRow drDadosConta = ConsultarDadosConta(idConta).Rows[0];

            string sCarteira = "1";
            string sNossoNumero = ProximoNossoNumero();

            string sSeuNumero = idConta.ToString("0000000000");

            string sEspecie = "A"; //Duplicata Mercantil por Indicação

            string sLinha1 =
                AjustaValor(001, 001, TipoCampo.Texto, "1") + //Identificação do registro header "1"
                AjustaValor(002, 002, TipoCampo.Texto, "A") + //Tipo de cobrança "A" -SICREDI Com Registro
                AjustaValor(003, 003, TipoCampo.Texto, sCarteira) + //Tipo de carteira	"A" – Simples
                AjustaValor(004, 004, TipoCampo.Texto, "A") + //Tipo de Impressão "A" – Normal "B" – Carnê
                AjustaValor(005, 016, TipoCampo.Texto, "") + //Filler Brancos
                AjustaValor(017, 017, TipoCampo.Texto, "A") + //Tipo de moeda "A" – Real
                AjustaValor(018, 018, TipoCampo.Texto, "A") + //Tipo de desconto "A" – Valor	"B" – Percentual
                AjustaValor(019, 019, TipoCampo.Texto, "A") + //Tipo de juros "A" – Valor	"B" – Percentual
                AjustaValor(020, 047, TipoCampo.Texto, "") + //Filler Brancos
                AjustaValor(048, 056, TipoCampo.Texto, sNossoNumero) + //Nosso número SICREDI sem edição
                AjustaValor(057, 062, TipoCampo.Texto, "") + //FillerBrancos
                AjustaValor(063, 070, TipoCampo.Data, "yyyyMMdd", DateTime.Now) + //Data da Instrução AAAAMMDD
                AjustaValor(071, 071, TipoCampo.Texto, "") + //Campo alterado, quando instrução "31"
                AjustaValor(072, 072, TipoCampo.Texto, "N") + //Postagem do título
                AjustaValor(073, 073, TipoCampo.Texto, "") + //Filler Brancos
                AjustaValor(074, 074, TipoCampo.Texto, "B") + //Emissão do bloqueto "A" – Impressão pelo SICREDI "B" – Impressão pelo Cedente
                AjustaValor(075, 076, TipoCampo.Texto, "") + //Número da parcela do carnê
                AjustaValor(077, 078, TipoCampo.Texto, "") + //Número totalde parcelas do carnê
                AjustaValor(079, 082, TipoCampo.Texto, "") + //Filler Brancos
                AjustaValor(083, 092, TipoCampo.Numero, "n2", 0) + //Valor de desconto por dia de antecipação
                AjustaValor(093, 096, TipoCampo.Numero, "n2", 0) + //% multa por pagamento em atraso
                AjustaValor(097, 108, TipoCampo.Texto, "") + //Filler Brancos
                AjustaValor(109, 110, TipoCampo.Numero, 10) + //Instrução
                AjustaValor(111, 120, TipoCampo.Texto, sSeuNumero) + //Seu número
                AjustaValor(121, 126, TipoCampo.Data, "ddMMyy", drDadosConta["vencimento"]) + //Data de Vencimento
                AjustaValor(127, 139, TipoCampo.Numero, "n2", drDadosConta["valor"]) + //Valor do título
                AjustaValor(140, 148, TipoCampo.Texto, "") + //Filler Brancos
                AjustaValor(149, 149, TipoCampo.Texto, sEspecie) + //Espécie de documento	X -conf. tabela (pág. 10)
                AjustaValor(150, 150, TipoCampo.Texto, "N") + //Aceite do título "S" – sim "N" – não
                AjustaValor(151, 156, TipoCampo.Data, "ddMMyy", drDadosConta["emissao"]) + //Data Emissão do Título
                AjustaValor(157, 158, TipoCampo.Numero, "00") + //Instrução de protesto automático	"00" -Não protestar "06" -Protestar automaticamente
                AjustaValor(159, 160, TipoCampo.Numero, 0) + //Número de dias p/protesto automático. mínimo 03 (três) dias
                AjustaValor(161, 173, TipoCampo.Numero, "n2", 0) + //Valor/% de juros por dia de atraso
                AjustaValor(174, 179, TipoCampo.Texto, "000000") + //Data limite p/concessão de desconto	DDMMAA
                AjustaValor(180, 192, TipoCampo.Numero, "n2", 0) + //Valor/% do desconto
                AjustaValor(193, 205, TipoCampo.Numero, 0) + //Filler Zeros
                AjustaValor(206, 218, TipoCampo.Numero, "n2", 0) + //Valor do abatimento
                AjustaValor(219, 219, TipoCampo.Numero, 1) + //Tipo de pessoa do sacado: PF ou PJ "1" -Pessoa Física "2" -Pessoa Jurídica
                AjustaValor(220, 220, TipoCampo.Numero, 0) + //Filler Zeros
                AjustaValor(221, 234, TipoCampo.Texto, drDadosConta["cpf"]) + //CIC/CGC do sacado
                AjustaValor(235, 274, TipoCampo.Texto, drDadosConta["nome"]) + //Nome do sacado
                AjustaValor(275, 314, TipoCampo.Texto, drDadosConta["endereco"]) + //Endereço do sacado
                AjustaValor(315, 319, TipoCampo.Numero, 0) + //Código do sacado na cooperativa cedente
                AjustaValor(320, 325, TipoCampo.Numero, 0) + //Filler Zeros
                AjustaValor(326, 326, TipoCampo.Texto, "") + //Filler Brancos
                AjustaValor(327, 334, TipoCampo.Numero, drDadosConta["cep"]) + //CEP do sacado
                AjustaValor(335, 339, TipoCampo.Numero, 0) + //Código do sacado junto ao cliente (zeros quando inexistente)
                AjustaValor(340, 353, TipoCampo.Texto, "") + //CIC/CGC do sacador avalista
                AjustaValor(354, 394, TipoCampo.Texto, "") + //Nome do sacador avalista
                AjustaValor(395, 400, TipoCampo.Numero, iSequencial); //Número Seqüencial do Registro no Arquivo
            strArquivo.AppendLine(sLinha1);
            iSequencial++;

            Conexao.Instancia.ExecutarSQL($@"update conta_receber
                set boleto_id = '001', nosso_numero = '{sNossoNumero}'
                where id = {idConta}"
            );

            Conexao.Instancia.ExecutarSQL($"update boleto set ult_nosso_numero = '{sNossoNumero}'");
        }

        //Gera a ultima linha do arquivo de remessa.
        protected override void ArquivoRemessa_GerarTrailler(StringBuilder strArquivo, ref int iSequencial)
        {
            string sLinha9 =
                AjustaValor(001, 001, TipoCampo.Texto, "9") + //Identificação do registro trailer "9"
                AjustaValor(002, 002, TipoCampo.Texto, "1") + //Identificação do arquivo remessa	"1"
                AjustaValor(003, 005, TipoCampo.Texto, "748") + //Número do SICREDI "748"
                AjustaValor(006, 010, TipoCampo.Numero, _sCodCedente) + //Código do cedente
                AjustaValor(011, 394, TipoCampo.Texto, "") + //Filler Brancos
                AjustaValor(395, 400, TipoCampo.Numero, iSequencial); //Número seqüencialdo registro
            strArquivo.AppendLine(sLinha9);
            iSequencial++;
        }

        //Função que realiza a leitura do retorno dos boletos.
        public override void LerRetorno()
        {
            using (OpenFileDialog ofd = new OpenFileDialog { Title = "Selecione o(s) arquivo(s) de retorno", Multiselect = true })
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    foreach (string sArq in ofd.FileNames)
                    {
                        using (StreamReader sr = new StreamReader(sArq))
                        {
                            while (!sr.EndOfStream)
                            {
                                string sLinha = sr.ReadLine();
                                if (sLinha.StartsWith("1"))
                                {
                                    string sOcorrencia = sLinha.Substring(108, 2);
                                    string sMotivo = sLinha.Substring(318, 10).Trim();
                                    string sNossoNumero = sLinha.Substring(47, 15).Trim();

                                    if (sOcorrencia == "06" || sOcorrencia == "15") // Baixa
                                    {
                                        Conexao.Instancia.ExecutarSQL($@"update conta_receber set status = 'F', boleto = 'Liquidação normal' where nosso_numero = '{sNossoNumero}'");
                                    }
                                    else if (sOcorrencia == "02") // Entrada confirmada
                                    {
                                        Conexao.Instancia.ExecutarSQL($@"update conta_receber set boleto = 'Entrada Confirmada' where nosso_numero = '{sNossoNumero}'");
                                    }
                                    else
                                    {
                                        MessageBox.Show(
                                            $@"Verificar com banco o boleto {sNossoNumero} que não foi registrado pelo notivo {sMotivo}.",
                                            "Retorno boletos",
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Information
                                        );
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //Gera o código de barras do boleto bancário.
        public override string Impressao_CodigoDeBarras(string sAgencia, string sNossoNumero, string sConta, decimal dValor, DateTime dtVencimento, DataRow drBolImp)
        {
            //campo livre
            string sCampoLivre = string.Empty;
            string s20a23 = sAgencia.PadLeft(4, '0'); //Agência Cedente (Sem o digito verificador, completar com zeros a esquerda quando necessário)
            string s24a25 = "01"; //Carteira
            string s26a36 = sNossoNumero.Replace("/", "").PadLeft(11, '0'); //Número do Nosso Número(Sem o digito verificador)
            string s37a43 = sConta.PadLeft(7, '0'); //Conta do Cedente (Sem o digito verificador, completar com zeros a esquerda quando necessário)
            string s44a44 = "0"; //Zero
            sCampoLivre = s20a23 + s24a25 + s26a36 + s37a43 + s44a44;

            string s01a03 = "748"; //Identificação do Banco
            string s04a04 = "9"; //Código da Moeda (Real = 9, Outras=0)
            string s06a09 = FatorVencimento(dtVencimento).ToString("0000"); //Fator de Vencimento (Vide Nota)
            string s10a19 = dValor.ToString("0.00").Replace(",", "").PadLeft(10, '0'); //Valor
            string s05a05 = DvCodBarras(s01a03 + s04a04 + s06a09 + s10a19 + sCampoLivre); //Dígito verificador do Código de Barras

            return s01a03 + s04a04 + s05a05 + s06a09 + s10a19 + sCampoLivre;
        }

        //Gera a linha digitavel do boleto bancário
        public override string Impressao_LinhaDigitavel(string sCodBarras)
        {
            //Composto pelo código de Banco, código da moeda, as cinco primeiras posições do campo livre e o dígito verificador deste campo;
            string sCampo1 = string.Empty;
            sCampo1 += sCodBarras.Substring(0, 4);
            sCampo1 += sCodBarras.Substring(19, 5);
            sCampo1 += DvMod10(sCampo1);
            sCampo1 = sCampo1.Insert(5, ".");

            //Composto pelas posições 6ª a 15ª do campo livre e o dígito verificador deste campo;
            string sCampo2 = string.Empty;
            sCampo2 += sCodBarras.Substring(24, 10);
            sCampo2 += DvMod10(sCampo2);
            sCampo2 = sCampo2.Insert(5, ".");

            //Composto pelas posições 16ª a 25ª do campo livre e o dígito verificador deste campo
            string sCampo3 = string.Empty;
            sCampo3 += sCodBarras.Substring(34, 10);
            sCampo3 += DvMod10(sCampo3);
            sCampo3 = sCampo3.Insert(5, ".");

            //Composto pelo dígito verificador do código de barras, ou seja, a 5ª posição do código de barras;
            string sCampo4 = string.Empty;
            sCampo4 += sCodBarras.Substring(4, 1);

            //Composto pelo fator de vencimento com 4(quatro) caracteres e o valor do documento com 10(dez) caracteres, sem separadores e sem edição
            string sCampo5 = string.Empty;
            sCampo5 += sCodBarras.Substring(5, 4);
            sCampo5 += sCodBarras.Substring(9, 10);

            return sCampo1 + " " + sCampo2 + " " + sCampo3 + " " + sCampo4 + " " + sCampo5;
        }

        #endregion
    }
}