using Npgsql;
using System;
using System.Data;
using System.Text;

namespace Projeto.Classes
{
    public enum TipoCampo { Texto, Numero, Data, Hora }

    public enum Bancos : ushort
    {
        Sicredi = 748,
        Outros = ushort.MaxValue
    }

    public abstract class BancoBase
    {
        #region Variaveis Privadas

        protected string _sCodigo = string.Empty;
        protected string _sNome = string.Empty;
        protected string _sAgencia = string.Empty;
        protected string _sConta = string.Empty;
        protected string _sCodCedente = string.Empty;

        protected string _sUltimoNossoNumero = string.Empty;
        protected int _iUltimoSequencialRemessa = 0;

        #endregion

        #region Construtor

        protected BancoBase(NpgsqlDataReader reader)
        {
            _sUltimoNossoNumero = reader.GetString(reader.GetOrdinal("ult_nosso_numero"));
            _iUltimoSequencialRemessa = reader.GetInt32(reader.GetOrdinal("sequencia_arquivo"));
        }

        #endregion

        #region Metodos Privados

        /// <summary>
        /// Calcular o Fator de Vencimento do Boleto
        /// </summary>
        /// <param name="dateVencimento">Data do Vencimento</param>
        /// <returns></returns>
        protected virtual int FatorVencimento(DateTime dateVencimento)
        {
            return dateVencimento.Subtract(new DateTime(1997, 10, 7)).Days;
        }

        #endregion

        #region Metodos Geracao Boleto

        /// <summary>
        /// Gerar o Próximo Nosso Número
        /// </summary>
        public abstract string ProximoNossoNumero();

        #endregion

        #region Metodos Gerar Remessa        

        protected abstract void ArquivoRemessa_GerarHeader(StringBuilder strArquivo, ref int iSequencial);
        protected abstract void ArquivoRemessa_GerarDetalhe(StringBuilder strArquivo, ref int iSequencial, int idConta);
        protected abstract void ArquivoRemessa_GerarTrailler(StringBuilder strArquivo, ref int iSequencial);

        public abstract string Impressao_LinhaDigitavel(string sCodBarras);
        public abstract string Impressao_CodigoDeBarras(string sAgencia, string sNossoNumero, string sConta, decimal dValor, DateTime dtVencimento, DataRow drBolImp);

        protected virtual string AjustaValor(TipoCampo aTipoCampo, int iTamanho, object oValor)
        {
            return AjustaValor(aTipoCampo, iTamanho, "", oValor);
        }
        protected virtual string AjustaValor(TipoCampo aTipoCampo, int iTamanho, string sFormato, object oValor)
        {
            if (aTipoCampo == TipoCampo.Texto)
            {
                string sTexto = oValor.ToString();
                return (sTexto.Length > iTamanho ? sTexto.Substring(0, iTamanho) : sTexto.PadRight(iTamanho, ' '));
            }

            if (aTipoCampo == TipoCampo.Data)
            {
                DateTime dtData = Convert.ToDateTime(oValor);
                return dtData.ToString(sFormato.Length == 0 ? "ddMMyy" : sFormato).PadLeft(iTamanho, ' ');
            }

            if (aTipoCampo == TipoCampo.Numero)
            {
                decimal dNumero = Convert.ToDecimal(oValor);
                return dNumero.ToString(sFormato.Length == 0 ? "" : sFormato).Replace(",", "").Replace(".", "").PadLeft(iTamanho, '0');
            }

            return new string(' ', iTamanho);
        }

        protected virtual string AjustaValor(int iPosDe, int iPosAte, TipoCampo aTipoCampo, object oValor)
        {
            return AjustaValor(iPosDe, iPosAte, aTipoCampo, "", oValor);
        }
        protected virtual string AjustaValor(int iPosDe, int iPosAte, TipoCampo aTipoCampo, string sFormato, object oValor)
        {
            int iTamanho = iPosAte - iPosDe + 1;

            if (oValor != null)
            {
                if (aTipoCampo == TipoCampo.Texto)
                {
                    string sTexto = oValor.ToString();
                    return (sTexto.Length > iTamanho ? sTexto.Substring(0, iTamanho) : sTexto.PadRight(iTamanho, ' '));
                }

                if (aTipoCampo == TipoCampo.Data)
                {
                    DateTime dtData = Convert.ToDateTime(oValor);
                    return dtData.ToString(sFormato.Length == 0 ? "ddMMyy" : sFormato).PadLeft(iTamanho, ' ');
                }

                if (aTipoCampo == TipoCampo.Hora)
                {
                    DateTime dtData = Convert.ToDateTime(oValor);
                    return dtData.ToString(sFormato.Length == 0 ? "HHmmss" : sFormato).PadLeft(iTamanho, ' ');
                }

                if (aTipoCampo == TipoCampo.Numero)
                {
                    decimal dNumero = Convert.ToDecimal(Convert.ToString(oValor).Replace("-", ""));
                    return dNumero.ToString(sFormato.Length == 0 ? "" : sFormato).Replace(",", "").Replace(".", "").Replace("-", "").PadLeft(iTamanho, '0');
                }
            }

            return new string(' ', iTamanho);
        }

        #endregion


        #region Métodos Privados

        protected DataTable ConsultarDadosConta(int id)
        {
            return Conexao.Instancia.Consultar($@"select r.*, a.*, c.cep 
                from conta_receber r
                inner join associado a on (a.id = r.associado_id)
                left join cidade c on (c.id = a.cidade_id)
                where r.id = {id}"
            );
        }

        protected string DvMod10(string sSequencia)
        {
            int iPesoDe = 2;
            int iPesoAte = 1;

            int iSoma = DvMod10_Soma(sSequencia, iPesoDe, iPesoAte);
            int iResto = (iSoma < 10 ? iSoma : (iSoma % 10));

            if (iResto > 0)
            {
                return (10 - iResto).ToString();
            }
            else
            {
                return "0";
            }
        }

        protected virtual int DvMod10_Soma(string sSequencia, int iPesoDe, int iPesoAte)
        {
            int iPesoDvMod10de = iPesoDe;
            int iPesoDvMod10ate = iPesoAte;
            int iPeso = iPesoDvMod10de;
            int iSoma = 0;

            for (int i = sSequencia.Length - 1; i >= 0; i--)
            {
                int iNum = Convert.ToInt32(sSequencia[i].ToString());
                int iRes = (iNum * iPeso);
                if (iRes > 9)
                {
                    iRes = iRes - 9;
                }

                iSoma += iRes;

                if ((iPesoDe > iPesoAte && iPeso < iPesoDe) || (iPesoDe < iPesoAte && iPeso > iPesoAte - 1))
                {
                    iPeso = iPesoDvMod10de;
                }
                else
                {
                    iPeso = (iPesoDe > iPesoAte ? iPeso - 1 : iPeso + 1);
                }
            }
            return iSoma;
        }

        protected string DvCodBarras(string sCodBarras)
        {
            int iSoma = DvMod11_Soma(sCodBarras, 2, 9);
            int iResto = iSoma % 11;
            int iSubtracao = 11 - iResto;

            if (iSubtracao > 0 && iSubtracao < 10)
            {
                return iSubtracao.ToString();
            }
            else
            {
                return 1.ToString();
            }
        }

        protected virtual int DvMod11_Soma(string sSequencia, int iPesoDe, int iPesoAte)
        {
            int iPesoDvMod11de = iPesoDe;
            int iPesoDvMod11ate = iPesoAte;
            int iPeso = iPesoDe;
            int iSoma = 0;

            for (int i = sSequencia.Length - 1; i >= 0; i--)
            {
                char cNum = sSequencia[i];
                if (cNum != ' ')
                {
                    int iNum = Convert.ToInt32(cNum.ToString());
                    int iRes = (iNum * iPeso);
                    iSoma += iRes;
                }
                if ((iPesoDe > iPesoAte && iPeso <= iPesoAte) || (iPesoDe < iPesoAte && iPeso > iPesoAte - 1))
                {
                    iPeso = iPesoDvMod11de;
                }
                else
                {
                    iPeso = (iPesoDe > iPesoAte ? iPeso - 1 : iPeso + 1);
                }
            }
            return iSoma;
        }

        #endregion

        public abstract void GerarRemessa(params int[] ids);

        public abstract void LerRetorno();

        public static BancoBase ObterBancoBase(int idConfig)
        {
            using (NpgsqlDataReader reader = Conexao.Instancia.Consultar(idConfig, "boleto"))
            {
                while (reader.Read())
                {
                    if (reader.GetInt32(reader.GetOrdinal("num_banco")) == 748) // Sicredi
                    {
                        return new Sicredi(reader);
                    }
                }
            }

            return null;
        }
    }
}