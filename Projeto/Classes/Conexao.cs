﻿using Npgsql;
using Config = Projeto.Properties.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Projeto.Classes
{
    internal class Conexao
    {
        #region Variaveis Publicas

        public static Conexao Instancia
        {
            get
            {
                lock (_sync)
                {
                    if (_instancia == null)
                    {
                        _instancia = new Conexao();
                    }

                    return _instancia;
                }
            }
        }

        #endregion

        #region Variaveis Estaticas

        private static object _sync = new object();
        private static Conexao _instancia = null;

        #endregion

        #region Variaveis Privadas

        private readonly NpgsqlConnection _connection;

        #endregion


        #region Construtor

        private Conexao()
        {
            _connection = new NpgsqlConnection(
                $"Host={Config.Default.DB_IP};Port={Config.Default.DB_PORTA};Database={Config.Default.DB_BANCO};Username={Config.Default.DB_USER};Password={Config.Default.DB_SENHA}"
            );
            _connection.Open();
        }

        #endregion

        #region Métodos Publicos


        /// <summary>
        /// Consulta um regsitro no banco de dados
        /// </summary>
        /// <param name="id">O id para consultar</param>
        /// <param name="tabela">A tabela para cosultar</param>
        /// <returns>Data reader para ler os dados cnsultados</returns>
        public NpgsqlDataReader Consultar(int id, string tabela)
        {
            NpgsqlCommand comando = _connection.CreateCommand();
            comando.CommandText = $"select * from {tabela} where id = {id}";
            comando.CommandType = CommandType.Text;

            return comando.ExecuteReader();
        }

        public DataTable Consultar(string sql)
        {
            DataTable resultado = new DataTable();
            NpgsqlCommand comando = _connection.CreateCommand();
            comando.CommandText = sql;
            comando.CommandType = CommandType.Text;

            using (NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter(comando))
            {
                dataAdapter.Fill(resultado);
                return resultado;
            }
        }

        public int ExecutarSQL(string sql)
        {
            NpgsqlCommand comando = _connection.CreateCommand();
            comando.CommandText = sql;
            comando.CommandType = CommandType.Text;

            return comando.ExecuteNonQuery();
        }

        #endregion
    }
}
