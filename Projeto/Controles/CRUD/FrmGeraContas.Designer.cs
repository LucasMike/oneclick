﻿namespace Projeto.Controles.CRUD
{
    partial class FrmGeraContas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmGeraContas));
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.FormLayoutControlBase = new DevExpress.XtraLayout.LayoutControl();
            this.teDataInicio = new DevExpress.XtraEditors.DateEdit();
            this.teDataFim = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btiSalvar = new DevExpress.XtraBars.BarButtonItem();
            this.btiCancelar = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.FormLayoutControlBase)).BeginInit();
            this.FormLayoutControlBase.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teDataInicio.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDataInicio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDataFim.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDataFim.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // bar1
            // 
            this.bar1.BarName = "Status bar";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Status bar";
            // 
            // FormLayoutControlBase
            // 
            this.FormLayoutControlBase.Controls.Add(this.teDataInicio);
            this.FormLayoutControlBase.Controls.Add(this.teDataFim);
            this.FormLayoutControlBase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FormLayoutControlBase.Location = new System.Drawing.Point(0, 0);
            this.FormLayoutControlBase.Name = "FormLayoutControlBase";
            this.FormLayoutControlBase.Root = this.layoutControlGroup1;
            this.FormLayoutControlBase.Size = new System.Drawing.Size(275, 71);
            this.FormLayoutControlBase.TabIndex = 5;
            this.FormLayoutControlBase.Text = "layoutControl1";
            // 
            // teDataInicio
            // 
            this.teDataInicio.EditValue = null;
            this.teDataInicio.Location = new System.Drawing.Point(66, 12);
            this.teDataInicio.Name = "teDataInicio";
            this.teDataInicio.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.teDataInicio.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.teDataInicio.Properties.EditFormat.FormatString = "";
            this.teDataInicio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.teDataInicio.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.teDataInicio.Size = new System.Drawing.Size(197, 20);
            this.teDataInicio.StyleController = this.FormLayoutControlBase;
            this.teDataInicio.TabIndex = 2;
            // 
            // teDataFim
            // 
            this.teDataFim.EditValue = null;
            this.teDataFim.Location = new System.Drawing.Point(66, 36);
            this.teDataFim.Name = "teDataFim";
            this.teDataFim.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.teDataFim.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.teDataFim.Properties.EditFormat.FormatString = "";
            this.teDataFim.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.teDataFim.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.teDataFim.Size = new System.Drawing.Size(197, 20);
            this.teDataFim.StyleController = this.FormLayoutControlBase;
            this.teDataFim.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem6});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(275, 71);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.teDataInicio;
            this.layoutControlItem4.CustomizationFormText = "Data Inicio";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(255, 24);
            this.layoutControlItem4.Text = "Data Inicio";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.teDataFim;
            this.layoutControlItem6.CustomizationFormText = "Data fim";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(255, 27);
            this.layoutControlItem6.Text = "Data fim";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(51, 13);
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btiSalvar,
            this.btiCancelar});
            this.barManager1.MaxItemId = 2;
            this.barManager1.StatusBar = this.bar2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Status bar";
            this.bar2.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btiSalvar, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btiCancelar, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Status bar";
            // 
            // btiSalvar
            // 
            this.btiSalvar.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btiSalvar.Caption = "Salvar";
            this.btiSalvar.Id = 0;
            this.btiSalvar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btiSalvar.ImageOptions.Image")));
            this.btiSalvar.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btiSalvar.ImageOptions.LargeImage")));
            this.btiSalvar.Name = "btiSalvar";
            this.btiSalvar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btiSalvar_ItemClick);
            // 
            // btiCancelar
            // 
            this.btiCancelar.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btiCancelar.Caption = "Cancelar";
            this.btiCancelar.Id = 1;
            this.btiCancelar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btiCancelar.ImageOptions.Image")));
            this.btiCancelar.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btiCancelar.ImageOptions.LargeImage")));
            this.btiCancelar.Name = "btiCancelar";
            this.btiCancelar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btiCancelar_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(275, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 71);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(275, 27);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 71);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(275, 0);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 71);
            // 
            // FrmGeraContas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(275, 98);
            this.Controls.Add(this.FormLayoutControlBase);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "FrmGeraContas";
            this.Text = "Gerar Contas";
            ((System.ComponentModel.ISupportInitialize)(this.FormLayoutControlBase)).EndInit();
            this.FormLayoutControlBase.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.teDataInicio.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDataInicio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDataFim.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDataFim.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.Bar bar1;
        protected DevExpress.XtraLayout.LayoutControl FormLayoutControlBase;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btiSalvar;
        private DevExpress.XtraBars.BarButtonItem btiCancelar;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.DateEdit teDataInicio;
        private DevExpress.XtraEditors.DateEdit teDataFim;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
    }
}