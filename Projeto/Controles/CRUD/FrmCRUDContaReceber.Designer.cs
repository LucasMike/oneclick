﻿namespace Projeto.Controles.CRUD
{
    partial class FrmCRUDContaReceber
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ceValor = new DevExpress.XtraEditors.CalcEdit();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lkuAssociado = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.teVencimento = new DevExpress.XtraEditors.DateEdit();
            ((System.ComponentModel.ISupportInitialize)(this.FormLayoutControlBase)).BeginInit();
            this.FormLayoutControlBase.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceValor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkuAssociado.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teVencimento.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teVencimento.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // FormLayoutControlBase
            // 
            this.FormLayoutControlBase.Controls.Add(this.ceValor);
            this.FormLayoutControlBase.Controls.Add(this.teVencimento);
            this.FormLayoutControlBase.Controls.Add(this.lkuAssociado);
            this.FormLayoutControlBase.Root = this.layoutControlGroup1;
            this.FormLayoutControlBase.Size = new System.Drawing.Size(290, 94);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem8});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(290, 94);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.ceValor;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(270, 24);
            this.layoutControlItem1.Text = "Valor";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(55, 13);
            // 
            // ceValor
            // 
            this.ceValor.Location = new System.Drawing.Point(70, 36);
            this.ceValor.Name = "ceValor";
            this.ceValor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ceValor.Properties.DisplayFormat.FormatString = "c2";
            this.ceValor.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.ceValor.Properties.EditFormat.FormatString = "c2";
            this.ceValor.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.ceValor.Properties.Mask.EditMask = "c2";
            this.ceValor.Properties.Precision = 2;
            this.ceValor.Size = new System.Drawing.Size(208, 20);
            this.ceValor.StyleController = this.FormLayoutControlBase;
            this.ceValor.TabIndex = 2;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.lkuAssociado;
            this.layoutControlItem2.CustomizationFormText = "Universidade";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(270, 24);
            this.layoutControlItem2.Text = "Associado";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(55, 13);
            // 
            // lkuAssociado
            // 
            this.lkuAssociado.Location = new System.Drawing.Point(70, 12);
            this.lkuAssociado.Name = "lkuAssociado";
            this.lkuAssociado.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkuAssociado.Properties.DisplayMember = "nome";
            this.lkuAssociado.Properties.KeyMember = "id";
            this.lkuAssociado.Properties.NullText = "";
            this.lkuAssociado.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.lkuAssociado.Properties.ValueMember = "id";
            this.lkuAssociado.Size = new System.Drawing.Size(208, 20);
            this.lkuAssociado.StyleController = this.FormLayoutControlBase;
            this.lkuAssociado.TabIndex = 0;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.teVencimento;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(270, 26);
            this.layoutControlItem8.Text = "Vencimento";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(55, 13);
            // 
            // teVencimento
            // 
            this.teVencimento.EditValue = null;
            this.teVencimento.Location = new System.Drawing.Point(70, 60);
            this.teVencimento.Name = "teVencimento";
            this.teVencimento.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.teVencimento.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.teVencimento.Properties.EditFormat.FormatString = "";
            this.teVencimento.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.teVencimento.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.teVencimento.Size = new System.Drawing.Size(208, 20);
            this.teVencimento.StyleController = this.FormLayoutControlBase;
            this.teVencimento.TabIndex = 0;
            // 
            // FrmCRUDContaReceber
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(290, 121);
            this.Location = new System.Drawing.Point(500, 100);
            this.Name = "FrmCRUDContaReceber";
            this.Text = "Conta a Receber";
            ((System.ComponentModel.ISupportInitialize)(this.FormLayoutControlBase)).EndInit();
            this.FormLayoutControlBase.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceValor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkuAssociado.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teVencimento.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teVencimento.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.DateEdit teVencimento;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.CalcEdit ceValor;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.LookUpEdit lkuAssociado;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
    }
}