﻿namespace Projeto.Controles.CRUD
{
    partial class FrmCRUDBoleto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.teNumBanco = new DevExpress.XtraEditors.TextEdit();
            this.teAgencia = new DevExpress.XtraEditors.TextEdit();
            this.teBanco = new DevExpress.XtraEditors.TextEdit();
            this.teCedente = new DevExpress.XtraEditors.TextEdit();
            this.teCarteira = new DevExpress.XtraEditors.TextEdit();
            this.teLayout = new DevExpress.XtraEditors.TextEdit();
            this.teAceite = new DevExpress.XtraEditors.TextEdit();
            this.teSequencia = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.teConta = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tePosto = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.teNossoNumero = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.FormLayoutControlBase)).BeginInit();
            this.FormLayoutControlBase.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teNumBanco.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teAgencia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teBanco.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teCedente.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teCarteira.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teLayout.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teAceite.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teSequencia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConta.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tePosto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teNossoNumero.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // FormLayoutControlBase
            // 
            this.FormLayoutControlBase.Controls.Add(this.teNumBanco);
            this.FormLayoutControlBase.Controls.Add(this.teAgencia);
            this.FormLayoutControlBase.Controls.Add(this.teBanco);
            this.FormLayoutControlBase.Controls.Add(this.teCedente);
            this.FormLayoutControlBase.Controls.Add(this.teCarteira);
            this.FormLayoutControlBase.Controls.Add(this.teLayout);
            this.FormLayoutControlBase.Controls.Add(this.teAceite);
            this.FormLayoutControlBase.Controls.Add(this.teSequencia);
            this.FormLayoutControlBase.Controls.Add(this.tePosto);
            this.FormLayoutControlBase.Controls.Add(this.teNossoNumero);
            this.FormLayoutControlBase.Controls.Add(this.teConta);
            this.FormLayoutControlBase.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(377, 179, 450, 400);
            this.FormLayoutControlBase.Root = this.layoutControlGroup1;
            this.FormLayoutControlBase.Size = new System.Drawing.Size(354, 287);
            // 
            // teNumBanco
            // 
            this.teNumBanco.Location = new System.Drawing.Point(84, 12);
            this.teNumBanco.Name = "teNumBanco";
            this.teNumBanco.Size = new System.Drawing.Size(258, 20);
            this.teNumBanco.StyleController = this.FormLayoutControlBase;
            this.teNumBanco.TabIndex = 0;
            // 
            // teAgencia
            // 
            this.teAgencia.Location = new System.Drawing.Point(84, 36);
            this.teAgencia.Name = "teAgencia";
            this.teAgencia.Size = new System.Drawing.Size(258, 20);
            this.teAgencia.StyleController = this.FormLayoutControlBase;
            this.teAgencia.TabIndex = 2;
            // 
            // teBanco
            // 
            this.teBanco.Location = new System.Drawing.Point(84, 84);
            this.teBanco.Name = "teBanco";
            this.teBanco.Size = new System.Drawing.Size(258, 20);
            this.teBanco.StyleController = this.FormLayoutControlBase;
            this.teBanco.TabIndex = 4;
            // 
            // teCedente
            // 
            this.teCedente.Location = new System.Drawing.Point(84, 132);
            this.teCedente.Name = "teCedente";
            this.teCedente.Size = new System.Drawing.Size(258, 20);
            this.teCedente.StyleController = this.FormLayoutControlBase;
            this.teCedente.TabIndex = 6;
            // 
            // teCarteira
            // 
            this.teCarteira.Location = new System.Drawing.Point(84, 156);
            this.teCarteira.Name = "teCarteira";
            this.teCarteira.Size = new System.Drawing.Size(258, 20);
            this.teCarteira.StyleController = this.FormLayoutControlBase;
            this.teCarteira.TabIndex = 7;
            // 
            // teLayout
            // 
            this.teLayout.Location = new System.Drawing.Point(84, 180);
            this.teLayout.Name = "teLayout";
            this.teLayout.Size = new System.Drawing.Size(258, 20);
            this.teLayout.StyleController = this.FormLayoutControlBase;
            this.teLayout.TabIndex = 8;
            // 
            // teAceite
            // 
            this.teAceite.Location = new System.Drawing.Point(84, 204);
            this.teAceite.Name = "teAceite";
            this.teAceite.Size = new System.Drawing.Size(258, 20);
            this.teAceite.StyleController = this.FormLayoutControlBase;
            this.teAceite.TabIndex = 9;
            // 
            // teSequencia
            // 
            this.teSequencia.Location = new System.Drawing.Point(84, 252);
            this.teSequencia.Name = "teSequencia";
            this.teSequencia.Size = new System.Drawing.Size(258, 20);
            this.teSequencia.StyleController = this.FormLayoutControlBase;
            this.teSequencia.TabIndex = 11;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem11,
            this.layoutControlItem1,
            this.layoutControlItem10});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(354, 287);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.teNumBanco;
            this.layoutControlItem2.CustomizationFormText = "Código";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(334, 24);
            this.layoutControlItem2.StartNewLine = true;
            this.layoutControlItem2.Text = "Num. Banco";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(69, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.teAgencia;
            this.layoutControlItem3.CustomizationFormText = "Código";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(334, 24);
            this.layoutControlItem3.StartNewLine = true;
            this.layoutControlItem3.Text = "Agencia";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(69, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.teBanco;
            this.layoutControlItem4.CustomizationFormText = "Código";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(334, 24);
            this.layoutControlItem4.StartNewLine = true;
            this.layoutControlItem4.Text = "Banco";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(69, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.teCedente;
            this.layoutControlItem5.CustomizationFormText = "Código";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(334, 24);
            this.layoutControlItem5.StartNewLine = true;
            this.layoutControlItem5.Text = "Cód. Cedente";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(69, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.teCarteira;
            this.layoutControlItem6.CustomizationFormText = "Código";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(334, 24);
            this.layoutControlItem6.StartNewLine = true;
            this.layoutControlItem6.Text = "Carteira";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(69, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.teLayout;
            this.layoutControlItem7.CustomizationFormText = "Código";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 168);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(334, 24);
            this.layoutControlItem7.StartNewLine = true;
            this.layoutControlItem7.Text = "Layout";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(69, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.teAceite;
            this.layoutControlItem8.CustomizationFormText = "Código";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 192);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(334, 24);
            this.layoutControlItem8.StartNewLine = true;
            this.layoutControlItem8.Text = "Aceite";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(69, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.teSequencia;
            this.layoutControlItem9.CustomizationFormText = "Código";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 240);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(334, 27);
            this.layoutControlItem9.StartNewLine = true;
            this.layoutControlItem9.Text = "Sequencia";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(69, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.teConta;
            this.layoutControlItem11.CustomizationFormText = "Código";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(334, 24);
            this.layoutControlItem11.StartNewLine = true;
            this.layoutControlItem11.Text = "Conta";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(69, 13);
            // 
            // teConta
            // 
            this.teConta.Location = new System.Drawing.Point(84, 60);
            this.teConta.Name = "teConta";
            this.teConta.Size = new System.Drawing.Size(258, 20);
            this.teConta.StyleController = this.FormLayoutControlBase;
            this.teConta.TabIndex = 3;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.tePosto;
            this.layoutControlItem1.CustomizationFormText = "Código";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(334, 24);
            this.layoutControlItem1.StartNewLine = true;
            this.layoutControlItem1.Text = "Posto";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(69, 13);
            // 
            // tePosto
            // 
            this.tePosto.Location = new System.Drawing.Point(84, 108);
            this.tePosto.Name = "tePosto";
            this.tePosto.Size = new System.Drawing.Size(258, 20);
            this.tePosto.StyleController = this.FormLayoutControlBase;
            this.tePosto.TabIndex = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.teNossoNumero;
            this.layoutControlItem10.CustomizationFormText = "Código";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 216);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(334, 24);
            this.layoutControlItem10.StartNewLine = true;
            this.layoutControlItem10.Text = "Nosso Número";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(69, 13);
            // 
            // teNossoNumero
            // 
            this.teNossoNumero.Location = new System.Drawing.Point(84, 228);
            this.teNossoNumero.Name = "teNossoNumero";
            this.teNossoNumero.Size = new System.Drawing.Size(258, 20);
            this.teNossoNumero.StyleController = this.FormLayoutControlBase;
            this.teNossoNumero.TabIndex = 10;
            // 
            // FrmCRUDBoleto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(354, 314);
            this.Location = new System.Drawing.Point(500, 100);
            this.Name = "FrmCRUDBoleto";
            this.Text = "Configuração de Boletos";
            ((System.ComponentModel.ISupportInitialize)(this.FormLayoutControlBase)).EndInit();
            this.FormLayoutControlBase.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.teNumBanco.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teAgencia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teBanco.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teCedente.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teCarteira.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teLayout.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teAceite.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teSequencia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teConta.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tePosto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teNossoNumero.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit teNumBanco;
        private DevExpress.XtraEditors.TextEdit teAgencia;
        private DevExpress.XtraEditors.TextEdit teBanco;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.TextEdit teCedente;
        private DevExpress.XtraEditors.TextEdit teCarteira;
        private DevExpress.XtraEditors.TextEdit teLayout;
        private DevExpress.XtraEditors.TextEdit teAceite;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.TextEdit teSequencia;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.TextEdit tePosto;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit teNossoNumero;
        private DevExpress.XtraEditors.TextEdit teConta;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
    }
}