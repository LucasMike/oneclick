﻿using Npgsql;

namespace Projeto.Controles.CRUD
{
    public partial class FrmCRUDCidade : FrmCRUDBase
    {
        int iID = ID_INSERINDO;

        public FrmCRUDCidade() : base("cidade")
        {
            InitializeComponent();
        }

        public override void Visit(NpgsqlDataReader visitor)
        {
            iID = LerValorColuna<int>(visitor, "id");
            teNome.Text = LerValorColuna<string>(visitor, "nome");
            teCEP.Text = LerValorColuna<string>(visitor, "cep");
            teEstado.Text = LerValorColuna<string>(visitor, "estado");
        }


        protected override string MontarSQLInsert()
        {
            return $@"insert into cidade(nome, cep, estado) values
                ('{teNome.Text}', '{teCEP.Text}', '{teEstado.Text}')";
        }

        protected override string MontarSQLUpdate()
        {
            return $@"update public.cidade 
                set nome='{teNome.Text}' , cep='{teCEP.Text}', estado='{teEstado.Text}' 
                where id= {iID} ;";
        }
    }
}
