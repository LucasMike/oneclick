﻿namespace Projeto.Controles.CRUD
{
    partial class FrmCRUDCaixa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.teId = new DevExpress.XtraEditors.TextEdit();
            this.teNome = new DevExpress.XtraEditors.TextEdit();
            this.teValor = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.FormLayoutControlBase)).BeginInit();
            this.FormLayoutControlBase.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teNome.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teValor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // FormLayoutControlBase
            // 
            this.FormLayoutControlBase.Controls.Add(this.teId);
            this.FormLayoutControlBase.Controls.Add(this.teNome);
            this.FormLayoutControlBase.Controls.Add(this.teValor);
            this.FormLayoutControlBase.Root = this.layoutControlGroup1;
            this.FormLayoutControlBase.Size = new System.Drawing.Size(206, 94);
            // 
            // teId
            // 
            this.teId.Location = new System.Drawing.Point(48, 12);
            this.teId.Name = "teId";
            this.teId.Size = new System.Drawing.Size(146, 20);
            this.teId.StyleController = this.FormLayoutControlBase;
            this.teId.TabIndex = 0;
            // 
            // teNome
            // 
            this.teNome.EditValue = "";
            this.teNome.Location = new System.Drawing.Point(48, 36);
            this.teNome.Name = "teNome";
            this.teNome.Size = new System.Drawing.Size(146, 20);
            this.teNome.StyleController = this.FormLayoutControlBase;
            this.teNome.TabIndex = 2;
            // 
            // teValor
            // 
            this.teValor.EditValue = "";
            this.teValor.Location = new System.Drawing.Point(48, 60);
            this.teValor.Name = "teValor";
            this.teValor.Size = new System.Drawing.Size(146, 20);
            this.teValor.StyleController = this.FormLayoutControlBase;
            this.teValor.TabIndex = 3;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutControlItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(206, 94);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.teId;
            this.layoutControlItem1.CustomizationFormText = "Código";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(186, 24);
            this.layoutControlItem1.StartNewLine = true;
            this.layoutControlItem1.Text = "Código";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(33, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.teNome;
            this.layoutControlItem3.CustomizationFormText = "Nome";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(186, 24);
            this.layoutControlItem3.Text = "Nome";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(33, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.teValor;
            this.layoutControlItem2.CustomizationFormText = "Nome";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(186, 26);
            this.layoutControlItem2.Text = "Valor";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(33, 13);
            // 
            // FrmCRUDCaixa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(206, 121);
            this.Location = new System.Drawing.Point(500, 100);
            this.Name = "FrmCRUDCaixa";
            this.Text = "Caixa";
            ((System.ComponentModel.ISupportInitialize)(this.FormLayoutControlBase)).EndInit();
            this.FormLayoutControlBase.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.teId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teNome.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teValor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit teId;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit teNome;
        private DevExpress.XtraEditors.TextEdit teValor;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
    }
}