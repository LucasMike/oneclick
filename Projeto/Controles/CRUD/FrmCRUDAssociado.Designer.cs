﻿namespace Projeto.Controles.CRUD
{
    partial class FrmCRUDAssociado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCRUDAssociado));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.gcDias = new DevExpress.XtraGrid.GridControl();
            this.gvDias = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcDiaSemana = new DevExpress.XtraGrid.Columns.GridColumn();
            this.luEditRepoDias = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gcTurnos = new DevExpress.XtraGrid.Columns.GridColumn();
            this.luEditRepoTurnos = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.teEmail = new DevExpress.XtraEditors.TextEdit();
            this.teTelefone = new DevExpress.XtraEditors.TextEdit();
            this.teCPF = new DevExpress.XtraEditors.TextEdit();
            this.teNome = new DevExpress.XtraEditors.TextEdit();
            this.teDataInicio = new DevExpress.XtraEditors.DateEdit();
            this.teDataFim = new DevExpress.XtraEditors.DateEdit();
            this.teEndereco = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lkuCidade = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.beAnexo = new DevExpress.XtraEditors.ButtonEdit();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lkuDestino = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.FormLayoutControlBase)).BeginInit();
            this.FormLayoutControlBase.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcDias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luEditRepoDias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luEditRepoTurnos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTelefone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teCPF.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teNome.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDataInicio.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDataInicio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDataFim.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDataFim.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teEndereco.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkuCidade.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.beAnexo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkuDestino.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            this.SuspendLayout();
            // 
            // FormLayoutControlBase
            // 
            this.FormLayoutControlBase.Controls.Add(this.lkuDestino);
            this.FormLayoutControlBase.Controls.Add(this.beAnexo);
            this.FormLayoutControlBase.Controls.Add(this.lkuCidade);
            this.FormLayoutControlBase.Controls.Add(this.panelControl1);
            this.FormLayoutControlBase.Controls.Add(this.teEmail);
            this.FormLayoutControlBase.Controls.Add(this.teTelefone);
            this.FormLayoutControlBase.Controls.Add(this.teCPF);
            this.FormLayoutControlBase.Controls.Add(this.teNome);
            this.FormLayoutControlBase.Controls.Add(this.teDataInicio);
            this.FormLayoutControlBase.Controls.Add(this.teDataFim);
            this.FormLayoutControlBase.Controls.Add(this.teEndereco);
            this.FormLayoutControlBase.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(628, 117, 450, 400);
            this.FormLayoutControlBase.Root = this.layoutControlGroup1;
            this.FormLayoutControlBase.Size = new System.Drawing.Size(578, 217);
            this.FormLayoutControlBase.TabIndex = 0;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.gcDias);
            this.panelControl1.Controls.Add(this.label1);
            this.panelControl1.Location = new System.Drawing.Point(256, 60);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(310, 145);
            this.panelControl1.TabIndex = 6;
            // 
            // gcDias
            // 
            this.gcDias.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcDias.EmbeddedNavigator.TextStringFormat = "Registro {0} de {1}";
            this.gcDias.Location = new System.Drawing.Point(2, 2);
            this.gcDias.MainView = this.gvDias;
            this.gcDias.Name = "gcDias";
            this.gcDias.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.luEditRepoDias,
            this.luEditRepoTurnos});
            this.gcDias.Size = new System.Drawing.Size(306, 141);
            this.gcDias.TabIndex = 2;
            this.gcDias.UseEmbeddedNavigator = true;
            this.gcDias.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvDias});
            // 
            // gvDias
            // 
            this.gvDias.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcDiaSemana,
            this.gcTurnos});
            this.gvDias.GridControl = this.gcDias;
            this.gvDias.Name = "gvDias";
            this.gvDias.OptionsCustomization.AllowGroup = false;
            this.gvDias.OptionsView.ShowGroupPanel = false;
            this.gvDias.OptionsView.ShowIndicator = false;
            // 
            // gcDiaSemana
            // 
            this.gcDiaSemana.Caption = "Dia";
            this.gcDiaSemana.ColumnEdit = this.luEditRepoDias;
            this.gcDiaSemana.FieldName = "dia";
            this.gcDiaSemana.Name = "gcDiaSemana";
            this.gcDiaSemana.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.gcDiaSemana.Visible = true;
            this.gcDiaSemana.VisibleIndex = 0;
            this.gcDiaSemana.Width = 529;
            // 
            // luEditRepoDias
            // 
            this.luEditRepoDias.AutoHeight = false;
            this.luEditRepoDias.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luEditRepoDias.Name = "luEditRepoDias";
            this.luEditRepoDias.NullText = "";
            // 
            // gcTurnos
            // 
            this.gcTurnos.Caption = "Turnos";
            this.gcTurnos.ColumnEdit = this.luEditRepoTurnos;
            this.gcTurnos.FieldName = "turno";
            this.gcTurnos.Name = "gcTurnos";
            this.gcTurnos.Visible = true;
            this.gcTurnos.VisibleIndex = 1;
            this.gcTurnos.Width = 179;
            // 
            // luEditRepoTurnos
            // 
            this.luEditRepoTurnos.AutoHeight = false;
            this.luEditRepoTurnos.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luEditRepoTurnos.Name = "luEditRepoTurnos";
            this.luEditRepoTurnos.NullText = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 1;
            this.label1.Tag = "";
            this.label1.Text = "Dias";
            // 
            // teEmail
            // 
            this.teEmail.Location = new System.Drawing.Point(66, 84);
            this.teEmail.Name = "teEmail";
            this.teEmail.Size = new System.Drawing.Size(186, 20);
            this.teEmail.StyleController = this.FormLayoutControlBase;
            this.teEmail.TabIndex = 7;
            // 
            // teTelefone
            // 
            this.teTelefone.Location = new System.Drawing.Point(66, 60);
            this.teTelefone.Name = "teTelefone";
            this.teTelefone.Properties.Mask.EditMask = "(99) 0000-0000";
            this.teTelefone.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.teTelefone.Size = new System.Drawing.Size(186, 20);
            this.teTelefone.StyleController = this.FormLayoutControlBase;
            this.teTelefone.TabIndex = 5;
            // 
            // teCPF
            // 
            this.teCPF.Location = new System.Drawing.Point(66, 36);
            this.teCPF.Name = "teCPF";
            this.teCPF.Properties.Mask.EditMask = "000.000.000-00";
            this.teCPF.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.teCPF.Size = new System.Drawing.Size(186, 20);
            this.teCPF.StyleController = this.FormLayoutControlBase;
            this.teCPF.TabIndex = 3;
            // 
            // teNome
            // 
            this.teNome.EditValue = "";
            this.teNome.Location = new System.Drawing.Point(66, 12);
            this.teNome.Name = "teNome";
            this.teNome.Size = new System.Drawing.Size(186, 20);
            this.teNome.StyleController = this.FormLayoutControlBase;
            this.teNome.TabIndex = 0;
            // 
            // teDataInicio
            // 
            this.teDataInicio.EditValue = null;
            this.teDataInicio.Location = new System.Drawing.Point(310, 12);
            this.teDataInicio.Name = "teDataInicio";
            this.teDataInicio.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.teDataInicio.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.teDataInicio.Properties.EditFormat.FormatString = "";
            this.teDataInicio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.teDataInicio.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.teDataInicio.Size = new System.Drawing.Size(256, 20);
            this.teDataInicio.StyleController = this.FormLayoutControlBase;
            this.teDataInicio.TabIndex = 2;
            // 
            // teDataFim
            // 
            this.teDataFim.EditValue = null;
            this.teDataFim.Location = new System.Drawing.Point(310, 36);
            this.teDataFim.Name = "teDataFim";
            this.teDataFim.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.teDataFim.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.teDataFim.Properties.EditFormat.FormatString = "";
            this.teDataFim.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.teDataFim.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.teDataFim.Size = new System.Drawing.Size(256, 20);
            this.teDataFim.StyleController = this.FormLayoutControlBase;
            this.teDataFim.TabIndex = 4;
            // 
            // teEndereco
            // 
            this.teEndereco.Location = new System.Drawing.Point(66, 108);
            this.teEndereco.Name = "teEndereco";
            this.teEndereco.Size = new System.Drawing.Size(186, 20);
            this.teEndereco.StyleController = this.FormLayoutControlBase;
            this.teEndereco.TabIndex = 8;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem13,
            this.layoutControlItem9,
            this.layoutControlItem8,
            this.layoutControlItem7,
            this.layoutControlItem6,
            this.layoutControlItem2,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem3,
            this.layoutControlItem5});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(578, 217);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.teDataInicio;
            this.layoutControlItem4.Location = new System.Drawing.Point(244, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(314, 24);
            this.layoutControlItem4.Text = "Data Inicio";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.panelControl1;
            this.layoutControlItem13.Location = new System.Drawing.Point(244, 48);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(314, 149);
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.teEndereco;
            this.layoutControlItem9.CustomizationFormText = "Telefone";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(244, 24);
            this.layoutControlItem9.Text = "Endereço";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.teEmail;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(244, 24);
            this.layoutControlItem8.Text = "Email";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.teTelefone;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(244, 24);
            this.layoutControlItem7.Text = "Telefone";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.teDataFim;
            this.layoutControlItem6.Location = new System.Drawing.Point(244, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(314, 24);
            this.layoutControlItem6.Text = "Data fim";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.lkuCidade;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(244, 24);
            this.layoutControlItem2.Text = "Cidade";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(51, 13);
            // 
            // lkuCidade
            // 
            this.lkuCidade.Location = new System.Drawing.Point(66, 132);
            this.lkuCidade.Name = "lkuCidade";
            this.lkuCidade.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkuCidade.Properties.DisplayMember = "nome";
            this.lkuCidade.Properties.KeyMember = "id";
            this.lkuCidade.Properties.NullText = "";
            this.lkuCidade.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.lkuCidade.Properties.ValueMember = "id";
            this.lkuCidade.Size = new System.Drawing.Size(186, 20);
            this.lkuCidade.StyleController = this.FormLayoutControlBase;
            this.lkuCidade.TabIndex = 9;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.beAnexo;
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 168);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(244, 29);
            this.layoutControlItem10.Text = "Anexo";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(51, 13);
            // 
            // beAnexo
            // 
            this.beAnexo.Location = new System.Drawing.Point(66, 180);
            this.beAnexo.Name = "beAnexo";
            this.beAnexo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.beAnexo.Size = new System.Drawing.Size(186, 20);
            this.beAnexo.StyleController = this.FormLayoutControlBase;
            this.beAnexo.TabIndex = 11;
            this.beAnexo.Click += new System.EventHandler(this.buttonEdit1_Click);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.lkuDestino;
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(244, 24);
            this.layoutControlItem11.Text = "Destino";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(51, 13);
            // 
            // lkuDestino
            // 
            this.lkuDestino.Location = new System.Drawing.Point(66, 156);
            this.lkuDestino.Name = "lkuDestino";
            this.lkuDestino.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkuDestino.Properties.DisplayMember = "nome";
            this.lkuDestino.Properties.KeyMember = "id";
            this.lkuDestino.Properties.NullText = "";
            this.lkuDestino.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.lkuDestino.Properties.ValueMember = "id";
            this.lkuDestino.Size = new System.Drawing.Size(186, 20);
            this.lkuDestino.StyleController = this.FormLayoutControlBase;
            this.lkuDestino.TabIndex = 10;
            this.lkuDestino.Tag = "";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.teNome;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(244, 24);
            this.layoutControlItem3.Text = "Nome";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.teCPF;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(244, 24);
            this.layoutControlItem5.Text = "CFP";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(51, 13);
            // 
            // FrmCRUDAssociado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(578, 244);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(400, 100);
            this.MinimizeBox = false;
            this.Name = "FrmCRUDAssociado";
            this.Text = "Associado";
            ((System.ComponentModel.ISupportInitialize)(this.FormLayoutControlBase)).EndInit();
            this.FormLayoutControlBase.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcDias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luEditRepoDias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luEditRepoTurnos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTelefone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teCPF.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teNome.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDataInicio.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDataInicio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDataFim.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDataFim.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teEndereco.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkuCidade.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.beAnexo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkuDestino.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit teNome;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.DateEdit teDataInicio;
        private DevExpress.XtraEditors.TextEdit teEmail;
        private DevExpress.XtraEditors.TextEdit teTelefone;
        private DevExpress.XtraEditors.TextEdit teCPF;
        private DevExpress.XtraEditors.DateEdit teDataFim;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.TextEdit teEndereco;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LookUpEdit lkuCidade;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraGrid.GridControl gcDias;
        private DevExpress.XtraGrid.Views.Grid.GridView gvDias;
        private DevExpress.XtraGrid.Columns.GridColumn gcDiaSemana;
        private DevExpress.XtraGrid.Columns.GridColumn gcTurnos;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit luEditRepoDias;
        private DevExpress.XtraEditors.ButtonEdit beAnexo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.LookUpEdit lkuDestino;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit luEditRepoTurnos;
    }
}