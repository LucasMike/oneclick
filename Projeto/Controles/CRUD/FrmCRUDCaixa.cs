﻿using Npgsql;
namespace Projeto.Controles.CRUD
{
    public partial class FrmCRUDCaixa : FrmCRUDBase
    {
        public FrmCRUDCaixa() :base("caixa")
        {
            InitializeComponent();
        }

        public override void Visit(NpgsqlDataReader visitor)
        {
            teId.Text = LerValorColuna<int>(visitor, "id").ToString();
            teNome.Text = LerValorColuna<string>(visitor, "nome");
            teValor.Text = LerValorColuna<double>(visitor, "valor").ToString();
        }
    }
}
