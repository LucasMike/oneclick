﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projeto.Controles.CRUD
{
    public partial class FrmGeraContas : Form
    {
        public FrmGeraContas()
        {
            InitializeComponent();
        }

        private void btiSalvar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (teDataInicio.Text != "" && teDataFim.Text != "")
            {

            }
            else
            {
                MessageBox.Show(
                    "Preencha todos os campos para poder salvar",
                    "Erro",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error,
                    MessageBoxDefaultButton.Button1,
                    MessageBoxOptions.DefaultDesktopOnly
                 );
            }
            //chamar metodo do conta a receber que gera as contas a receber e passar o periodo
        }

        private void btiCancelar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }
    }
}
