﻿using Npgsql;
using Projeto.Classes;

namespace Projeto.Controles.CRUD
{
    public partial class FrmCRUDDestino : FrmCRUDBase
    {
        public FrmCRUDDestino() : base("destino")
        {
            InitializeComponent();
        }

        public override void Visit(NpgsqlDataReader visitor)
        {
            _ID = LerValorColuna<int>(visitor, "id");
            teUniversidade.Text = LerValorColuna<string>(visitor, "universidade");
            lkuCidade.EditValue = LerValorColuna<int>(visitor, "cidade_id").ToString();
        }

        protected override void CarregarRelacoes()
        {
            lkuCidade.Properties.DataSource = Conexao.Instancia.Consultar("select id, nome from cidade");
        }

        protected override string MontarSQLInsert()
        {
            return $@"insert into destino(universidade, cidade_id) values
                ('{teUniversidade.Text}', '{lkuCidade.EditValue}')";
        }

        protected override string MontarSQLUpdate()
        {
            return $@"update public.destino 
                set universidade='{teUniversidade.Text}' , cidade_id='{lkuCidade.EditValue}' 
                where id= {_ID} ;";
        }
    }
}
