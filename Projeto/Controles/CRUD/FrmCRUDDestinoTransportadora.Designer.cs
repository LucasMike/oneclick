﻿namespace Projeto.Controles.CRUD
{
    partial class FrmCRUDDestinoTransportadora
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lkuDestino = new DevExpress.XtraEditors.LookUpEdit();
            this.lkuTransportador = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ceValor = new DevExpress.XtraEditors.CalcEdit();
            ((System.ComponentModel.ISupportInitialize)(this.FormLayoutControlBase)).BeginInit();
            this.FormLayoutControlBase.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lkuDestino.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkuTransportador.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceValor.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // FormLayoutControlBase
            // 
            this.FormLayoutControlBase.Controls.Add(this.ceValor);
            this.FormLayoutControlBase.Controls.Add(this.lkuTransportador);
            this.FormLayoutControlBase.Controls.Add(this.lkuDestino);
            this.FormLayoutControlBase.Root = this.layoutControlGroup1;
            this.FormLayoutControlBase.Size = new System.Drawing.Size(315, 95);
            // 
            // lkuDestino
            // 
            this.lkuDestino.Location = new System.Drawing.Point(90, 12);
            this.lkuDestino.Name = "lkuDestino";
            this.lkuDestino.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkuDestino.Properties.DisplayMember = "universidade";
            this.lkuDestino.Properties.KeyMember = "id";
            this.lkuDestino.Properties.NullText = "";
            this.lkuDestino.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.lkuDestino.Properties.ValueMember = "id";
            this.lkuDestino.Size = new System.Drawing.Size(213, 20);
            this.lkuDestino.StyleController = this.FormLayoutControlBase;
            this.lkuDestino.TabIndex = 0;
            // 
            // lkuTransportador
            // 
            this.lkuTransportador.Location = new System.Drawing.Point(90, 36);
            this.lkuTransportador.Name = "lkuTransportador";
            this.lkuTransportador.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkuTransportador.Properties.DisplayMember = "nome";
            this.lkuTransportador.Properties.KeyMember = "id";
            this.lkuTransportador.Properties.NullText = "";
            this.lkuTransportador.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.lkuTransportador.Properties.ValueMember = "id";
            this.lkuTransportador.Size = new System.Drawing.Size(213, 20);
            this.lkuTransportador.StyleController = this.FormLayoutControlBase;
            this.lkuTransportador.TabIndex = 2;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(315, 95);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.lkuDestino;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(295, 24);
            this.layoutControlItem1.Text = "Universidade";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(75, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.lkuTransportador;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(295, 24);
            this.layoutControlItem2.Text = "Transportadora";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(75, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.ceValor;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(295, 27);
            this.layoutControlItem3.Text = "Valor dia";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(75, 13);
            // 
            // ceValor
            // 
            this.ceValor.Location = new System.Drawing.Point(90, 60);
            this.ceValor.Name = "ceValor";
            this.ceValor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ceValor.Properties.DisplayFormat.FormatString = "c2";
            this.ceValor.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.ceValor.Properties.EditFormat.FormatString = "c2";
            this.ceValor.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.ceValor.Properties.Mask.EditMask = "c2";
            this.ceValor.Properties.Precision = 2;
            this.ceValor.Size = new System.Drawing.Size(213, 20);
            this.ceValor.StyleController = this.FormLayoutControlBase;
            this.ceValor.TabIndex = 3;
            // 
            // FrmCRUDDestinoTransportadora
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(315, 122);
            this.Location = new System.Drawing.Point(500, 100);
            this.Name = "FrmCRUDDestinoTransportadora";
            this.Text = "FrmCRUDDestinoTransportadora";
            ((System.ComponentModel.ISupportInitialize)(this.FormLayoutControlBase)).EndInit();
            this.FormLayoutControlBase.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lkuDestino.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkuTransportador.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceValor.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraEditors.LookUpEdit lkuDestino;
        private DevExpress.XtraEditors.LookUpEdit lkuTransportador;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.CalcEdit ceValor;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
    }
}