﻿using DevExpress.XtraEditors;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using Projeto.Classes;

namespace Projeto.Controles.CRUD
{
    public partial class FrmCRUDAssociado : FrmCRUDBase
    {
        private DataTable dtDados = new DataTable();
        int iID = ID_INSERINDO;

        public FrmCRUDAssociado() : base("associado")
        {
            InitializeComponent();

            dtDados.Columns.Add("dia", typeof(int));
            dtDados.Columns.Add("turno", typeof(int));

            gcDias.DataSource = dtDados;

            luEditRepoDias.DataSource = new Dictionary<int, string>
            {
                [(int)DayOfWeek.Monday] = "Segunda-Feira",
                [(int)DayOfWeek.Tuesday] = "Terça-Feria",
                [(int)DayOfWeek.Wednesday] = "Quarta-Feira",
                [(int)DayOfWeek.Thursday] = "Quinta-Feira",
                [(int)DayOfWeek.Friday] = "Sexta-Feira",
                [(int)DayOfWeek.Saturday] = "Sabado"
            };

            luEditRepoTurnos.DataSource = new Dictionary<int, string>
            {
                [1] = "Manhã",
                [2] = "Tarde",
                [3] = "Noite"
            };
        }

        public override void Visit(NpgsqlDataReader visitor)
        {
            iID = LerValorColuna<int>(visitor, "id");
            teNome.Text = LerValorColuna<string>(visitor, "nome");
            teCPF.Text = LerValorColuna<string>(visitor, "cpf");
            teTelefone.Text = LerValorColuna<string>(visitor, "telefone");
            teEmail.Text = LerValorColuna<string>(visitor, "email");
            teEndereco.Text = LerValorColuna<string>(visitor, "endereco");
            lkuCidade.EditValue = LerValorColuna<int>(visitor, "cidade_id").ToString();
            lkuDestino.EditValue = LerValorColuna<int>(visitor, "destino_transportadora_id").ToString();
            beAnexo.Text = LerValorColuna<string>(visitor, "anexo");
            teDataInicio.EditValue = LerValorColuna<DateTime>(visitor, "dataini");
            teDataFim.EditValue = LerValorColuna<DateTime>(visitor, "datafim");
        }

        protected override string MontarSQLInsert()
        {
            string query = $@"INSERT INTO public.associado(
	            nome, cpf, telefone, email, endereco, anexo, dataini, datafim, cidade_id, destino_transportadora_id)
	            VALUES ('{teNome.Text}', '{teCPF.Text}', '{teTelefone.Text}', '{teEmail.Text}', '{teEndereco.Text}',
                '{beAnexo.Text}', '{teDataInicio.DateTime}', '{teDataFim.DateTime}', {lkuCidade.EditValue}, {lkuDestino.EditValue});";

            if (dtDados.Rows.Count > 0)
            {
                query += "INSERT INTO public.associado_dias(associado_id, dia, turno) VALUES ";
                foreach (DataRow dr in dtDados.Rows)
                {
                    query += $"((select max(id) from associado), {dr["dia"]}, {dr["turno"]}),\n";
                }
                query = query.Remove(query.Length - 2) + ";";
            }
            return query;
        }

        protected override string MontarSQLUpdate()
        {
            string query = $@"update public.associado
	            set nome='{teNome.Text}', cpf='{teCPF.Text}', telefone='{teTelefone.Text}', email='{teEmail.Text}', 
                endereco='{teEndereco.Text}', anexo='{beAnexo.Text}', dataini='{teDataInicio.DateTime}', 
                datafim='{teDataFim.DateTime}', cidade_id={lkuCidade.EditValue}, destino_transportadora_id={lkuDestino.EditValue}
	            where id = {iID};
            ";

            query += $@"DELETE FROM public.associado_dias where associado_id = {iID}; ";

            dtDados.AcceptChanges();
            if (dtDados.Rows.Count > 0)
            {
                query += "INSERT INTO public.associado_dias(associado_id, dia, turno) VALUES ";
                foreach (DataRow dr in dtDados.Rows)
                {
                    if (dr["dia"].ToString() != "" || !dr.IsNull("dia"))
                    {
                        query += $"({iID}, {dr["dia"]}, {dr["turno"]}),\n";
                    }
                }
                query = query.Remove(query.Length - 2) + ";";
            }

            return query;
        }

        protected override void CarregarRelacoes()
        {
            lkuCidade.Properties.DataSource = Conexao.Instancia.Consultar("select id, nome from cidade");
            lkuDestino.Properties.DataSource = Conexao.Instancia.Consultar("select a.id, (b.universidade || ' - ' || c.nome) as nome from destino_transportadora a, destino b, transportadora c where a.destino_id = b.id and a.transportadora_id = c.id");

            dtDados = Conexao.Instancia.Consultar($"select dia::integer, turno::integer from associado_dias where associado_id = {_ID}");
            gcDias.DataSource = dtDados;
        }

        private void buttonEdit1_Click(object sender, EventArgs e)
        {
            ButtonEdit btn = (ButtonEdit)sender;
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    btn.Text = dlg.FileName;
                }
            }
        }
    }
}