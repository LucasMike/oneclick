﻿using Npgsql;
using Projeto.Classes;
using System;
using static System.Convert;

namespace Projeto.Controles.CRUD
{
    public partial class FrmCRUDContaPagar : FrmCRUDBase
    {
        int iID = ID_INSERINDO;

        public FrmCRUDContaPagar() : base("conta_pagar")
        {
            InitializeComponent();
        }

        public override void Visit(NpgsqlDataReader visitor)
        {
            iID = LerValorColuna<int>(visitor, "id");
            teVencimento.Text = LerValorColuna<DateTime>(visitor, "vencimento").ToString();
            ceValor.EditValue = ToDecimal(LerValorColuna<double>(visitor, "valor").ToString());
            lkuTransportadora.EditValue = LerValorColuna<int>(visitor, "transportadora_id").ToString();
        }

        protected override void CarregarRelacoes()
        {
            lkuTransportadora.Properties.DataSource = Conexao.Instancia.Consultar("select id, nome from transportadora");
        }

        protected override string MontarSQLInsert()
        {
            return $@"INSERT INTO public.conta_pagar(vencimento, valor, status, transportadora_id)
	            VALUES ('{teVencimento.DateTime}', '{ceValor.Value}', 'A', '{lkuTransportadora.EditValue}')";
        }

        protected override string MontarSQLUpdate()
        {
            return $@"update public.conta_receber 
                set vencimento='{teVencimento.DateTime}' , valor='{ceValor.Value}', transportadora_id='{lkuTransportadora.EditValue}'
                where id= {iID} ;";
        }
    }
}
