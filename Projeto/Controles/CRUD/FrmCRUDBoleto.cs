﻿using Npgsql;

namespace Projeto.Controles.CRUD
{
    public partial class FrmCRUDBoleto : FrmCRUDBase
    {
        int iID = ID_INSERINDO;

        public FrmCRUDBoleto() :base("boleto")
        {
            InitializeComponent();
        }

        public override void Visit(NpgsqlDataReader visitor)
        {
            iID = LerValorColuna<int>(visitor, "id");
            teNumBanco.Text = LerValorColuna<int>(visitor, "num_banco").ToString();
            teAgencia.Text = LerValorColuna<int>(visitor, "agencia").ToString();
            teConta.Text = LerValorColuna<string>(visitor, "conta");
            teBanco.Text = LerValorColuna<string>(visitor, "banco");
            tePosto.Text = LerValorColuna<string>(visitor, "posto");
            teCedente.Text = LerValorColuna<int>(visitor, "cedente").ToString();
            teCarteira.Text = LerValorColuna<string>(visitor, "carteira");
            teLayout.Text = LerValorColuna<string>(visitor, "layout");
            teAceite.Text = LerValorColuna<string>(visitor, "aceite");
            teNossoNumero.Text = LerValorColuna<string>(visitor, "ult_nosso_numero");
            teSequencia.Text = LerValorColuna<int>(visitor, "sequencia_arquivo").ToString();
        }

        protected override string MontarSQLInsert()
        {
            return $@"insert into boleto(num_banco, agencia, banco, cedente, carteira, layout, aceite, status, sequencia_arquivo, posto, ult_nosso_numero, conta)) values
                ('{teNumBanco.Text}', '{teAgencia.Text}', '{teBanco.Text}', '{teCedente.Text}', '{teCarteira.Text}', '{teLayout.Text}', '{teAceite.Text}', 'P', '{teSequencia.Text}', '{tePosto.Text}', '{teNossoNumero.Text}', '{teConta.Text}')";
        }

        protected override string MontarSQLUpdate()
        {
            return $@"update public.boleto 
                set num_banco='{teNumBanco.Text}', agencia='{teAgencia.Text}', banco='{teBanco.Text}', cedente='{teCedente.Text}', carteira='{teCarteira.Text}', layout='{teLayout.Text}', aceite='{teAceite.Text}', status='P', sequencia_arquivo='{teSequencia.Text}', posto='{tePosto.Text}', ult_nosso_numero='{teNossoNumero.Text}', conta='{teConta.Text}'
                where id= {iID} ;";
        }
    }
}
