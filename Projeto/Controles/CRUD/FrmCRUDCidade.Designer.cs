﻿namespace Projeto.Controles.CRUD
{
    partial class FrmCRUDCidade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.teNome = new DevExpress.XtraEditors.TextEdit();
            this.teCEP = new DevExpress.XtraEditors.TextEdit();
            this.teEstado = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.FormLayoutControlBase)).BeginInit();
            this.FormLayoutControlBase.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teNome.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teCEP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teEstado.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            this.SuspendLayout();
            // 
            // FormLayoutControlBase
            // 
            this.FormLayoutControlBase.Controls.Add(this.teNome);
            this.FormLayoutControlBase.Controls.Add(this.teCEP);
            this.FormLayoutControlBase.Controls.Add(this.teEstado);
            this.FormLayoutControlBase.Root = this.layoutControlGroup1;
            this.FormLayoutControlBase.Size = new System.Drawing.Size(310, 94);
            // 
            // teNome
            // 
            this.teNome.Location = new System.Drawing.Point(48, 12);
            this.teNome.Name = "teNome";
            this.teNome.Size = new System.Drawing.Size(250, 20);
            this.teNome.StyleController = this.FormLayoutControlBase;
            this.teNome.TabIndex = 0;
            // 
            // teCEP
            // 
            this.teCEP.Location = new System.Drawing.Point(48, 36);
            this.teCEP.Name = "teCEP";
            this.teCEP.Properties.Mask.EditMask = "00000-999";
            this.teCEP.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.teCEP.Size = new System.Drawing.Size(250, 20);
            this.teCEP.StyleController = this.FormLayoutControlBase;
            this.teCEP.TabIndex = 2;
            // 
            // teEstado
            // 
            this.teEstado.Location = new System.Drawing.Point(48, 60);
            this.teEstado.Name = "teEstado";
            this.teEstado.Size = new System.Drawing.Size(250, 20);
            this.teEstado.StyleController = this.FormLayoutControlBase;
            this.teEstado.TabIndex = 3;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(310, 94);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.teNome;
            this.layoutControlItem2.CustomizationFormText = "Código";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(290, 24);
            this.layoutControlItem2.StartNewLine = true;
            this.layoutControlItem2.Text = "Nome";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(33, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.teCEP;
            this.layoutControlItem3.CustomizationFormText = "Código";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(290, 24);
            this.layoutControlItem3.StartNewLine = true;
            this.layoutControlItem3.Text = "CEP";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(33, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.teEstado;
            this.layoutControlItem4.CustomizationFormText = "Código";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(290, 26);
            this.layoutControlItem4.StartNewLine = true;
            this.layoutControlItem4.Text = "Estado";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(33, 13);
            // 
            // FrmCRUDCidade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(310, 121);
            this.Location = new System.Drawing.Point(500, 100);
            this.Name = "FrmCRUDCidade";
            this.Text = "Cidade";
            ((System.ComponentModel.ISupportInitialize)(this.FormLayoutControlBase)).EndInit();
            this.FormLayoutControlBase.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.teNome.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teCEP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teEstado.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraEditors.TextEdit teNome;
        private DevExpress.XtraEditors.TextEdit teCEP;
        private DevExpress.XtraEditors.TextEdit teEstado;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
    }
}