﻿using Npgsql;

namespace Projeto.Controles.CRUD
{
    public partial class FrmCRUDUsuario : FrmCRUDBase
    {
        int iID = ID_INSERINDO;

        public FrmCRUDUsuario() : base("usuario")
        {
            InitializeComponent();
        }

        public override void Visit(NpgsqlDataReader visitor)
        {
            iID = LerValorColuna<int>(visitor, "id");
            teNome.Text = LerValorColuna<string>(visitor, "nome");
            teUsuario.Text = LerValorColuna<string>(visitor, "usuario");
            teSenha.Text = LerValorColuna<string>(visitor, "senha");
        }

        protected override string MontarSQLInsert()
        {
            return $@"INSERT INTO public.usuario(nome, usuario, senha)
	            VALUES ('{teNome.Text}', '{teUsuario.Text}', '{teSenha.Text}')";
        }

        protected override string MontarSQLUpdate()
        {
            return $@"update public.usuario 
                set nome='{teNome.Text}' , usuario='{teUsuario.Text}', senha='{teSenha.Text}' 
                where id= {iID} ;";
        }
    }
}
