﻿using DevExpress.XtraEditors;
using Npgsql;
using System;
using Projeto.Interfaces;
using System.Windows.Forms;
using Projeto.Controles.Views;
using Projeto.Classes;

namespace Projeto.Controles.CRUD
{
    // TODO: Implemtar o IDisposable
    public partial class FrmCRUDBase : XtraForm, IDataReaderVisitor
    {
        #region Constantes Publicas

        public const int ID_INSERINDO = -1;

        #endregion

        #region Variaveis Privadas

        private string _tabela;

        #endregion

        #region Variaveis Internas

        /// <summary>
        /// ID do regitro para visualizar/alterar
        /// Se for -1 é um novo registro
        /// </summary>
        protected int _ID = ID_INSERINDO;

        #endregion

        #region Metodos Abstratos

        internal virtual void Salvar()
        {

        }

        #endregion

        #region Métodos Protegidos

        public T LerValorColuna<T>(NpgsqlDataReader reader, string coluna)
        {
            if (reader.IsDBNull(reader.GetOrdinal(coluna)))
            {
                return default(T);
            }

            return reader.GetFieldValue<T>(reader.GetOrdinal(coluna));
        }

        protected virtual string MontarSQLUpdate()
        {
            throw new NotImplementedException("MontarSQLUpdate não implementado no filho");
        }

        protected virtual string MontarSQLInsert()
        {
            throw new NotImplementedException("MonstarSQLInsert não implementado no filho");
        }


        #endregion

        #region Métodos Privados

        public void SetaHabilitadoBotaoSalvar(bool habilitado)
        {
            btiSalvar.Enabled = habilitado;
        }

        public void CarregarDados(int id)
        {
            _ID = id;
            if (_ID != ID_INSERINDO)
            {
                using (NpgsqlDataReader reader = Conexao.Instancia.Consultar(_ID, _tabela))
                {
                    while (reader.Read())
                    {
                        Visit(reader);
                    }
                }
            }

            CarregarRelacoes();
        }

        #endregion

        #region Construtores
        protected FrmCRUDBase()
        {
            InitializeComponent();
            TopLevel = false;

            DialogResult = DialogResult.Cancel;
        }

        protected FrmCRUDBase(string tabela) : this()
        {
            _tabela = tabela;
        }

        #endregion

        #region IDataReaderVisitor

        public virtual void Visit(NpgsqlDataReader visitor)
        {
            throw new System.NotImplementedException("O método IDataReaderVisitor.Visit(NpgsqlDataReader) deve ser implementado no form filho");
        }

        #endregion

        private void btiCancelar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }

        private bool ValidarDados()
        {
            foreach (Control control in FormLayoutControlBase.Controls)
            {
                if (control.Name.StartsWith("te") && string.IsNullOrWhiteSpace(control.Text))
                {
                    return false;
                }
            }
            return true;
        }

        private void btiSalvar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (ValidarDados())
            {
                if (_ID == ID_INSERINDO)
                {
                    Conexao.Instancia.ExecutarSQL(MontarSQLInsert());
                }
                else
                {
                    Conexao.Instancia.ExecutarSQL(MontarSQLUpdate());
                }

                DialogResult = DialogResult.OK;
                Close();
            }
            else
            {
                MessageBox.Show(
                    "Preencha todos os campos para poder salvar",
                    "Erro",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error,
                    MessageBoxDefaultButton.Button1,
                    MessageBoxOptions.DefaultDesktopOnly
                 );
            }
        }

        protected virtual void CarregarRelacoes()
        {
        }
    }
}