﻿using Npgsql;
using Projeto.Classes;
using System;
using static System.Convert;

namespace Projeto.Controles.CRUD
{
    public partial class FrmCRUDContaReceber : FrmCRUDBase
    {
        int iID = ID_INSERINDO;

        public FrmCRUDContaReceber() :base("conta_receber")
        {
            InitializeComponent();
        }
        
        public override void Visit(NpgsqlDataReader visitor)
        {
            iID = LerValorColuna<int>(visitor, "id");
            teVencimento.EditValue = LerValorColuna<DateTime>(visitor, "vencimento");
            lkuAssociado.EditValue = LerValorColuna<int>(visitor, "associado_id").ToString();
            ceValor.EditValue = ToDecimal(LerValorColuna<double>(visitor, "valor").ToString());
        }

        protected override void CarregarRelacoes()
        {
            lkuAssociado.Properties.DataSource = Conexao.Instancia.Consultar("select id, nome from associado");
        }

        protected override string MontarSQLInsert()
        {
            return $@"INSERT INTO public.conta_receber(vencimento, valor, boleto, associado_id, boleto_id, status)
	            VALUES ('{teVencimento.DateTime}', '{ceValor.Value}', '', '{lkuAssociado.EditValue}','1', 'A')";
        }

        protected override string MontarSQLUpdate()
        {
            return $@"update public.conta_receber 
                set vencimento='{teVencimento.DateTime}' , valor='{ceValor.Value}', associado_id='{lkuAssociado.EditValue}'
                where id= {iID} ;";
        }
    }
}
