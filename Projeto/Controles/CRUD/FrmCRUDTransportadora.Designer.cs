﻿namespace Projeto.Controles.CRUD
{
    partial class FrmCRUDTransportadora
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.teNome = new DevExpress.XtraEditors.TextEdit();
            this.teTelefone = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.FormLayoutControlBase)).BeginInit();
            this.FormLayoutControlBase.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teNome.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTelefone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // FormLayoutControlBase
            // 
            this.FormLayoutControlBase.Controls.Add(this.teNome);
            this.FormLayoutControlBase.Controls.Add(this.teTelefone);
            this.FormLayoutControlBase.Root = this.layoutControlGroup1;
            this.FormLayoutControlBase.Size = new System.Drawing.Size(263, 72);
            // 
            // teNome
            // 
            this.teNome.EditValue = "";
            this.teNome.Location = new System.Drawing.Point(57, 12);
            this.teNome.Name = "teNome";
            this.teNome.Size = new System.Drawing.Size(194, 20);
            this.teNome.StyleController = this.FormLayoutControlBase;
            this.teNome.TabIndex = 0;
            // 
            // teTelefone
            // 
            this.teTelefone.Location = new System.Drawing.Point(57, 36);
            this.teTelefone.Name = "teTelefone";
            this.teTelefone.Properties.Mask.EditMask = "(99) 0000-0000";
            this.teTelefone.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.teTelefone.Size = new System.Drawing.Size(194, 20);
            this.teTelefone.StyleController = this.FormLayoutControlBase;
            this.teTelefone.TabIndex = 2;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(263, 72);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.teNome;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(243, 24);
            this.layoutControlItem2.Text = "Nome";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(42, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.teTelefone;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(243, 28);
            this.layoutControlItem3.Text = "Telefone";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(42, 13);
            // 
            // FrmCRUDTransportadora
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(263, 99);
            this.Location = new System.Drawing.Point(500, 100);
            this.Name = "FrmCRUDTransportadora";
            this.Text = "Transportadora";
            ((System.ComponentModel.ISupportInitialize)(this.FormLayoutControlBase)).EndInit();
            this.FormLayoutControlBase.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.teNome.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTelefone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit teNome;
        private DevExpress.XtraEditors.TextEdit teTelefone;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
    }
}