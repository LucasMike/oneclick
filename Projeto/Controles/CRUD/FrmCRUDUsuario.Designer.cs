﻿namespace Projeto.Controles.CRUD
{
    partial class FrmCRUDUsuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.teNome = new DevExpress.XtraEditors.TextEdit();
            this.teSenha = new DevExpress.XtraEditors.TextEdit();
            this.teUsuario = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.FormLayoutControlBase)).BeginInit();
            this.FormLayoutControlBase.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teNome.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teSenha.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teUsuario.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // FormLayoutControlBase
            // 
            this.FormLayoutControlBase.Controls.Add(this.teNome);
            this.FormLayoutControlBase.Controls.Add(this.teSenha);
            this.FormLayoutControlBase.Controls.Add(this.teUsuario);
            this.FormLayoutControlBase.Root = this.layoutControlGroup1;
            this.FormLayoutControlBase.Size = new System.Drawing.Size(302, 92);
            // 
            // teNome
            // 
            this.teNome.EditValue = "";
            this.teNome.Location = new System.Drawing.Point(51, 12);
            this.teNome.Name = "teNome";
            this.teNome.Size = new System.Drawing.Size(239, 20);
            this.teNome.StyleController = this.FormLayoutControlBase;
            this.teNome.TabIndex = 0;
            // 
            // teSenha
            // 
            this.teSenha.Location = new System.Drawing.Point(51, 60);
            this.teSenha.Name = "teSenha";
            this.teSenha.Size = new System.Drawing.Size(239, 20);
            this.teSenha.StyleController = this.FormLayoutControlBase;
            this.teSenha.TabIndex = 3;
            // 
            // teUsuario
            // 
            this.teUsuario.Location = new System.Drawing.Point(51, 36);
            this.teUsuario.Name = "teUsuario";
            this.teUsuario.Size = new System.Drawing.Size(239, 20);
            this.teUsuario.StyleController = this.FormLayoutControlBase;
            this.teUsuario.TabIndex = 2;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem5,
            this.layoutControlItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(302, 92);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.teNome;
            this.layoutControlItem3.CustomizationFormText = "Nome";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(282, 24);
            this.layoutControlItem3.Text = "Nome";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(36, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.teSenha;
            this.layoutControlItem5.CustomizationFormText = "CFP";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(282, 24);
            this.layoutControlItem5.Text = "Senha";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(36, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.teUsuario;
            this.layoutControlItem2.CustomizationFormText = "CFP";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(282, 24);
            this.layoutControlItem2.Text = "Usuário";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(36, 13);
            // 
            // FrmCRUDUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(302, 119);
            this.Location = new System.Drawing.Point(500, 100);
            this.Name = "FrmCRUDUsuario";
            this.Text = "Usuario";
            ((System.ComponentModel.ISupportInitialize)(this.FormLayoutControlBase)).EndInit();
            this.FormLayoutControlBase.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.teNome.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teSenha.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teUsuario.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit teNome;
        private DevExpress.XtraEditors.TextEdit teSenha;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.TextEdit teUsuario;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
    }
}