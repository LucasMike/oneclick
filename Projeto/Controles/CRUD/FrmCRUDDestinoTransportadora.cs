﻿using Npgsql;
using Projeto.Classes;
using Projeto.Controles.Views;
using static System.Convert;

namespace Projeto.Controles.CRUD
{
    public partial class FrmCRUDDestinoTransportadora : FrmCRUDBase
    {
        /*public FrmCRUDDestinoTransportadora(ViewDestinoTransportadora view) : base("")
        {
            InitializeComponent();
        }
        */
        public FrmCRUDDestinoTransportadora() : base("destino_transportadora")
        {
            InitializeComponent();
        }

        public override void Visit(NpgsqlDataReader visitor)
        {
            _ID = LerValorColuna<int>(visitor, "id");
            lkuDestino.EditValue = LerValorColuna<int>(visitor, "destino_id").ToString();
            lkuTransportador.EditValue = LerValorColuna<int>(visitor, "transportadora_id").ToString();
            ceValor.EditValue = ToDecimal(LerValorColuna<double>(visitor, "valordia").ToString());
        }

        protected override void CarregarRelacoes()
        {
            lkuDestino.Properties.DataSource = Conexao.Instancia.Consultar("select id, universidade from destino");
            lkuTransportador.Properties.DataSource = Conexao.Instancia.Consultar("select id, nome from transportadora");
        }

        protected override string MontarSQLInsert()
        {
            return $@"insert into destino_transportadora(destino_id, transportadora_id, valordia) values
                ('{lkuDestino.EditValue}', '{lkuTransportador.EditValue}', '{ceValor.Value}')";
        }

        protected override string MontarSQLUpdate()
        {
            return $@"update public.destino_transportadora 
                set destino_id='{lkuDestino.EditValue}' , transportadora_id='{lkuTransportador.EditValue}', valordia='{ceValor.Value}' 
                where id= {_ID} ;";
        }
    }
}

