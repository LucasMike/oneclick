﻿using Npgsql;

namespace Projeto.Controles.CRUD
{
    public partial class FrmCRUDTransportadora : FrmCRUDBase
    {
        int iID = ID_INSERINDO;

        public FrmCRUDTransportadora() : base("transportadora")
        {
            InitializeComponent();
        }

        public override void Visit(NpgsqlDataReader visitor)
        {
            iID = LerValorColuna<int>(visitor, "id");
            teNome.Text = LerValorColuna<string>(visitor, "nome");
            teTelefone.Text = LerValorColuna<string>(visitor, "telefone");
        }


        protected override string MontarSQLInsert()
        {
            return $@"INSERT INTO public.transportadora(nome, telefone)
	            VALUES ('{teNome.Text}', '{teTelefone.Text}')";
        }

        protected override string MontarSQLUpdate()
        {
            return $@"update public.transportadora 
                set nome='{teNome.Text}' , telefone='{teTelefone.Text}' 
                where id= {iID} ;";
        }
    }
}
