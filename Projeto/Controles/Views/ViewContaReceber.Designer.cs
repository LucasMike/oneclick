﻿namespace Projeto.Controles.Views
{
    partial class ViewContaReceber
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem1 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            this.gcImagem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repoImagensColunas = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.gcVencimento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.navBarGroup1 = new DevExpress.XtraNavBar.NavBarGroup();
            this.nbiGerarContas = new DevExpress.XtraNavBar.NavBarItem();
            this.nbiBaixarContas = new DevExpress.XtraNavBar.NavBarItem();
            this.nbiCancelarBaixa = new DevExpress.XtraNavBar.NavBarItem();
            this.gcValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcBoleto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcAssociado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.navBarGroup2 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarItem2 = new DevExpress.XtraNavBar.NavBarItem();
            this.nvbCanBole = new DevExpress.XtraNavBar.NavBarItem();
            this.nvbLerRetorno = new DevExpress.XtraNavBar.NavBarItem();
            this.nbiImpBol = new DevExpress.XtraNavBar.NavBarItem();
            this.nvbSalvarPDF = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem3 = new DevExpress.XtraNavBar.NavBarItem();
            this.gcNossoNumero = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.lciNavBotoes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DVView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navBotoes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repoImagensColunas)).BeginInit();
            this.SuspendLayout();
            // 
            // lciNavBotoes
            // 
            this.lciNavBotoes.Size = new System.Drawing.Size(118, 460);
            // 
            // DVView
            // 
            this.DVView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcImagem,
            this.gcAssociado,
            this.gcVencimento,
            this.gcValor,
            this.gcBoleto,
            this.gcNossoNumero});
            this.DVView.OptionsBehavior.Editable = false;
            this.DVView.OptionsFind.AlwaysVisible = true;
            this.DVView.OptionsFind.FindNullPrompt = "Digite aqui para pesquisar...";
            // 
            // DGGrid
            // 
            this.DGGrid.DataMember = null;
            this.DGGrid.DataSource = this.FonteDeDados;
            this.DGGrid.Location = new System.Drawing.Point(130, 12);
            this.DGGrid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repoImagensColunas});
            this.DGGrid.Size = new System.Drawing.Size(498, 456);
            // 
            // navBotoes
            // 
            this.navBotoes.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.navBarGroup1,
            this.navBarGroup2});
            this.navBotoes.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.nbiGerarContas,
            this.nbiBaixarContas,
            this.nbiCancelarBaixa,
            this.navBarItem2,
            this.nvbCanBole,
            this.nvbLerRetorno,
            this.nbiImpBol,
            this.nvbSalvarPDF});
            this.navBotoes.OptionsNavPane.ExpandedWidth = 114;
            this.navBotoes.Size = new System.Drawing.Size(114, 456);
            // 
            // gcImagem
            // 
            this.gcImagem.Caption = "S";
            this.gcImagem.ColumnEdit = this.repoImagensColunas;
            this.gcImagem.FieldName = "status";
            this.gcImagem.MaxWidth = 20;
            this.gcImagem.Name = "gcImagem";
            this.gcImagem.Visible = true;
            this.gcImagem.VisibleIndex = 0;
            this.gcImagem.Width = 20;
            // 
            // repoImagensColunas
            // 
            this.repoImagensColunas.AutoHeight = false;
            this.repoImagensColunas.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repoImagensColunas.Name = "repoImagensColunas";
            // 
            // gcVencimento
            // 
            this.gcVencimento.Caption = "Vencimento";
            this.gcVencimento.DisplayFormat.FormatString = "d";
            this.gcVencimento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gcVencimento.FieldName = "vencimento";
            this.gcVencimento.Name = "gcVencimento";
            this.gcVencimento.Visible = true;
            this.gcVencimento.VisibleIndex = 2;
            this.gcVencimento.Width = 54;
            // 
            // navBarGroup1
            // 
            this.navBarGroup1.Caption = "Operações";
            this.navBarGroup1.Expanded = true;
            this.navBarGroup1.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiGerarContas),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiBaixarContas),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiCancelarBaixa)});
            this.navBarGroup1.Name = "navBarGroup1";
            // 
            // nbiGerarContas
            // 
            this.nbiGerarContas.Caption = "Gerar Contas";
            this.nbiGerarContas.Name = "nbiGerarContas";
            this.nbiGerarContas.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.nbiGerarContas_LinkClicked);
            // 
            // nbiBaixarContas
            // 
            this.nbiBaixarContas.Caption = "Baixar Conta";
            this.nbiBaixarContas.Name = "nbiBaixarContas";
            this.nbiBaixarContas.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.nbiBaixarContas_LinkClicked);
            // 
            // nbiCancelarBaixa
            // 
            this.nbiCancelarBaixa.Caption = "Cancelar Baixa";
            this.nbiCancelarBaixa.Name = "nbiCancelarBaixa";
            this.nbiCancelarBaixa.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.nbiEnviarBoleto_LinkClicked);
            // 
            // gcValor
            // 
            this.gcValor.Caption = "Valor";
            this.gcValor.DisplayFormat.FormatString = "c2";
            this.gcValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gcValor.FieldName = "valor";
            this.gcValor.Name = "gcValor";
            this.gcValor.Visible = true;
            this.gcValor.VisibleIndex = 3;
            this.gcValor.Width = 39;
            // 
            // gcBoleto
            // 
            this.gcBoleto.Caption = "Boleto";
            this.gcBoleto.FieldName = "boleto";
            this.gcBoleto.Name = "gcBoleto";
            this.gcBoleto.Visible = true;
            this.gcBoleto.VisibleIndex = 5;
            this.gcBoleto.Width = 183;
            // 
            // gcAssociado
            // 
            this.gcAssociado.Caption = "Associado";
            this.gcAssociado.FieldName = "nome";
            this.gcAssociado.Name = "gcAssociado";
            this.gcAssociado.Visible = true;
            this.gcAssociado.VisibleIndex = 1;
            this.gcAssociado.Width = 110;
            // 
            // navBarGroup2
            // 
            this.navBarGroup2.Caption = "Boletos";
            this.navBarGroup2.Expanded = true;
            this.navBarGroup2.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem2),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nvbCanBole),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nvbLerRetorno),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiImpBol),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nvbSalvarPDF)});
            this.navBarGroup2.Name = "navBarGroup2";
            // 
            // navBarItem2
            // 
            this.navBarItem2.Caption = "Gerar Boleto";
            this.navBarItem2.Name = "navBarItem2";
            toolTipTitleItem1.Text = "Gerar Boleto";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Gera o boleto da conta";
            toolTipTitleItem2.LeftIndent = 6;
            toolTipTitleItem2.Text = "Boleto";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            superToolTip1.Items.Add(toolTipSeparatorItem1);
            superToolTip1.Items.Add(toolTipTitleItem2);
            this.navBarItem2.SuperTip = superToolTip1;
            this.navBarItem2.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem2_LinkClicked);
            // 
            // nvbCanBole
            // 
            this.nvbCanBole.Caption = "Cancelar Boleto";
            this.nvbCanBole.Name = "nvbCanBole";
            this.nvbCanBole.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.nvbCanBole_LinkClicked);
            // 
            // nvbLerRetorno
            // 
            this.nvbLerRetorno.Caption = "Ler Retorno";
            this.nvbLerRetorno.Name = "nvbLerRetorno";
            this.nvbLerRetorno.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.nvbLerRetorno_LinkClicked);
            // 
            // nbiImpBol
            // 
            this.nbiImpBol.Caption = "Imprimir Boleto";
            this.nbiImpBol.Name = "nbiImpBol";
            this.nbiImpBol.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.nbiImpBol_LinkClicked);
            // 
            // nvbSalvarPDF
            // 
            this.nvbSalvarPDF.Caption = "Salvar PDF Boleto";
            this.nvbSalvarPDF.Name = "nvbSalvarPDF";
            this.nvbSalvarPDF.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.nvbSalvarPDF_LinkClicked);
            // 
            // navBarItem3
            // 
            this.navBarItem3.Caption = "Cancelar boleto";
            this.navBarItem3.Name = "navBarItem3";
            // 
            // gcNossoNumero
            // 
            this.gcNossoNumero.Caption = "Nosso numero";
            this.gcNossoNumero.FieldName = "nosso_numero";
            this.gcNossoNumero.Name = "gcNossoNumero";
            this.gcNossoNumero.Visible = true;
            this.gcNossoNumero.VisibleIndex = 4;
            this.gcNossoNumero.Width = 78;
            // 
            // ViewContaReceber
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "ViewContaReceber";
            ((System.ComponentModel.ISupportInitialize)(this.lciNavBotoes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DVView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navBotoes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repoImagensColunas)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn gcImagem;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repoImagensColunas;
        private DevExpress.XtraGrid.Columns.GridColumn gcVencimento;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup1;
        private DevExpress.XtraNavBar.NavBarItem nbiGerarContas;
        private DevExpress.XtraNavBar.NavBarItem nbiBaixarContas;
        private DevExpress.XtraNavBar.NavBarItem nbiCancelarBaixa;
        private DevExpress.XtraGrid.Columns.GridColumn gcValor;
        private DevExpress.XtraGrid.Columns.GridColumn gcBoleto;
        private DevExpress.XtraGrid.Columns.GridColumn gcAssociado;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup2;
        private DevExpress.XtraNavBar.NavBarItem navBarItem2;
        private DevExpress.XtraNavBar.NavBarItem nvbCanBole;
        private DevExpress.XtraNavBar.NavBarItem navBarItem3;
        private DevExpress.XtraNavBar.NavBarItem nvbLerRetorno;
        private DevExpress.XtraNavBar.NavBarItem nbiImpBol;
        private DevExpress.XtraNavBar.NavBarItem nvbSalvarPDF;
        private DevExpress.XtraGrid.Columns.GridColumn gcNossoNumero;
    }
}
