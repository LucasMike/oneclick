﻿using Projeto.Controles.CRUD;

namespace Projeto.Controles.Views
{
    public partial class ViewBoleto : ViewBase
    {
        public override string Titulo => "Boleto";

        protected override FrmCRUDBase FormCRUD
        {
            get
            {
                return new FrmCRUDBoleto();
            }
        }

        public ViewBoleto(string sTabela) :base(sTabela)
        {
            InitializeComponent();
        }

        protected override void CarregarDados()
        {
            InicializarFonteDeDados();
            CriarConsulta("id", "num_banco", "agencia", "banco", "cedente", "carteira", "layout", "aceite", "status");
            base.CarregarDados();
        }
    }
}
