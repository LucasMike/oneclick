﻿using Projeto.Controles.CRUD;

namespace Projeto.Controles.Views
{
    public partial class ViewAssociado : ViewBase
    {
        public override string Titulo => "Associado";

        protected override FrmCRUDBase FormCRUD
        {
            get
            {
                return new FrmCRUDAssociado();
            }
        }

        public ViewAssociado(string sTabela) : base(sTabela)
        {
            InitializeComponent();
        }

        protected override void CarregarDados()
        {
            InicializarFonteDeDados();
            CriarConsulta("id", "nome", "cpf", "telefone", "email", "endereco", "anexo", "dataini", "datafim");
            base.CarregarDados();
        }
    }
}
