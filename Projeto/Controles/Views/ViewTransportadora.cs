﻿using Projeto.Controles.CRUD;

namespace Projeto.Controles.Views
{
    public partial class ViewTransportadora : ViewBase
    {
        public override string Titulo => "Transportadoras";

        protected override FrmCRUDBase FormCRUD
        {
            get
            {
                return new FrmCRUDTransportadora();
            }
        }

        public ViewTransportadora(string sTabela) : base(sTabela)
        {
            InitializeComponent();
        }

        protected override void CarregarDados()
        {
            InicializarFonteDeDados();
            CriarConsulta("id", "nome", "telefone");
            base.CarregarDados();
        }        
    }
}
