﻿
using Projeto.Controles.CRUD;

namespace Projeto.Controles.Views
{
    public partial class ViewUsuario : ViewBase
    {
        public override string Titulo => "Usuários";

        protected override FrmCRUDBase FormCRUD
        {
            get
            {
                return new FrmCRUDUsuario();
            }
        }

        public ViewUsuario(string sTabela) : base(sTabela)
        {
            InitializeComponent();
        }

        protected override void CarregarDados()
        {
            InicializarFonteDeDados();
            CriarConsulta("id", "nome", "usuario", "senha");
            base.CarregarDados();
        }
    }
}
