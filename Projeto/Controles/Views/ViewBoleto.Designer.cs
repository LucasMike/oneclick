﻿namespace Projeto.Controles.Views
{
    partial class ViewBoleto
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this.lciNavBotoes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DVView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navBotoes)).BeginInit();
            this.SuspendLayout();
            // 
            // DVView
            // 
            this.DVView.OptionsBehavior.Editable = false;
            this.DVView.OptionsFind.AlwaysVisible = true;
            this.DVView.OptionsFind.FindNullPrompt = "Digite aqui para pesquisar...";
            // 
            // DGGrid
            // 
            this.DGGrid.DataMember = null;
            this.DGGrid.DataSource = this.FonteDeDados;
            // 
            // navBotoes
            // 
            this.navBotoes.OptionsNavPane.ExpandedWidth = 112;
            // 
            // ViewBoleto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "ViewBoleto";
            ((System.ComponentModel.ISupportInitialize)(this.lciNavBotoes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DVView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navBotoes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
    }
}
