﻿namespace Projeto.Controles.Views
{
    partial class ViewBase
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lcControleLayout = new DevExpress.XtraLayout.LayoutControl();
            this.navBotoes = new DevExpress.XtraNavBar.NavBarControl();
            this.navGrupoBotoes = new DevExpress.XtraNavBar.NavBarGroup();
            this.navItemVisualizar = new DevExpress.XtraNavBar.NavBarItem();
            this.navItemInserir = new DevExpress.XtraNavBar.NavBarItem();
            this.navItemAlterar = new DevExpress.XtraNavBar.NavBarItem();
            this.navItemRemover = new DevExpress.XtraNavBar.NavBarItem();
            this.DGGrid = new DevExpress.XtraGrid.GridControl();
            this.DVView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciNavBotoes = new DevExpress.XtraLayout.LayoutControlItem();
            this.FonteDeDados = new DevExpress.DataAccess.Sql.SqlDataSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.lcControleLayout)).BeginInit();
            this.lcControleLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.navBotoes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DVView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciNavBotoes)).BeginInit();
            this.SuspendLayout();
            // 
            // lcControleLayout
            // 
            this.lcControleLayout.Controls.Add(this.navBotoes);
            this.lcControleLayout.Controls.Add(this.DGGrid);
            this.lcControleLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcControleLayout.Location = new System.Drawing.Point(0, 0);
            this.lcControleLayout.Name = "lcControleLayout";
            this.lcControleLayout.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(726, 201, 450, 400);
            this.lcControleLayout.Root = this.layoutControlGroup1;
            this.lcControleLayout.Size = new System.Drawing.Size(640, 480);
            this.lcControleLayout.TabIndex = 0;
            this.lcControleLayout.Text = "layoutControl1";
            // 
            // navBotoes
            // 
            this.navBotoes.ActiveGroup = this.navGrupoBotoes;
            this.navBotoes.DragDropFlags = DevExpress.XtraNavBar.NavBarDragDrop.None;
            this.navBotoes.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.navGrupoBotoes});
            this.navBotoes.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.navItemVisualizar,
            this.navItemInserir,
            this.navItemAlterar,
            this.navItemRemover});
            this.navBotoes.Location = new System.Drawing.Point(12, 12);
            this.navBotoes.Name = "navBotoes";
            this.navBotoes.OptionsNavPane.ExpandedWidth = 112;
            this.navBotoes.Size = new System.Drawing.Size(112, 456);
            this.navBotoes.TabIndex = 5;
            // 
            // navGrupoBotoes
            // 
            this.navGrupoBotoes.Caption = "Registros";
            this.navGrupoBotoes.Expanded = true;
            this.navGrupoBotoes.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navItemVisualizar),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navItemInserir),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navItemAlterar),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navItemRemover)});
            this.navGrupoBotoes.Name = "navGrupoBotoes";
            // 
            // navItemVisualizar
            // 
            this.navItemVisualizar.Caption = "Visualizar";
            this.navItemVisualizar.Name = "navItemVisualizar";
            this.navItemVisualizar.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navItemVisualizar_LinkClicked);
            // 
            // navItemInserir
            // 
            this.navItemInserir.Caption = "Inserir";
            this.navItemInserir.Name = "navItemInserir";
            this.navItemInserir.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navItemInserir_LinkClicked);
            // 
            // navItemAlterar
            // 
            this.navItemAlterar.Caption = "Alterar";
            this.navItemAlterar.Name = "navItemAlterar";
            this.navItemAlterar.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navItemAlterar_LinkClicked);
            // 
            // navItemRemover
            // 
            this.navItemRemover.Caption = "Remover";
            this.navItemRemover.Name = "navItemRemover";
            this.navItemRemover.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navItemRemover_LinkClicked);
            // 
            // DGGrid
            // 
            this.DGGrid.Location = new System.Drawing.Point(128, 12);
            this.DGGrid.MainView = this.DVView;
            this.DGGrid.Name = "DGGrid";
            this.DGGrid.Size = new System.Drawing.Size(500, 456);
            this.DGGrid.TabIndex = 4;
            this.DGGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.DVView});
            this.DGGrid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DGGrid_KeyDown);
            // 
            // DVView
            // 
            this.DVView.GridControl = this.DGGrid;
            this.DVView.Name = "DVView";
            this.DVView.OptionsBehavior.Editable = false;
            this.DVView.OptionsFind.AlwaysVisible = true;
            this.DVView.OptionsFind.FindNullPrompt = "Digite aqui para pesquisar...";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.lciNavBotoes});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(640, 480);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.DGGrid;
            this.layoutControlItem1.Location = new System.Drawing.Point(116, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(504, 460);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // lciNavBotoes
            // 
            this.lciNavBotoes.Control = this.navBotoes;
            this.lciNavBotoes.Location = new System.Drawing.Point(0, 0);
            this.lciNavBotoes.Name = "layoutControlItem2";
            this.lciNavBotoes.Size = new System.Drawing.Size(116, 460);
            this.lciNavBotoes.TextSize = new System.Drawing.Size(0, 0);
            this.lciNavBotoes.TextVisible = false;
            // 
            // FonteDeDados
            // 
            this.FonteDeDados.Name = "FonteDeDados";
            // 
            // ViewBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lcControleLayout);
            this.MinimumSize = new System.Drawing.Size(640, 480);
            this.Name = "ViewBase";
            this.Size = new System.Drawing.Size(640, 480);
            this.Load += new System.EventHandler(this.ViewBase_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lcControleLayout)).EndInit();
            this.lcControleLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.navBotoes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DVView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciNavBotoes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl lcControleLayout;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraNavBar.NavBarGroup navGrupoBotoes;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraNavBar.NavBarItem navItemVisualizar;
        private DevExpress.XtraNavBar.NavBarItem navItemInserir;
        private DevExpress.XtraNavBar.NavBarItem navItemAlterar;
        private DevExpress.XtraNavBar.NavBarItem navItemRemover;
        protected DevExpress.DataAccess.Sql.SqlDataSource FonteDeDados;
        protected DevExpress.XtraLayout.LayoutControlItem lciNavBotoes;
        protected DevExpress.XtraGrid.Views.Grid.GridView DVView;
        protected DevExpress.XtraGrid.GridControl DGGrid;
        protected DevExpress.XtraNavBar.NavBarControl navBotoes;
    }
}
