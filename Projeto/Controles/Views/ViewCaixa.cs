﻿using DevExpress.DataAccess.Sql;
using Projeto.Controles.CRUD;

namespace Projeto.Controles.Views
{
    public partial class ViewCaixa : ViewBase
    {
        public override string Titulo => "Caixa";

        protected override FrmCRUDBase FormCRUD
        {
            get
            {
                return new FrmCRUDCaixa();
            }
        }

        public ViewCaixa(string sTabela) : base(sTabela)
        {
            InitializeComponent();

            lciNavBotoes.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
        }

        protected override void CarregarDados()
        {
            InicializarFonteDeDados();

            CustomSqlQuery cus = new CustomSqlQuery();
            cus.Name = Tabela;
            cus.Sql = @"select
                (select coalesce(sum(valor), 0) from conta_receber where status = 'F') -
                (select coalesce(sum(valor), 0) from conta_pagar where status = 'F') as " + "\"Valor em Caixa\"";
            FonteDeDados.Queries.Add(cus);

            base.CarregarDados();
        }
    }
}
