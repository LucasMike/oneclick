﻿using DevExpress.DataAccess.Sql;
using Projeto.Controles.CRUD;

namespace Projeto.Controles.Views
{
    public partial class ViewDestino : ViewBase
    {
        public override string Titulo => "Destino";

        protected override FrmCRUDBase FormCRUD
        {
            get
            {
                return new FrmCRUDDestino();
            }
        }
        
        public ViewDestino(string sTabela) : base(sTabela)
        {
            InitializeComponent();
        }

        protected override void CarregarDados()
        {
            InicializarFonteDeDados();
            CriarConsulta("id", "universidade", "cidade_id");
            CriarRelacao("cidade", new RelationColumnInfo("cidade_id", "id"), "nome");
            base.CarregarDados();
        }
    }
}
