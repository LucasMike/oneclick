﻿using DevExpress.DataAccess.Sql;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraLayout;
using DevExpress.XtraWaitForm;
using Projeto.Classes;
using Projeto.Controles.CRUD;
using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using Config = Projeto.Properties.Settings;


namespace Projeto.Controles.Views
{
    public partial class ViewBase : XtraUserControl
    {
        #region Propriedades Publicas

        public virtual string Titulo => "inserir_o_titulo";

        public string Tabela { get; protected set; }

        #endregion

        #region Variaveis Privadas

        protected virtual FrmCRUDBase FormCRUD { get; }

        #endregion

        #region Construtor

        private ViewBase()
        {
            InitializeComponent();
        }

        protected ViewBase(string sTabela)
        {
            InitializeComponent();
            Tabela = sTabela;
            Dock = DockStyle.Fill;

            Disposed += ViewBase_Disposed;
        }

        #endregion

        #region Metodos Protegidos

        protected int IDLinhaSelecionada => Convert.ToInt32(DVView.GetFocusedRowCellValue("id"));

        protected void InicializarFonteDeDados()
        {
            FonteDeDados.ConnectionParameters = new DevExpress.DataAccess.ConnectionParameters.PostgreSqlConnectionParameters(
                Config.Default.DB_IP,
                Config.Default.DB_PORTA,
                Config.Default.DB_BANCO,
                Config.Default.DB_USER,
                Config.Default.DB_SENHA
            );
            FonteDeDados.ConnectionName = "OneClick";
        }

        /// <summary>
        /// Criar a cosutla para carregar os dados
        /// </summary>
        /// <param name="campos">Campos para consltar</param>
        protected void CriarConsulta(params string[] campos)
        {
            SelectQuery query = new SelectQuery();
            Table tab = query.AddTable(Tabela);
            query.SelectColumns(tab, campos);
            query.Name = Tabela;
            query.SortBy(tab, "id");
            FonteDeDados.Queries.Add(query);
        }

        protected void CriarRelacao(string tabela, RelationColumnInfo info, params string[] campos)
        {
            SelectQuery query = (SelectQuery)FonteDeDados.Queries[0];
            Table tab = query.AddTable(tabela);
            query.AddRelation(
                query.Tables[0],
                tab,
                info
            );
            query.SelectColumns(tab, campos);
        }

        protected void Remover()
        {
            if (DVView.IsValidRowHandle(DVView.FocusedRowHandle))
            {
                if (Conexao.Instancia.ExecutarSQL(
                    $"delete from {Tabela} where id = {DVView.GetRowCellValue(DVView.FocusedRowHandle, "id")}"
                ) > 0)
                {
                    DVView.DeleteRow(DVView.FocusedRowHandle);
                }

                FonteDeDados.Fill();

                DVView.MoveFirst();
                DVView.Focus();
            }
        }

        protected void ProcessarEmThread(string titulo, Action acao)
        {
            ManualResetEvent mre = new ManualResetEvent(false);
            ProgressPanel progress = new ProgressPanel();
            progress.Location = new Point((Width - progress.Width) / 2, (Height - progress.Height) / 2);
            progress.Visible = true;
            progress.WaitAnimationType = DevExpress.Utils.Animation.WaitingAnimatorType.Bar;
            progress.Caption = titulo;
            progress.Description = "Por favor aguarde...";
            progress.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;

            Controls.Add(progress);
            Controls.SetChildIndex(progress, 0);

            ThreadPool.QueueUserWorkItem((_) =>
            {
                acao();
                mre.Set();
            });
            ThreadPool.QueueUserWorkItem((_) =>
            {
                mre.WaitOne();
                progress.Invoke((MethodInvoker)delegate
                {
                    Controls.Remove(progress);
                    progress.Visible = false;
                    progress.Dispose();
                });
            });

        }

        protected void ProcessarEmThread<T>(string titulo, Action<T> acao, T arg)
        {
            ManualResetEvent mre = new ManualResetEvent(false);
            ProgressPanel progress = new ProgressPanel();
            progress.Location = new Point((Width - progress.Width) / 2, (Height - progress.Height) / 2);
            progress.Visible = true;
            progress.WaitAnimationType = DevExpress.Utils.Animation.WaitingAnimatorType.Bar;
            progress.Caption = titulo;
            progress.Description = "Por favor aguarde...";
            progress.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;

            Controls.Add(progress);
            Controls.SetChildIndex(progress, 0);

            ThreadPool.QueueUserWorkItem((_) =>
            {
                acao(arg);
                mre.Set();
            });
            ThreadPool.QueueUserWorkItem((_) =>
            {
                mre.WaitOne();
                progress.Invoke((MethodInvoker)delegate
               {
                   Controls.Remove(progress);
                   progress.Visible = false;
                   progress.Dispose();
               });
            });

        }

        #endregion

        #region Metodos Virtual

        protected virtual void CarregarDados()
        {
            if (FonteDeDados.Connection != null)
            {
                FonteDeDados.Fill();
            }

            DGGrid.DataSource = FonteDeDados;
            DGGrid.DataMember = Tabela;
        }

        #endregion

        #region Eventos Controles

        private void navItemVisualizar_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (DVView.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                FrmCRUDBase frmCRUD = FormCRUD;
                frmCRUD.CarregarDados(IDLinhaSelecionada);
                frmCRUD.SetaHabilitadoBotaoSalvar(false);

                Controls.Add(frmCRUD);
                Controls.SetChildIndex(frmCRUD, 0);

                foreach (Control control in frmCRUD.Controls)
                {
                    LayoutControl lc = control as LayoutControl;

                    if (lc != null)
                    {
                        foreach (Control c in lc.Controls)
                        {
                            BaseEdit be = c as BaseEdit;
                            if (be != null)
                            {
                                be.ReadOnly = true;
                            }
                        }

                        break;
                    }
                }

                frmCRUD.Show();
            }
        }

        private void navItemInserir_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            FrmCRUDBase frmCRUD = FormCRUD;
            frmCRUD.CarregarDados(FrmCRUDBase.ID_INSERINDO);

            Controls.Add(frmCRUD);
            Controls.SetChildIndex(frmCRUD, 0);
            frmCRUD.FormClosed += (s, ev) =>
            {
                if (frmCRUD.DialogResult == DialogResult.OK)
                {
                    AtualizarDados();
                }
            };
            frmCRUD.Show();
        }

        private void navItemAlterar_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (DVView.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                FrmCRUDBase frmCRUD = FormCRUD;
                frmCRUD.CarregarDados(IDLinhaSelecionada);

                Controls.Add(frmCRUD);
                Controls.SetChildIndex(frmCRUD, 0);
                frmCRUD.FormClosed += (s, ev) =>
                {
                    if (frmCRUD.DialogResult == DialogResult.OK)
                    {
                        AtualizarDados();
                    }
                };
                frmCRUD.Show();
            }
        }

        private void navItemRemover_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            Remover();
        }

        private void ViewBase_Load(object sender, System.EventArgs e)
        {
            CarregarDados();
        }

        private void ViewBase_Disposed(object sender, System.EventArgs e)
        {
            FonteDeDados.ClearRows();
            FonteDeDados.Dispose();

            System.GC.Collect();
        }

        #endregion

        #region Metodos Estaticos

        public static ViewBase ObterViewReferenteTabela(string sTabela)
        {
            switch (sTabela)
            {
                case "usuario":
                    return new ViewUsuario(sTabela);

                case "associado":
                    return new ViewAssociado(sTabela);

                case "transportadora":
                    return new ViewTransportadora(sTabela);

                case "caixa":
                    return new ViewCaixa(sTabela);

                case "cidade":
                    return new ViewCidade(sTabela);

                case "boleto":
                    return new ViewBoleto(sTabela);

                case "conta_receber":
                    return new ViewContaReceber(sTabela);

                case "conta_pagar":
                    return new ViewContaPagar(sTabela);

                case "destino":
                    return new ViewDestino(sTabela);

                case "destino_transportadora":
                    return new ViewDestinoTransportadora(sTabela);

                default:
                    return null;
            }
        }

        internal void AtualizarDados()
        {
            if (InvokeRequired)
            {
                Invoke((MethodInvoker)delegate
               {
                   AtualizarDados();
               });
            }
            else
            {
                int iPos = DVView.FocusedRowHandle;
                FonteDeDados.Fill();

                DVView.FocusedRowHandle = iPos;
            }
        }

        #endregion

        private void DGGrid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                FonteDeDados.Fill();
            }
        }
    }
}
