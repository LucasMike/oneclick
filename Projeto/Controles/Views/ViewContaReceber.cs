﻿using DevExpress.DataAccess.Sql;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraNavBar;
using Projeto.Classes;
using Projeto.Controles.CRUD;
using Projeto.Forms;
using Projeto.Relatorios;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Projeto.Controles.Views
{
    public partial class ViewContaReceber : ViewBase
    {
        public override string Titulo => "Contas a Receber";

        protected override FrmCRUDBase FormCRUD
        {
            get
            {
                return new FrmCRUDContaReceber();
            }
        }

        public ViewContaReceber(string sTabela) : base(sTabela)
        {
            InitializeComponent();

            DevExpress.Utils.ImageCollection imageCollection1 = new DevExpress.Utils.ImageCollection();

            foreach (Image img in new Image[] { Properties.Resources.Apply_16x16, Properties.Resources.Cancel_16x16 })
            {
                imageCollection1.AddImage(img);
            }

            repoImagensColunas.SmallImages = imageCollection1;
            repoImagensColunas.Items.Add(new ImageComboBoxItem("F", 0) { Description = null });
            repoImagensColunas.Items.Add(new ImageComboBoxItem("A", 1) { Description = null });
        }

        protected override void CarregarDados()
        {
            InicializarFonteDeDados();
            CriarConsulta("id", "vencimento", "associado_id", "valor", "boleto_id", "boleto", "status", "nosso_numero");
            CriarRelacao("associado", new RelationColumnInfo("associado_id", "id"), "nome");
            base.CarregarDados();
        }

        private void nbiBaixarContas_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            if (DVView.IsValidRowHandle(DVView.FocusedRowHandle) && DVView.GetRowCellValue(DVView.FocusedRowHandle, "status").ToString() == "A")
            {
                Conexao.Instancia.ExecutarSQL($"update conta_receber set status = 'F' where id = {IDLinhaSelecionada}");
                MessageBox.Show(
                    $"Conta baixada com sucesso!",
                    "Baixar conta",
                    MessageBoxButtons.OK
                );
                AtualizarDados();
            }
            else
            {
                MessageBox.Show(
                    $"Esta conta já esta Baixada!",
                    "Baixar conta",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information
                );
            }
        }

        private void nbiEnviarBoleto_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            if (DVView.IsValidRowHandle(DVView.FocusedRowHandle) && DVView.GetRowCellValue(DVView.FocusedRowHandle, "status").ToString() == "F")
            {
                Conexao.Instancia.ExecutarSQL($"update conta_receber set status = 'A' where id = {IDLinhaSelecionada}");
                MessageBox.Show(
                    $"Baixa cancelada com sucesso!",
                    "Cancelar baixa",
                    MessageBoxButtons.OK
                );
                AtualizarDados();
            }
            else
            {
                MessageBox.Show(
                    $"Esta conta já esta aberta!",
                    "Cancelar baixa",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information
                );
            }
        }

        private void nbiGerarContas_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            FrmParametrosGerarContas frm = new FrmParametrosGerarContas();

            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                ProcessarEmThread("Gerando contas...", GerarContas, frm);
            }
            AtualizarDados();
        }

        private void navBarItem2_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            DataTable dtDados = Conexao.Instancia.Consultar("select id from conta_receber where nosso_numero is null and associado_id <> 0");
            if (dtDados.Rows.Count > 0)
            {
                int[] ids = new int[dtDados.Rows.Count];

                for (int i = 0; i < dtDados.Rows.Count; ++i)
                {
                    ids[i] = Convert.ToInt32(dtDados.Rows[i][0]);
                }

                BancoBase.ObterBancoBase(Convert.ToInt32(DVView.GetFocusedRowCellValue("boleto_id")))?.GerarRemessa(ids);
                AtualizarDados();
            }
        }

        private void nbiImpBol_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            if (DVView.IsValidRowHandle(DVView.FocusedRowHandle) && DVView.GetRowCellValue(DVView.FocusedRowHandle, "nosso_numero").ToString() != "")
            {
                Boleto bol = new Boleto(IDLinhaSelecionada);
                bol.SHowPreview();
                bol.Dispose();
            }
            else
            {
                MessageBox.Show(
                    $"Conta a receber selecionada não possui boleto gerado!",
                    "Imprimir boleto",
                    MessageBoxButtons.OK
                );
            }
        }

        private void nvbSalvarPDF_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            FrmSelecionarData frm = new FrmSelecionarData();

            if (frm.ShowDialog() == DialogResult.OK)
            {
                ProcessarEmThread("Gerando boletos", GerarBoletos, frm);
            }
        }

        private void nvbLerRetorno_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            BancoBase.ObterBancoBase(Convert.ToInt32(DVView.GetFocusedRowCellValue("boleto_id")))?.LerRetorno();
            MessageBox.Show(
                    $"O retorno foi lido com sucesso!",
                    "Ler retorno",
                    MessageBoxButtons.OK
                );
            AtualizarDados();
        }

        private void nvbCanBole_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            if (DVView.IsValidRowHandle(DVView.FocusedRowHandle) && DVView.GetRowCellValue(DVView.FocusedRowHandle, "status").ToString() == "A" && DVView.GetRowCellValue(DVView.FocusedRowHandle, "nosso_numero").ToString() != "")
            {
                Conexao.Instancia.ExecutarSQL($"update conta_receber set nosso_numero = '', boleto = '' where id = {IDLinhaSelecionada}");
                MessageBox.Show(
                    $"O boleto foi cancelado com sucesso!",
                    "Cancelar boleto",
                    MessageBoxButtons.OK
                );
                AtualizarDados();
            }
            else if(DVView.GetRowCellValue(DVView.FocusedRowHandle, "status").ToString() == "F")
            {
                MessageBox.Show(
                    $"A conta já está baixada, cancele a baixa primeiro",
                    "Cancelar boleto",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information
                    );
            }
            else
            {
                MessageBox.Show(
                    $"Este boleto já esta cancelado",
                    "Cancelar boleto",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information
                    );
            }
        }

        private void GerarContas(FrmParametrosGerarContas frm)
        {
            Conexao.Instancia.ExecutarSQL($@"WITH valor_caixa AS
                                          ( SELECT
                                             (SELECT coalesce(sum(valor), 0)
                                              FROM conta_receber
                                              WHERE status = 'F') -
                                             (SELECT coalesce(sum(valor), 0)
                                              FROM conta_pagar
                                              WHERE status = 'F') AS val )
                                        INSERT INTO conta_receber(vencimento, valor, associado_id, boleto_id, status)
                                        SELECT '{frm.DataVencimento.ToString("yyyy-MM-dd")}',
                                               (ad.dias * dt.valordia -
                                                  (SELECT *
                                                   FROM valor_caixa) /
                                                  (SELECT count(*)
                                                   FROM associado)) AS valor,
                                               a.id,
                                               1,
                                               'A'
                                        FROM associado a
                                        INNER JOIN destino_transportadora dt ON (dt.id = a.destino_transportadora_id)
                                        INNER JOIN
                                          ( SELECT associado_id,
                                                   sum(qtd) AS dias
                                           FROM associado_dias a,

                                             (SELECT EXTRACT(DOW
                                                             FROM mydate) AS dia_semana,
                                                     COUNT(*) AS qtd
                                              FROM generate_series(TIMESTAMP '{frm.DataInicio.ToString("yyyy-MM-dd")}', 
                                                '{frm.DataFim.ToString("yyyy-MM-dd")}', '1 day') AS g(mydate)
                                              GROUP BY 1
                                              ORDER BY 1) d
                                           WHERE dia_semana = (a.dia -1)
                                           GROUP BY associado_id) AS ad ON (ad.associado_id = a.id)
                                        WHERE a.id <> 0
                                        GROUP BY a.id,
                                                 dt.valordia,
                                                 ad.dias");
            MessageBox.Show(
                $"As contas foram geradas com sucesso!",
                "Gerar contas",
                MessageBoxButtons.OK
            );
            AtualizarDados();
        }

        private void GerarBoletos(FrmSelecionarData frm)
        {
            DataTable dtDados = Conexao.Instancia.Consultar($@"select c.id, a.nome, c.vencimento
                from conta_receber c
                inner join associado a on (a.id = associado_id)
                where nosso_numero is not null
                and emissao between '{frm.DataInicio.ToString("yyyy-MM-dd")}' and '{frm.DataFim.ToString("yyyy-MM-dd")}'
                and associado_id <> 0
                and c.status = 'A'"
            );
            string sCamihoBase = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                "Boletos Associados"
            );
            Directory.CreateDirectory(sCamihoBase);

            foreach (DataRow dr in dtDados.Rows)
            {
                string sDirPastaUsuario = Path.Combine(sCamihoBase, dr["nome"].ToString());
                Directory.CreateDirectory(sDirPastaUsuario);

                Boleto bol = new Boleto(IDLinhaSelecionada);
                bol.ExportToPdf(
                    Path.Combine(sDirPastaUsuario, $"{Convert.ToDateTime(dr["vencimento"]).ToString("dd_MM_yyyy")}.pdf")
                );
            }

            MessageBox.Show(
                $"Boletos gerados com sucesso na pasta {sCamihoBase}",
                "Boletos Gerados",
                MessageBoxButtons.OK
            );
        }
    }
}
