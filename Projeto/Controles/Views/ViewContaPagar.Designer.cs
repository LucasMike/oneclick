﻿namespace Projeto.Controles.Views
{
    partial class ViewContaPagar
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcTransportadora = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcVencimento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcValor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.navBarGroup1 = new DevExpress.XtraNavBar.NavBarGroup();
            this.nbiBaixarContas = new DevExpress.XtraNavBar.NavBarItem();
            this.nbiCancelarBaixa = new DevExpress.XtraNavBar.NavBarItem();
            this.gcStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repoImagensColunas = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.lciNavBotoes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DVView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navBotoes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repoImagensColunas)).BeginInit();
            this.SuspendLayout();
            // 
            // DVView
            // 
            this.DVView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcStatus,
            this.gcTransportadora,
            this.gcVencimento,
            this.gcValor});
            this.DVView.OptionsBehavior.Editable = false;
            this.DVView.OptionsFind.AlwaysVisible = true;
            this.DVView.OptionsFind.FindNullPrompt = "Digite aqui para pesquisar...";
            // 
            // DGGrid
            // 
            this.DGGrid.DataMember = null;
            this.DGGrid.DataSource = this.FonteDeDados;
            this.DGGrid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repoImagensColunas});
            // 
            // navBotoes
            // 
            this.navBotoes.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.navBarGroup1});
            this.navBotoes.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.nbiBaixarContas,
            this.nbiCancelarBaixa});
            this.navBotoes.OptionsNavPane.ExpandedWidth = 112;
            // 
            // gcTransportadora
            // 
            this.gcTransportadora.Caption = "Transportador";
            this.gcTransportadora.FieldName = "nome";
            this.gcTransportadora.Name = "gcTransportadora";
            this.gcTransportadora.Visible = true;
            this.gcTransportadora.VisibleIndex = 1;
            this.gcTransportadora.Width = 135;
            // 
            // gcVencimento
            // 
            this.gcVencimento.Caption = "Vencimento";
            this.gcVencimento.FieldName = "vencimento";
            this.gcVencimento.Name = "gcVencimento";
            this.gcVencimento.Visible = true;
            this.gcVencimento.VisibleIndex = 2;
            this.gcVencimento.Width = 158;
            // 
            // gcValor
            // 
            this.gcValor.Caption = "Valor";
            this.gcValor.DisplayFormat.FormatString = "c2";
            this.gcValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gcValor.FieldName = "valor";
            this.gcValor.Name = "gcValor";
            this.gcValor.Visible = true;
            this.gcValor.VisibleIndex = 3;
            this.gcValor.Width = 169;
            // 
            // navBarGroup1
            // 
            this.navBarGroup1.Caption = "Operações";
            this.navBarGroup1.Expanded = true;
            this.navBarGroup1.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiBaixarContas),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiCancelarBaixa)});
            this.navBarGroup1.Name = "navBarGroup1";
            // 
            // nbiBaixarContas
            // 
            this.nbiBaixarContas.Caption = "Baixar";
            this.nbiBaixarContas.Name = "nbiBaixarContas";
            this.nbiBaixarContas.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.nbiBaixarContas_LinkClicked);
            // 
            // nbiCancelarBaixa
            // 
            this.nbiCancelarBaixa.Caption = "Cancelar Baixa";
            this.nbiCancelarBaixa.Name = "nbiCancelarBaixa";
            this.nbiCancelarBaixa.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.nbiCancelarBaixa_LinkClicked);
            // 
            // gcStatus
            // 
            this.gcStatus.Caption = "S";
            this.gcStatus.ColumnEdit = this.repoImagensColunas;
            this.gcStatus.FieldName = "status";
            this.gcStatus.Name = "gcStatus";
            this.gcStatus.Visible = true;
            this.gcStatus.VisibleIndex = 0;
            this.gcStatus.Width = 20;
            // 
            // repoImagensColunas
            // 
            this.repoImagensColunas.AutoHeight = false;
            this.repoImagensColunas.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repoImagensColunas.Name = "repoImagensColunas";
            // 
            // ViewContaPagar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "ViewContaPagar";
            ((System.ComponentModel.ISupportInitialize)(this.lciNavBotoes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DVView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navBotoes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repoImagensColunas)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn gcTransportadora;
        private DevExpress.XtraGrid.Columns.GridColumn gcVencimento;
        private DevExpress.XtraGrid.Columns.GridColumn gcValor;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup1;
        private DevExpress.XtraNavBar.NavBarItem nbiBaixarContas;
        private DevExpress.XtraNavBar.NavBarItem nbiCancelarBaixa;
        private DevExpress.XtraGrid.Columns.GridColumn gcStatus;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repoImagensColunas;
    }
}
