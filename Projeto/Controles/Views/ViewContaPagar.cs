﻿using DevExpress.DataAccess.Sql;
using DevExpress.XtraEditors.Controls;
using Projeto.Classes;
using Projeto.Controles.CRUD;
using System.Drawing;
using System.Windows.Forms;

namespace Projeto.Controles.Views
{
    public partial class ViewContaPagar : ViewBase
    {
        public override string Titulo => "Contas a Pagar";

        protected override FrmCRUDBase FormCRUD
        {
            get
            {
                return new FrmCRUDContaPagar();
            }
        }

        public ViewContaPagar(string sTabela) : base(sTabela)
        {
            InitializeComponent();

            DevExpress.Utils.ImageCollection imageCollection1 = new DevExpress.Utils.ImageCollection();

            foreach (Image img in new Image[] { Properties.Resources.Apply_16x16, Properties.Resources.Cancel_16x16 })
            {
                imageCollection1.AddImage(img);
            }

            repoImagensColunas.SmallImages = imageCollection1;
            repoImagensColunas.Items.Add(new ImageComboBoxItem("F", 0) { Description = null });
            repoImagensColunas.Items.Add(new ImageComboBoxItem("A", 1) { Description = null });
        }

        protected override void CarregarDados()
        {
            InicializarFonteDeDados();
            CriarConsulta("id", "vencimento", "valor", "transportadora_id", "status");
            CriarRelacao("transportadora", new RelationColumnInfo("transportadora_id", "id"), "nome");
            base.CarregarDados();
        }

        private void nbiBaixarContas_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (DVView.IsValidRowHandle(DVView.FocusedRowHandle) && DVView.GetRowCellValue(DVView.FocusedRowHandle, "status").ToString() == "A")
            {
                Conexao.Instancia.ExecutarSQL($"update conta_pagar set status = 'F' where id = {IDLinhaSelecionada}");
                MessageBox.Show(
                    $"Conta baixada com sucesso!",
                    "Baixar conta",
                    MessageBoxButtons.OK
                );
                AtualizarDados();
            }
            else
            {
                MessageBox.Show(
                    $"Esta conta já esta Baixada!",
                    "Baixar conta",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information
                );
            }
        }

        private void nbiCancelarBaixa_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (DVView.IsValidRowHandle(DVView.FocusedRowHandle) && DVView.GetRowCellValue(DVView.FocusedRowHandle, "status").ToString() == "F")
            {
                Conexao.Instancia.ExecutarSQL($"update conta_pagar set status = 'A' where id = {IDLinhaSelecionada}");
                MessageBox.Show(
                    $"Baixa cancelada com sucesso!",
                    "Cancelar baixa",
                    MessageBoxButtons.OK
                );
                AtualizarDados();
            }
            else
            {
                MessageBox.Show(
                    $"Esta conta já esta aberta!",
                    "Cancelar baixa",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information
                );
            }
        }
    }
}
