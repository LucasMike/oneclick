﻿using DevExpress.DataAccess.Sql;
using Projeto.Controles.CRUD;

namespace Projeto.Controles.Views
{
    public partial class ViewDestinoTransportadora : ViewBase
    {
        public override string Titulo => "Viagens";

        protected override FrmCRUDBase FormCRUD
        {
            get
            {
                return new FrmCRUDDestinoTransportadora();
            }
        }

        public ViewDestinoTransportadora(string sTabela) : base(sTabela)
        {
            InitializeComponent();
        }

        protected override void CarregarDados()
        {
            InicializarFonteDeDados();

            CriarConsulta("id", "destino_id", "transportadora_id", "valordia");
            CriarRelacao("transportadora", new RelationColumnInfo("transportadora_id", "id"), "nome");
            CriarRelacao("destino", new RelationColumnInfo("destino_id", "id"), "universidade");

            base.CarregarDados();
        }
    }
}