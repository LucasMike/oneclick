﻿namespace Projeto.Controles.Views
{
    partial class ViewDestinoTransportadora
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcTransportadora = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcDestino = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcValor = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.lciNavBotoes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DVView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navBotoes)).BeginInit();
            this.SuspendLayout();
            // 
            // DVView
            // 
            this.DVView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcTransportadora,
            this.gcDestino,
            this.gcValor});
            this.DVView.OptionsBehavior.Editable = false;
            this.DVView.OptionsFind.AlwaysVisible = true;
            this.DVView.OptionsFind.FindNullPrompt = "Digite aqui para pesquisar...";
            // 
            // DGGrid
            // 
            this.DGGrid.DataMember = null;
            this.DGGrid.DataSource = this.FonteDeDados;
            // 
            // navBotoes
            // 
            this.navBotoes.OptionsNavPane.ExpandedWidth = 112;
            // 
            // gcTransportadora
            // 
            this.gcTransportadora.Caption = "Transportadora";
            this.gcTransportadora.FieldName = "nome";
            this.gcTransportadora.Name = "gcTransportadora";
            this.gcTransportadora.Visible = true;
            this.gcTransportadora.VisibleIndex = 0;
            // 
            // gcDestino
            // 
            this.gcDestino.Caption = "Destino";
            this.gcDestino.FieldName = "universidade";
            this.gcDestino.Name = "gcDestino";
            this.gcDestino.Visible = true;
            this.gcDestino.VisibleIndex = 1;
            // 
            // gcValor
            // 
            this.gcValor.Caption = "Valor";
            this.gcValor.DisplayFormat.FormatString = "c2";
            this.gcValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gcValor.FieldName = "valordia";
            this.gcValor.Name = "gcValor";
            this.gcValor.Visible = true;
            this.gcValor.VisibleIndex = 2;
            // 
            // ViewDestinoTransportadora
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "ViewDestinoTransportadora";
            ((System.ComponentModel.ISupportInitialize)(this.lciNavBotoes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DVView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navBotoes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn gcTransportadora;
        private DevExpress.XtraGrid.Columns.GridColumn gcDestino;
        private DevExpress.XtraGrid.Columns.GridColumn gcValor;
    }
}
