﻿using Projeto.Controles.CRUD;

namespace Projeto.Controles.Views
{
    public partial class ViewCidade : ViewBase
    {
        public override string Titulo => "Cidades";

        protected override FrmCRUDBase FormCRUD
        {
            get
            {
                return new FrmCRUDCidade();
            }
        }

        public ViewCidade(string sTabela) : base(sTabela)
        {
            InitializeComponent();
        }

        protected override void CarregarDados()
        {
            InicializarFonteDeDados();
            CriarConsulta("id", "nome", "cep", "estado");
            base.CarregarDados();
        }
    }
}
