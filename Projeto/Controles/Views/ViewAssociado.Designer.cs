﻿namespace Projeto.Controles.Views
{
    partial class ViewAssociado
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riteGenerico = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gcCPF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcTelefone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcEndereco = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcDataIni = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcDataFim = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.lciNavBotoes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DVView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navBotoes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riteGenerico)).BeginInit();
            this.SuspendLayout();
            // 
            // DVView
            // 
            this.DVView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcNome,
            this.gcCPF,
            this.gcTelefone,
            this.gcEmail,
            this.gcEndereco,
            this.gcDataIni,
            this.gcDataFim});
            this.DVView.OptionsBehavior.Editable = false;
            this.DVView.OptionsFind.AlwaysVisible = true;
            this.DVView.OptionsFind.FindNullPrompt = "Digite aqui para pesquisar...";
            // 
            // DGGrid
            // 
            this.DGGrid.DataMember = null;
            this.DGGrid.DataSource = this.FonteDeDados;
            this.DGGrid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.riteGenerico});
            // 
            // navBotoes
            // 
            this.navBotoes.OptionsNavPane.ExpandedWidth = 98;
            // 
            // gcNome
            // 
            this.gcNome.Caption = "Nome";
            this.gcNome.FieldName = "nome";
            this.gcNome.Name = "gcNome";
            this.gcNome.Visible = true;
            this.gcNome.VisibleIndex = 0;
            // 
            // riteGenerico
            // 
            this.riteGenerico.AutoHeight = false;
            this.riteGenerico.Name = "riteGenerico";
            // 
            // gcCPF
            // 
            this.gcCPF.Caption = "CPF";
            this.gcCPF.FieldName = "cpf";
            this.gcCPF.Name = "gcCPF";
            this.gcCPF.Visible = true;
            this.gcCPF.VisibleIndex = 1;
            // 
            // gcTelefone
            // 
            this.gcTelefone.Caption = "Telefone";
            this.gcTelefone.FieldName = "telefone";
            this.gcTelefone.Name = "gcTelefone";
            this.gcTelefone.Visible = true;
            this.gcTelefone.VisibleIndex = 2;
            // 
            // gcEmail
            // 
            this.gcEmail.Caption = "Email";
            this.gcEmail.FieldName = "email";
            this.gcEmail.Name = "gcEmail";
            this.gcEmail.Visible = true;
            this.gcEmail.VisibleIndex = 3;
            // 
            // gcEndereco
            // 
            this.gcEndereco.Caption = "Endereço";
            this.gcEndereco.FieldName = "endereco";
            this.gcEndereco.Name = "gcEndereco";
            this.gcEndereco.Visible = true;
            this.gcEndereco.VisibleIndex = 4;
            // 
            // gcDataIni
            // 
            this.gcDataIni.Caption = "Data de início";
            this.gcDataIni.FieldName = "dataini";
            this.gcDataIni.Name = "gcDataIni";
            this.gcDataIni.Visible = true;
            this.gcDataIni.VisibleIndex = 5;
            // 
            // gcDataFim
            // 
            this.gcDataFim.Caption = "Data final";
            this.gcDataFim.FieldName = "datafim";
            this.gcDataFim.Name = "gcDataFim";
            this.gcDataFim.Visible = true;
            this.gcDataFim.VisibleIndex = 6;
            // 
            // ViewAssociado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "ViewAssociado";
            ((System.ComponentModel.ISupportInitialize)(this.lciNavBotoes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DVView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navBotoes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riteGenerico)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn gcNome;
        private DevExpress.XtraGrid.Columns.GridColumn gcCPF;
        private DevExpress.XtraGrid.Columns.GridColumn gcTelefone;
        private DevExpress.XtraGrid.Columns.GridColumn gcEmail;
        private DevExpress.XtraGrid.Columns.GridColumn gcEndereco;
        private DevExpress.XtraGrid.Columns.GridColumn gcDataIni;
        private DevExpress.XtraGrid.Columns.GridColumn gcDataFim;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit riteGenerico;
    }
}
