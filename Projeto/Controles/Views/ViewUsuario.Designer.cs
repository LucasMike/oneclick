﻿namespace Projeto.Controles.Views
{
    partial class ViewUsuario
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcUsuario = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcSenha = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.lciNavBotoes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DVView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navBotoes)).BeginInit();
            this.SuspendLayout();
            // 
            // DVView
            // 
            this.DVView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcNome,
            this.gcUsuario,
            this.gcSenha});
            this.DVView.OptionsBehavior.Editable = false;
            this.DVView.OptionsFind.AlwaysVisible = true;
            this.DVView.OptionsFind.FindNullPrompt = "Digite aqui para pesquisar...";
            // 
            // DGGrid
            // 
            this.DGGrid.DataMember = null;
            this.DGGrid.DataSource = this.FonteDeDados;
            // 
            // navBotoes
            // 
            this.navBotoes.OptionsNavPane.ExpandedWidth = 112;
            // 
            // gcNome
            // 
            this.gcNome.Caption = "Nome";
            this.gcNome.FieldName = "nome";
            this.gcNome.Name = "gcNome";
            this.gcNome.Visible = true;
            this.gcNome.VisibleIndex = 0;
            // 
            // gcUsuario
            // 
            this.gcUsuario.Caption = "Usuário";
            this.gcUsuario.FieldName = "usuario";
            this.gcUsuario.Name = "gcUsuario";
            this.gcUsuario.Visible = true;
            this.gcUsuario.VisibleIndex = 1;
            // 
            // gcSenha
            // 
            this.gcSenha.Caption = "Senha";
            this.gcSenha.FieldName = "senha";
            this.gcSenha.Name = "gcSenha";
            this.gcSenha.Visible = true;
            this.gcSenha.VisibleIndex = 2;
            // 
            // ViewUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "ViewUsuario";
            ((System.ComponentModel.ISupportInitialize)(this.lciNavBotoes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DVView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navBotoes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn gcNome;
        private DevExpress.XtraGrid.Columns.GridColumn gcUsuario;
        private DevExpress.XtraGrid.Columns.GridColumn gcSenha;
    }
}
