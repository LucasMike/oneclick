﻿namespace Projeto.Controles.Views
{
    partial class ViewCidade
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcCEP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcEstado = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.lciNavBotoes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DVView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navBotoes)).BeginInit();
            this.SuspendLayout();
            // 
            // DVView
            // 
            this.DVView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcNome,
            this.gcCEP,
            this.gcEstado});
            this.DVView.OptionsBehavior.Editable = false;
            this.DVView.OptionsFind.AlwaysVisible = true;
            this.DVView.OptionsFind.FindNullPrompt = "Digite aqui para pesquisar...";
            // 
            // DGGrid
            // 
            this.DGGrid.DataMember = null;
            this.DGGrid.DataSource = this.FonteDeDados;
            // 
            // navBotoes
            // 
            this.navBotoes.OptionsNavPane.ExpandedWidth = 112;
            // 
            // gcNome
            // 
            this.gcNome.Caption = "Nome";
            this.gcNome.FieldName = "nome";
            this.gcNome.Name = "gcNome";
            this.gcNome.Visible = true;
            this.gcNome.VisibleIndex = 0;
            // 
            // gcCEP
            // 
            this.gcCEP.Caption = "CEP";
            this.gcCEP.DisplayFormat.FormatString = "#####-###";
            this.gcCEP.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gcCEP.FieldName = "cep";
            this.gcCEP.Name = "gcCEP";
            this.gcCEP.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.gcCEP.Visible = true;
            this.gcCEP.VisibleIndex = 1;
            // 
            // gcEstado
            // 
            this.gcEstado.Caption = "Estado";
            this.gcEstado.FieldName = "estado";
            this.gcEstado.Name = "gcEstado";
            this.gcEstado.Visible = true;
            this.gcEstado.VisibleIndex = 2;
            // 
            // ViewCidade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "ViewCidade";
            ((System.ComponentModel.ISupportInitialize)(this.lciNavBotoes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DVView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navBotoes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn gcNome;
        private DevExpress.XtraGrid.Columns.GridColumn gcCEP;
        private DevExpress.XtraGrid.Columns.GridColumn gcEstado;
    }
}
