﻿namespace Projeto.Relatorios
{
    partial class Boleto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator interleaved2of5Generator1 = new DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Boleto));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPageBreak2 = new DevExpress.XtraReports.UI.XRPageBreak();
            this.xrPageBreak1 = new DevExpress.XtraReports.UI.XRPageBreak();
            this.xrLine36 = new DevExpress.XtraReports.UI.XRLine();
            this.bcCodBarras = new DevExpress.XtraReports.UI.XRBarCode();
            this.panelBoleto = new DevExpress.XtraReports.UI.XRPanel();
            this.label7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine52 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine51 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine50 = new DevExpress.XtraReports.UI.XRLine();
            this.labelAgCodCed2 = new DevExpress.XtraReports.UI.XRLabel();
            this.labelSacCidUF2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine40 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine39 = new DevExpress.XtraReports.UI.XRLine();
            this.labelBanco2 = new DevExpress.XtraReports.UI.XRLabel();
            this.pictLogoBanco2 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.labelLinhaDigitavel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.labelDataPgto1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel67 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel65 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel64 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel63 = new DevExpress.XtraReports.UI.XRLabel();
            this.labelSacCNPJ2 = new DevExpress.XtraReports.UI.XRLabel();
            this.labelSacEnd2 = new DevExpress.XtraReports.UI.XRLabel();
            this.labelSacNome2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel59 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel58 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel57 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel56 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel55 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine35 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine34 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine33 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine32 = new DevExpress.XtraReports.UI.XRLine();
            this.labelValDoc2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel52 = new DevExpress.XtraReports.UI.XRLabel();
            this.labelNossoNum2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel50 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel49 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel48 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
            this.labelEspecie2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine31 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine30 = new DevExpress.XtraReports.UI.XRLine();
            this.labelDataProces2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrLabel44 = new DevExpress.XtraReports.UI.XRLabel();
            this.labelAceite2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel41 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.labelEspDoc2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.labelNumDoc2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.labelDataDoc2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.labelVenc2 = new DevExpress.XtraReports.UI.XRLabel();
            this.labelCedCNPJ1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.labelCedNome2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine29 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine28 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine27 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine26 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine25 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine24 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine14 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine13 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine10 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine9 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine8 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine7 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine6 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.panelRecibo = new DevExpress.XtraReports.UI.XRPanel();
            this.label6 = new DevExpress.XtraReports.UI.XRLabel();
            this.label4 = new DevExpress.XtraReports.UI.XRLabel();
            this.label5 = new DevExpress.XtraReports.UI.XRLabel();
            this.label3 = new DevExpress.XtraReports.UI.XRLabel();
            this.label1 = new DevExpress.XtraReports.UI.XRLabel();
            this.label2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine38 = new DevExpress.XtraReports.UI.XRLine();
            this.labelBanco1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine37 = new DevExpress.XtraReports.UI.XRLine();
            this.pictLogoBanco1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLine23 = new DevExpress.XtraReports.UI.XRLine();
            this.labelEspecie1 = new DevExpress.XtraReports.UI.XRLabel();
            this.labelEspDoc1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine22 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine21 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine20 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine19 = new DevExpress.XtraReports.UI.XRLine();
            this.labelDataDoc1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.labelAceite1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine18 = new DevExpress.XtraReports.UI.XRLine();
            this.labelAgCodCed1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine17 = new DevExpress.XtraReports.UI.XRLine();
            this.labelCedNome1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.labelValDoc1 = new DevExpress.XtraReports.UI.XRLabel();
            this.labelNossoNum1 = new DevExpress.XtraReports.UI.XRLabel();
            this.labelVenc1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.labelNumDoc1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine16 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine15 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.labelSacNome1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine12 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine11 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.panelObs = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.labelSAC = new DevExpress.XtraReports.UI.XRLabel();
            this.nomeRel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel8 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel35,
            this.xrLabel29,
            this.xrPageBreak2,
            this.xrPageBreak1,
            this.xrLine36,
            this.bcCodBarras,
            this.panelBoleto,
            this.panelRecibo,
            this.panelObs,
            this.nomeRel,
            this.xrPanel8});
            this.Detail.Dpi = 254F;
            this.Detail.Font = new System.Drawing.Font("Courier New", 9F);
            this.Detail.HeightF = 5780F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.Scripts.OnBeforePrint = "Detail_BeforePrint";
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel35
            // 
            this.xrLabel35.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel35.Dpi = 254F;
            this.xrLabel35.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(21F, 143F);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel35.SizeF = new System.Drawing.SizeF(1439F, 37F);
            this.xrLabel35.StylePriority.UsePadding = false;
            this.xrLabel35.StylePriority.UseTextAlignment = false;
            this.xrLabel35.Text = "CNPJ: 10713108000185";
            this.xrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrLabel35.WordWrap = false;
            // 
            // xrLabel29
            // 
            this.xrLabel29.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel29.Dpi = 254F;
            this.xrLabel29.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(21F, 21F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(1439F, 42F);
            this.xrLabel29.StylePriority.UsePadding = false;
            this.xrLabel29.StylePriority.UseTextAlignment = false;
            this.xrLabel29.Text = "ASSOCIAÇÃO DE ESTUDANTES PEQUENO PARAISO";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrLabel29.WordWrap = false;
            // 
            // xrPageBreak2
            // 
            this.xrPageBreak2.Dpi = 254F;
            this.xrPageBreak2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5778F);
            this.xrPageBreak2.Name = "xrPageBreak2";
            // 
            // xrPageBreak1
            // 
            this.xrPageBreak1.Dpi = 254F;
            this.xrPageBreak1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2900F);
            this.xrPageBreak1.Name = "xrPageBreak1";
            // 
            // xrLine36
            // 
            this.xrLine36.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine36.Dpi = 254F;
            this.xrLine36.LineWidth = 5;
            this.xrLine36.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2641F);
            this.xrLine36.Name = "xrLine36";
            this.xrLine36.SizeF = new System.Drawing.SizeF(1863F, 15F);
            // 
            // bcCodBarras
            // 
            this.bcCodBarras.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.bcCodBarras.AutoModule = true;
            this.bcCodBarras.Dpi = 254F;
            this.bcCodBarras.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2735F);
            this.bcCodBarras.Module = 508F;
            this.bcCodBarras.Name = "bcCodBarras";
            this.bcCodBarras.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.bcCodBarras.ShowText = false;
            this.bcCodBarras.SizeF = new System.Drawing.SizeF(1030F, 134F);
            interleaved2of5Generator1.CalcCheckSum = false;
            interleaved2of5Generator1.WideNarrowRatio = 2.5F;
            this.bcCodBarras.Symbology = interleaved2of5Generator1;
            this.bcCodBarras.Text = "[codigo_barras_bol]";
            // 
            // panelBoleto
            // 
            this.panelBoleto.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.label7,
            this.xrLine52,
            this.xrLine51,
            this.xrLabel25,
            this.xrLabel19,
            this.xrLabel24,
            this.xrLine50,
            this.labelAgCodCed2,
            this.labelSacCidUF2,
            this.xrLine40,
            this.xrLine39,
            this.labelBanco2,
            this.pictLogoBanco2,
            this.labelLinhaDigitavel2,
            this.labelDataPgto1,
            this.xrLabel67,
            this.xrLabel65,
            this.xrLabel64,
            this.xrLabel63,
            this.labelSacCNPJ2,
            this.labelSacEnd2,
            this.labelSacNome2,
            this.xrLabel59,
            this.xrLabel58,
            this.xrLabel57,
            this.xrLabel56,
            this.xrLabel55,
            this.xrLabel54,
            this.xrLine35,
            this.xrLine34,
            this.xrLine33,
            this.xrLine32,
            this.labelValDoc2,
            this.xrLabel52,
            this.labelNossoNum2,
            this.xrLabel50,
            this.xrLabel49,
            this.xrLabel48,
            this.xrLabel47,
            this.labelEspecie2,
            this.xrLabel45,
            this.xrLabel43,
            this.xrLine31,
            this.xrLine30,
            this.labelDataProces2,
            this.xrLabel44,
            this.labelAceite2,
            this.xrLabel41,
            this.xrLabel40,
            this.labelEspDoc2,
            this.xrLabel38,
            this.labelNumDoc2,
            this.xrLabel36,
            this.labelDataDoc2,
            this.xrLabel34,
            this.xrLabel33,
            this.xrLabel32,
            this.labelVenc2,
            this.labelCedCNPJ1,
            this.xrLabel28,
            this.labelCedNome2,
            this.xrLine29,
            this.xrLine28,
            this.xrLine27,
            this.xrLine26,
            this.xrLine25,
            this.xrLine24,
            this.xrLine14,
            this.xrLine13,
            this.xrLine10,
            this.xrLine9,
            this.xrLine8,
            this.xrLine7,
            this.xrLine6,
            this.xrLine5,
            this.xrLabel14});
            this.panelBoleto.Dpi = 254F;
            this.panelBoleto.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1696F);
            this.panelBoleto.Name = "panelBoleto";
            this.panelBoleto.SizeF = new System.Drawing.SizeF(1860F, 992F);
            // 
            // label7
            // 
            this.label7.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.label7.Dpi = 254F;
            this.label7.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Bold);
            this.label7.LocationFloat = new DevExpress.Utils.PointFloat(215F, 908F);
            this.label7.Name = "label7";
            this.label7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.label7.SizeF = new System.Drawing.SizeF(1181F, 37F);
            this.label7.StylePriority.UseFont = false;
            this.label7.StylePriority.UsePadding = false;
            this.label7.StylePriority.UseTextAlignment = false;
            this.label7.Text = "[sacadornome]   [sacadorcpfcnpj]";
            this.label7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.label7.WordWrap = false;
            // 
            // xrLine52
            // 
            this.xrLine52.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine52.Dpi = 254F;
            this.xrLine52.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine52.LocationFloat = new DevExpress.Utils.PointFloat(1402F, 884F);
            this.xrLine52.Name = "xrLine52";
            this.xrLine52.SizeF = new System.Drawing.SizeF(5F, 69F);
            // 
            // xrLine51
            // 
            this.xrLine51.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine51.Dpi = 254F;
            this.xrLine51.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine51.LocationFloat = new DevExpress.Utils.PointFloat(1069F, 238F);
            this.xrLine51.Name = "xrLine51";
            this.xrLine51.SizeF = new System.Drawing.SizeF(5F, 69F);
            // 
            // xrLabel25
            // 
            this.xrLabel25.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel25.Dpi = 254F;
            this.xrLabel25.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel25.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(1080F, 242F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(188F, 21F);
            this.xrLabel25.StylePriority.UsePadding = false;
            this.xrLabel25.Text = "CNPJ";
            // 
            // xrLabel19
            // 
            this.xrLabel19.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel19.Dpi = 254F;
            this.xrLabel19.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(370F, 403F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(265F, 37F);
            this.xrLabel19.StylePriority.UsePadding = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "[carteira]";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrLabel19.WordWrap = false;
            // 
            // xrLabel24
            // 
            this.xrLabel24.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel24.Dpi = 254F;
            this.xrLabel24.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel24.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(370F, 382F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(265F, 19F);
            this.xrLabel24.StylePriority.UsePadding = false;
            this.xrLabel24.Text = "CARTEIRA";
            // 
            // xrLine50
            // 
            this.xrLine50.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine50.Dpi = 254F;
            this.xrLine50.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine50.LocationFloat = new DevExpress.Utils.PointFloat(360F, 378F);
            this.xrLine50.Name = "xrLine50";
            this.xrLine50.SizeF = new System.Drawing.SizeF(5F, 69F);
            // 
            // labelAgCodCed2
            // 
            this.labelAgCodCed2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelAgCodCed2.Dpi = 254F;
            this.labelAgCodCed2.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.labelAgCodCed2.LocationFloat = new DevExpress.Utils.PointFloat(1413F, 263F);
            this.labelAgCodCed2.Name = "labelAgCodCed2";
            this.labelAgCodCed2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.labelAgCodCed2.SizeF = new System.Drawing.SizeF(430F, 37F);
            this.labelAgCodCed2.StylePriority.UsePadding = false;
            this.labelAgCodCed2.StylePriority.UseTextAlignment = false;
            this.labelAgCodCed2.Text = "[cedente]";
            this.labelAgCodCed2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.labelAgCodCed2.WordWrap = false;
            // 
            // labelSacCidUF2
            // 
            this.labelSacCidUF2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelSacCidUF2.Dpi = 254F;
            this.labelSacCidUF2.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.labelSacCidUF2.LocationFloat = new DevExpress.Utils.PointFloat(119F, 873F);
            this.labelSacCidUF2.Name = "labelSacCidUF2";
            this.labelSacCidUF2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.labelSacCidUF2.SizeF = new System.Drawing.SizeF(572F, 35F);
            this.labelSacCidUF2.StylePriority.UsePadding = false;
            this.labelSacCidUF2.StylePriority.UseTextAlignment = false;
            this.labelSacCidUF2.Text = "Dois Lajeados - 99220000 - RS";
            this.labelSacCidUF2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.labelSacCidUF2.WordWrap = false;
            // 
            // xrLine40
            // 
            this.xrLine40.Dpi = 254F;
            this.xrLine40.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine40.LineWidth = 5;
            this.xrLine40.LocationFloat = new DevExpress.Utils.PointFloat(513F, 91F);
            this.xrLine40.Name = "xrLine40";
            this.xrLine40.SizeF = new System.Drawing.SizeF(5F, 68F);
            // 
            // xrLine39
            // 
            this.xrLine39.Dpi = 254F;
            this.xrLine39.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine39.LineWidth = 5;
            this.xrLine39.LocationFloat = new DevExpress.Utils.PointFloat(360F, 91F);
            this.xrLine39.Name = "xrLine39";
            this.xrLine39.SizeF = new System.Drawing.SizeF(5F, 68F);
            // 
            // labelBanco2
            // 
            this.labelBanco2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelBanco2.Dpi = 254F;
            this.labelBanco2.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold);
            this.labelBanco2.LocationFloat = new DevExpress.Utils.PointFloat(370F, 82F);
            this.labelBanco2.Name = "labelBanco2";
            this.labelBanco2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.labelBanco2.SizeF = new System.Drawing.SizeF(142F, 64F);
            this.labelBanco2.StylePriority.UsePadding = false;
            this.labelBanco2.StylePriority.UseTextAlignment = false;
            this.labelBanco2.Text = "748-X";
            this.labelBanco2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.labelBanco2.WordWrap = false;
            // 
            // pictLogoBanco2
            // 
            this.pictLogoBanco2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.pictLogoBanco2.Dpi = 254F;
            this.pictLogoBanco2.Image = ((System.Drawing.Image)(resources.GetObject("pictLogoBanco2.Image")));
            this.pictLogoBanco2.LocationFloat = new DevExpress.Utils.PointFloat(2F, 71F);
            this.pictLogoBanco2.Name = "pictLogoBanco2";
            this.pictLogoBanco2.SizeF = new System.Drawing.SizeF(354F, 80F);
            this.pictLogoBanco2.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // labelLinhaDigitavel2
            // 
            this.labelLinhaDigitavel2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelLinhaDigitavel2.Dpi = 254F;
            this.labelLinhaDigitavel2.Font = new System.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.labelLinhaDigitavel2.LocationFloat = new DevExpress.Utils.PointFloat(550F, 79F);
            this.labelLinhaDigitavel2.Name = "labelLinhaDigitavel2";
            this.labelLinhaDigitavel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.labelLinhaDigitavel2.SizeF = new System.Drawing.SizeF(1270F, 64F);
            this.labelLinhaDigitavel2.StylePriority.UsePadding = false;
            this.labelLinhaDigitavel2.StylePriority.UseTextAlignment = false;
            this.labelLinhaDigitavel2.Text = "[linha_digitavel_bol]";
            this.labelLinhaDigitavel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.labelLinhaDigitavel2.WordWrap = false;
            // 
            // labelDataPgto1
            // 
            this.labelDataPgto1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelDataPgto1.Dpi = 254F;
            this.labelDataPgto1.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.labelDataPgto1.LocationFloat = new DevExpress.Utils.PointFloat(12F, 188F);
            this.labelDataPgto1.Name = "labelDataPgto1";
            this.labelDataPgto1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.labelDataPgto1.SizeF = new System.Drawing.SizeF(1382F, 37F);
            this.labelDataPgto1.StylePriority.UsePadding = false;
            this.labelDataPgto1.StylePriority.UseTextAlignment = false;
            this.labelDataPgto1.Text = "PAGÁVEL PREFERENCIALMENTE NAS COOPERATIVAS DE CRÉDITO DO SICREDI";
            this.labelDataPgto1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.labelDataPgto1.WordWrap = false;
            // 
            // xrLabel67
            // 
            this.xrLabel67.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel67.Dpi = 254F;
            this.xrLabel67.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Bold);
            this.xrLabel67.LocationFloat = new DevExpress.Utils.PointFloat(1122F, 960F);
            this.xrLabel67.Name = "xrLabel67";
            this.xrLabel67.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel67.SizeF = new System.Drawing.SizeF(741F, 32F);
            this.xrLabel67.StylePriority.UsePadding = false;
            this.xrLabel67.StylePriority.UseTextAlignment = false;
            this.xrLabel67.Text = "AUTENTICAÇÃO MECÂNICA - FICHA DE COMPENSAÇÃO";
            this.xrLabel67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel65
            // 
            this.xrLabel65.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel65.Dpi = 254F;
            this.xrLabel65.Font = new System.Drawing.Font("Tahoma", 4.5F, System.Drawing.FontStyle.Bold);
            this.xrLabel65.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel65.LocationFloat = new DevExpress.Utils.PointFloat(1410F, 929F);
            this.xrLabel65.Name = "xrLabel65";
            this.xrLabel65.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel65.SizeF = new System.Drawing.SizeF(190F, 20F);
            this.xrLabel65.StylePriority.UsePadding = false;
            this.xrLabel65.Text = "CÓDIGO DE BAIXA";
            // 
            // xrLabel64
            // 
            this.xrLabel64.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel64.Dpi = 254F;
            this.xrLabel64.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel64.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel64.LocationFloat = new DevExpress.Utils.PointFloat(13F, 908F);
            this.xrLabel64.Name = "xrLabel64";
            this.xrLabel64.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel64.SizeF = new System.Drawing.SizeF(202F, 36.99976F);
            this.xrLabel64.StylePriority.UsePadding = false;
            this.xrLabel64.StylePriority.UseTextAlignment = false;
            this.xrLabel64.Text = "SACADOR/AVALISTA:";
            this.xrLabel64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel63
            // 
            this.xrLabel63.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel63.Dpi = 254F;
            this.xrLabel63.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.xrLabel63.LocationFloat = new DevExpress.Utils.PointFloat(973.9998F, 802.9998F);
            this.xrLabel63.Name = "xrLabel63";
            this.xrLabel63.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel63.SizeF = new System.Drawing.SizeF(156.0001F, 34.99976F);
            this.xrLabel63.StylePriority.UsePadding = false;
            this.xrLabel63.StylePriority.UseTextAlignment = false;
            this.xrLabel63.Text = "CNPJ/CPF";
            this.xrLabel63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel63.WordWrap = false;
            // 
            // labelSacCNPJ2
            // 
            this.labelSacCNPJ2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelSacCNPJ2.Dpi = 254F;
            this.labelSacCNPJ2.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.labelSacCNPJ2.LocationFloat = new DevExpress.Utils.PointFloat(1122F, 803.0002F);
            this.labelSacCNPJ2.Name = "labelSacCNPJ2";
            this.labelSacCNPJ2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 5, 0, 0, 254F);
            this.labelSacCNPJ2.SizeF = new System.Drawing.SizeF(317.1041F, 34.99927F);
            this.labelSacCNPJ2.StylePriority.UsePadding = false;
            this.labelSacCNPJ2.StylePriority.UseTextAlignment = false;
            this.labelSacCNPJ2.Text = "[cpf]";
            this.labelSacCNPJ2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.labelSacCNPJ2.WordWrap = false;
            // 
            // labelSacEnd2
            // 
            this.labelSacEnd2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelSacEnd2.Dpi = 254F;
            this.labelSacEnd2.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.labelSacEnd2.LocationFloat = new DevExpress.Utils.PointFloat(119F, 838F);
            this.labelSacEnd2.Name = "labelSacEnd2";
            this.labelSacEnd2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.labelSacEnd2.SizeF = new System.Drawing.SizeF(706F, 35F);
            this.labelSacEnd2.StylePriority.UsePadding = false;
            this.labelSacEnd2.StylePriority.UseTextAlignment = false;
            this.labelSacEnd2.Text = "[endereco]";
            this.labelSacEnd2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.labelSacEnd2.WordWrap = false;
            // 
            // labelSacNome2
            // 
            this.labelSacNome2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelSacNome2.Dpi = 254F;
            this.labelSacNome2.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.labelSacNome2.LocationFloat = new DevExpress.Utils.PointFloat(119F, 803F);
            this.labelSacNome2.Name = "labelSacNome2";
            this.labelSacNome2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.labelSacNome2.SizeF = new System.Drawing.SizeF(847F, 35F);
            this.labelSacNome2.StylePriority.UsePadding = false;
            this.labelSacNome2.StylePriority.UseTextAlignment = false;
            this.labelSacNome2.Text = "[nome]";
            this.labelSacNome2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.labelSacNome2.WordWrap = false;
            // 
            // xrLabel59
            // 
            this.xrLabel59.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel59.Dpi = 254F;
            this.xrLabel59.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel59.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel59.LocationFloat = new DevExpress.Utils.PointFloat(13.00002F, 803F);
            this.xrLabel59.Name = "xrLabel59";
            this.xrLabel59.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel59.SizeF = new System.Drawing.SizeF(106F, 34.99951F);
            this.xrLabel59.StylePriority.UsePadding = false;
            this.xrLabel59.StylePriority.UseTextAlignment = false;
            this.xrLabel59.Text = "PAGADOR:";
            this.xrLabel59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel58
            // 
            this.xrLabel58.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel58.Dpi = 254F;
            this.xrLabel58.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel58.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel58.LocationFloat = new DevExpress.Utils.PointFloat(1413F, 734F);
            this.xrLabel58.Name = "xrLabel58";
            this.xrLabel58.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel58.SizeF = new System.Drawing.SizeF(429F, 21F);
            this.xrLabel58.StylePriority.UsePadding = false;
            this.xrLabel58.Text = "(=)VALOR COBRADO";
            // 
            // xrLabel57
            // 
            this.xrLabel57.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel57.Dpi = 254F;
            this.xrLabel57.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel57.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel57.LocationFloat = new DevExpress.Utils.PointFloat(1413F, 665F);
            this.xrLabel57.Name = "xrLabel57";
            this.xrLabel57.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel57.SizeF = new System.Drawing.SizeF(429F, 21F);
            this.xrLabel57.StylePriority.UsePadding = false;
            this.xrLabel57.Text = "(+)OUTROS ACRÉSCIMOS";
            // 
            // xrLabel56
            // 
            this.xrLabel56.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel56.Dpi = 254F;
            this.xrLabel56.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel56.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel56.LocationFloat = new DevExpress.Utils.PointFloat(1413F, 596F);
            this.xrLabel56.Name = "xrLabel56";
            this.xrLabel56.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel56.SizeF = new System.Drawing.SizeF(429F, 21F);
            this.xrLabel56.StylePriority.UsePadding = false;
            this.xrLabel56.Text = "(+)MORA/MULTA/JUROS";
            // 
            // xrLabel55
            // 
            this.xrLabel55.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel55.Dpi = 254F;
            this.xrLabel55.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel55.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel55.LocationFloat = new DevExpress.Utils.PointFloat(1414F, 525F);
            this.xrLabel55.Name = "xrLabel55";
            this.xrLabel55.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel55.SizeF = new System.Drawing.SizeF(429F, 21F);
            this.xrLabel55.StylePriority.UsePadding = false;
            this.xrLabel55.Text = "(-)OUTRAS DEDUÇÕES";
            // 
            // xrLabel54
            // 
            this.xrLabel54.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel54.Dpi = 254F;
            this.xrLabel54.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel54.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel54.LocationFloat = new DevExpress.Utils.PointFloat(1413F, 453F);
            this.xrLabel54.Name = "xrLabel54";
            this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel54.SizeF = new System.Drawing.SizeF(429F, 21F);
            this.xrLabel54.StylePriority.UsePadding = false;
            this.xrLabel54.Text = "(-)DESCONTO/ABATIMENTOS";
            // 
            // xrLine35
            // 
            this.xrLine35.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine35.Dpi = 254F;
            this.xrLine35.LocationFloat = new DevExpress.Utils.PointFloat(1404F, 721F);
            this.xrLine35.Name = "xrLine35";
            this.xrLine35.SizeF = new System.Drawing.SizeF(460F, 16F);
            // 
            // xrLine34
            // 
            this.xrLine34.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine34.Dpi = 254F;
            this.xrLine34.LocationFloat = new DevExpress.Utils.PointFloat(1404F, 651F);
            this.xrLine34.Name = "xrLine34";
            this.xrLine34.SizeF = new System.Drawing.SizeF(460F, 16F);
            // 
            // xrLine33
            // 
            this.xrLine33.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine33.Dpi = 254F;
            this.xrLine33.LocationFloat = new DevExpress.Utils.PointFloat(1404F, 581F);
            this.xrLine33.Name = "xrLine33";
            this.xrLine33.SizeF = new System.Drawing.SizeF(460F, 16F);
            // 
            // xrLine32
            // 
            this.xrLine32.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine32.Dpi = 254F;
            this.xrLine32.LocationFloat = new DevExpress.Utils.PointFloat(1404F, 510F);
            this.xrLine32.Name = "xrLine32";
            this.xrLine32.SizeF = new System.Drawing.SizeF(460F, 16F);
            // 
            // labelValDoc2
            // 
            this.labelValDoc2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelValDoc2.Dpi = 254F;
            this.labelValDoc2.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.labelValDoc2.LocationFloat = new DevExpress.Utils.PointFloat(1413F, 402F);
            this.labelValDoc2.Name = "labelValDoc2";
            this.labelValDoc2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.labelValDoc2.SizeF = new System.Drawing.SizeF(430F, 37F);
            this.labelValDoc2.StylePriority.UsePadding = false;
            this.labelValDoc2.StylePriority.UseTextAlignment = false;
            this.labelValDoc2.Text = "[valor!c2]";
            this.labelValDoc2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.labelValDoc2.WordWrap = false;
            // 
            // xrLabel52
            // 
            this.xrLabel52.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel52.Dpi = 254F;
            this.xrLabel52.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel52.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel52.LocationFloat = new DevExpress.Utils.PointFloat(1412F, 383F);
            this.xrLabel52.Name = "xrLabel52";
            this.xrLabel52.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel52.SizeF = new System.Drawing.SizeF(429.0002F, 20F);
            this.xrLabel52.StylePriority.UsePadding = false;
            this.xrLabel52.Text = "(=)VALOR DO DOCUMENTO";
            // 
            // labelNossoNum2
            // 
            this.labelNossoNum2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelNossoNum2.Dpi = 254F;
            this.labelNossoNum2.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.labelNossoNum2.LocationFloat = new DevExpress.Utils.PointFloat(1413F, 335F);
            this.labelNossoNum2.Name = "labelNossoNum2";
            this.labelNossoNum2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.labelNossoNum2.SizeF = new System.Drawing.SizeF(430F, 37F);
            this.labelNossoNum2.StylePriority.UsePadding = false;
            this.labelNossoNum2.StylePriority.UseTextAlignment = false;
            this.labelNossoNum2.Text = "[nosso_numero]";
            this.labelNossoNum2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.labelNossoNum2.WordWrap = false;
            // 
            // xrLabel50
            // 
            this.xrLabel50.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel50.Dpi = 254F;
            this.xrLabel50.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel50.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel50.LocationFloat = new DevExpress.Utils.PointFloat(1413F, 313F);
            this.xrLabel50.Name = "xrLabel50";
            this.xrLabel50.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel50.SizeF = new System.Drawing.SizeF(429F, 21F);
            this.xrLabel50.StylePriority.UsePadding = false;
            this.xrLabel50.Text = "NOSSO NÚMERO";
            // 
            // xrLabel49
            // 
            this.xrLabel49.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel49.Dpi = 254F;
            this.xrLabel49.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel49.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel49.LocationFloat = new DevExpress.Utils.PointFloat(13.00001F, 452F);
            this.xrLabel49.Name = "xrLabel49";
            this.xrLabel49.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel49.SizeF = new System.Drawing.SizeF(1384F, 21F);
            this.xrLabel49.StylePriority.UsePadding = false;
            this.xrLabel49.Text = "(INSTRUÇÕES DE RESPONSABILIDADE DO BENEFICIÁRIO. QUALQUER DÚVIDA SOBRE ESTE BOLET" +
    "O, CONTATE O BENEFICIÁRIO)";
            // 
            // xrLabel48
            // 
            this.xrLabel48.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel48.Dpi = 254F;
            this.xrLabel48.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel48.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel48.LocationFloat = new DevExpress.Utils.PointFloat(863F, 381F);
            this.xrLabel48.Name = "xrLabel48";
            this.xrLabel48.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel48.SizeF = new System.Drawing.SizeF(220F, 21F);
            this.xrLabel48.StylePriority.UsePadding = false;
            this.xrLabel48.Text = "QUANTIDADE";
            // 
            // xrLabel47
            // 
            this.xrLabel47.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel47.Dpi = 254F;
            this.xrLabel47.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel47.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel47.LocationFloat = new DevExpress.Utils.PointFloat(1106F, 381F);
            this.xrLabel47.Name = "xrLabel47";
            this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel47.SizeF = new System.Drawing.SizeF(280F, 21F);
            this.xrLabel47.StylePriority.UsePadding = false;
            this.xrLabel47.Text = "VALOR";
            // 
            // labelEspecie2
            // 
            this.labelEspecie2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelEspecie2.Dpi = 254F;
            this.labelEspecie2.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.labelEspecie2.LocationFloat = new DevExpress.Utils.PointFloat(651F, 402F);
            this.labelEspecie2.Name = "labelEspecie2";
            this.labelEspecie2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.labelEspecie2.SizeF = new System.Drawing.SizeF(188F, 37F);
            this.labelEspecie2.StylePriority.UsePadding = false;
            this.labelEspecie2.StylePriority.UseTextAlignment = false;
            this.labelEspecie2.Text = "R$";
            this.labelEspecie2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.labelEspecie2.WordWrap = false;
            // 
            // xrLabel45
            // 
            this.xrLabel45.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel45.Dpi = 254F;
            this.xrLabel45.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel45.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel45.LocationFloat = new DevExpress.Utils.PointFloat(651F, 381F);
            this.xrLabel45.Name = "xrLabel45";
            this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel45.SizeF = new System.Drawing.SizeF(188F, 21F);
            this.xrLabel45.StylePriority.UsePadding = false;
            this.xrLabel45.Text = "MOEDA";
            // 
            // xrLabel43
            // 
            this.xrLabel43.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel43.Dpi = 254F;
            this.xrLabel43.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel43.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(13F, 382F);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel43.SizeF = new System.Drawing.SizeF(325F, 21F);
            this.xrLabel43.StylePriority.UsePadding = false;
            this.xrLabel43.Text = "USO DO BANCO";
            // 
            // xrLine31
            // 
            this.xrLine31.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine31.Dpi = 254F;
            this.xrLine31.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine31.LocationFloat = new DevExpress.Utils.PointFloat(1095F, 379F);
            this.xrLine31.Name = "xrLine31";
            this.xrLine31.SizeF = new System.Drawing.SizeF(5F, 69F);
            // 
            // xrLine30
            // 
            this.xrLine30.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine30.Dpi = 254F;
            this.xrLine30.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine30.LocationFloat = new DevExpress.Utils.PointFloat(849F, 379F);
            this.xrLine30.Name = "xrLine30";
            this.xrLine30.SizeF = new System.Drawing.SizeF(5F, 69F);
            // 
            // labelDataProces2
            // 
            this.labelDataProces2.Dpi = 254F;
            this.labelDataProces2.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.labelDataProces2.Format = "{0:dd/MM/yyyy}";
            this.labelDataProces2.LocationFloat = new DevExpress.Utils.PointFloat(1146F, 332F);
            this.labelDataProces2.Name = "labelDataProces2";
            this.labelDataProces2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.labelDataProces2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.labelDataProces2.SizeF = new System.Drawing.SizeF(251F, 37F);
            this.labelDataProces2.StylePriority.UseTextAlignment = false;
            this.labelDataProces2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.labelDataProces2.WordWrap = false;
            // 
            // xrLabel44
            // 
            this.xrLabel44.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel44.Dpi = 254F;
            this.xrLabel44.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel44.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel44.LocationFloat = new DevExpress.Utils.PointFloat(1146F, 313F);
            this.xrLabel44.Name = "xrLabel44";
            this.xrLabel44.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 254F);
            this.xrLabel44.SizeF = new System.Drawing.SizeF(251F, 21F);
            this.xrLabel44.StylePriority.UsePadding = false;
            this.xrLabel44.Text = "DATA DO PROCESSAMENTO";
            // 
            // labelAceite2
            // 
            this.labelAceite2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelAceite2.Dpi = 254F;
            this.labelAceite2.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.labelAceite2.LocationFloat = new DevExpress.Utils.PointFloat(1053F, 331F);
            this.labelAceite2.Name = "labelAceite2";
            this.labelAceite2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.labelAceite2.SizeF = new System.Drawing.SizeF(77F, 37F);
            this.labelAceite2.StylePriority.UsePadding = false;
            this.labelAceite2.StylePriority.UseTextAlignment = false;
            this.labelAceite2.Text = "N";
            this.labelAceite2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.labelAceite2.WordWrap = false;
            // 
            // xrLabel41
            // 
            this.xrLabel41.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel41.Dpi = 254F;
            this.xrLabel41.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel41.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel41.LocationFloat = new DevExpress.Utils.PointFloat(1053F, 312F);
            this.xrLabel41.Name = "xrLabel41";
            this.xrLabel41.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 254F);
            this.xrLabel41.SizeF = new System.Drawing.SizeF(77F, 21F);
            this.xrLabel41.StylePriority.UsePadding = false;
            this.xrLabel41.Text = "ACEITE";
            // 
            // xrLabel40
            // 
            this.xrLabel40.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel40.Dpi = 254F;
            this.xrLabel40.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel40.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel40.LocationFloat = new DevExpress.Utils.PointFloat(743F, 313F);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel40.SizeF = new System.Drawing.SizeF(251F, 21F);
            this.xrLabel40.StylePriority.UsePadding = false;
            this.xrLabel40.Text = "ESPÉCIE DOC";
            // 
            // labelEspDoc2
            // 
            this.labelEspDoc2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelEspDoc2.Dpi = 254F;
            this.labelEspDoc2.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.labelEspDoc2.LocationFloat = new DevExpress.Utils.PointFloat(744F, 332F);
            this.labelEspDoc2.Name = "labelEspDoc2";
            this.labelEspDoc2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.labelEspDoc2.SizeF = new System.Drawing.SizeF(260F, 37F);
            this.labelEspDoc2.StylePriority.UsePadding = false;
            this.labelEspDoc2.StylePriority.UseTextAlignment = false;
            this.labelEspDoc2.Text = "DM";
            this.labelEspDoc2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.labelEspDoc2.WordWrap = false;
            // 
            // xrLabel38
            // 
            this.xrLabel38.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel38.Dpi = 254F;
            this.xrLabel38.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel38.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(283F, 313F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(437F, 21F);
            this.xrLabel38.StylePriority.UsePadding = false;
            this.xrLabel38.Text = "NÚMERO DO DOCUMENTO";
            // 
            // labelNumDoc2
            // 
            this.labelNumDoc2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelNumDoc2.Dpi = 254F;
            this.labelNumDoc2.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.labelNumDoc2.LocationFloat = new DevExpress.Utils.PointFloat(283F, 332F);
            this.labelNumDoc2.Name = "labelNumDoc2";
            this.labelNumDoc2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.labelNumDoc2.SizeF = new System.Drawing.SizeF(440F, 37F);
            this.labelNumDoc2.StylePriority.UsePadding = false;
            this.labelNumDoc2.StylePriority.UseTextAlignment = false;
            this.labelNumDoc2.Text = "[sequencia_arquivo]/001";
            this.labelNumDoc2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.labelNumDoc2.WordWrap = false;
            // 
            // xrLabel36
            // 
            this.xrLabel36.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel36.Dpi = 254F;
            this.xrLabel36.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel36.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(13F, 313F);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel36.SizeF = new System.Drawing.SizeF(241F, 21F);
            this.xrLabel36.StylePriority.UsePadding = false;
            this.xrLabel36.Text = "DATA DO DOCUMENTO";
            // 
            // labelDataDoc2
            // 
            this.labelDataDoc2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelDataDoc2.Dpi = 254F;
            this.labelDataDoc2.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.labelDataDoc2.LocationFloat = new DevExpress.Utils.PointFloat(11.99999F, 331.9999F);
            this.labelDataDoc2.Name = "labelDataDoc2";
            this.labelDataDoc2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.labelDataDoc2.SizeF = new System.Drawing.SizeF(182.7916F, 37.00003F);
            this.labelDataDoc2.StylePriority.UsePadding = false;
            this.labelDataDoc2.StylePriority.UseTextAlignment = false;
            this.labelDataDoc2.Text = "[emissao]";
            this.labelDataDoc2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.labelDataDoc2.WordWrap = false;
            // 
            // xrLabel34
            // 
            this.xrLabel34.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel34.Dpi = 254F;
            this.xrLabel34.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel34.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(13F, 162F);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(1363F, 21F);
            this.xrLabel34.StylePriority.UsePadding = false;
            this.xrLabel34.Text = "LOCAL DE PAGAMENTO";
            // 
            // xrLabel33
            // 
            this.xrLabel33.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel33.Dpi = 254F;
            this.xrLabel33.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel33.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(1413F, 244F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(429F, 21F);
            this.xrLabel33.StylePriority.UsePadding = false;
            this.xrLabel33.Text = "AGÊNCIA/CÓDIGO BENEFICIÁRIO";
            // 
            // xrLabel32
            // 
            this.xrLabel32.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel32.Dpi = 254F;
            this.xrLabel32.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel32.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(1413F, 162F);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(407F, 21F);
            this.xrLabel32.StylePriority.UsePadding = false;
            this.xrLabel32.Text = "VENCIMENTO";
            // 
            // labelVenc2
            // 
            this.labelVenc2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelVenc2.Dpi = 254F;
            this.labelVenc2.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.labelVenc2.LocationFloat = new DevExpress.Utils.PointFloat(1666.813F, 185F);
            this.labelVenc2.Name = "labelVenc2";
            this.labelVenc2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.labelVenc2.SizeF = new System.Drawing.SizeF(176.1876F, 37F);
            this.labelVenc2.StylePriority.UsePadding = false;
            this.labelVenc2.StylePriority.UseTextAlignment = false;
            this.labelVenc2.Text = "[vencimento]";
            this.labelVenc2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.labelVenc2.WordWrap = false;
            // 
            // labelCedCNPJ1
            // 
            this.labelCedCNPJ1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelCedCNPJ1.Dpi = 254F;
            this.labelCedCNPJ1.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.labelCedCNPJ1.LocationFloat = new DevExpress.Utils.PointFloat(1080F, 260F);
            this.labelCedCNPJ1.Name = "labelCedCNPJ1";
            this.labelCedCNPJ1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.labelCedCNPJ1.SizeF = new System.Drawing.SizeF(315F, 37F);
            this.labelCedCNPJ1.StylePriority.UsePadding = false;
            this.labelCedCNPJ1.StylePriority.UseTextAlignment = false;
            this.labelCedCNPJ1.Text = "10713108000185";
            this.labelCedCNPJ1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.labelCedCNPJ1.WordWrap = false;
            // 
            // xrLabel28
            // 
            this.xrLabel28.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel28.Dpi = 254F;
            this.xrLabel28.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel28.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(13F, 242F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(876F, 21F);
            this.xrLabel28.StylePriority.UsePadding = false;
            this.xrLabel28.Text = "BENEFICIÁRIO";
            // 
            // labelCedNome2
            // 
            this.labelCedNome2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelCedNome2.Dpi = 254F;
            this.labelCedNome2.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.labelCedNome2.LocationFloat = new DevExpress.Utils.PointFloat(11.99999F, 262.9999F);
            this.labelCedNome2.Name = "labelCedNome2";
            this.labelCedNome2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.labelCedNome2.SizeF = new System.Drawing.SizeF(1047F, 37F);
            this.labelCedNome2.StylePriority.UsePadding = false;
            this.labelCedNome2.StylePriority.UseTextAlignment = false;
            this.labelCedNome2.Text = "ASSOCIAÇÃO DE ESTUDANTES PEQUENO PARAISO";
            this.labelCedNome2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.labelCedNome2.WordWrap = false;
            // 
            // xrLine29
            // 
            this.xrLine29.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine29.Dpi = 254F;
            this.xrLine29.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine29.LocationFloat = new DevExpress.Utils.PointFloat(637F, 379F);
            this.xrLine29.Name = "xrLine29";
            this.xrLine29.SizeF = new System.Drawing.SizeF(5F, 69F);
            // 
            // xrLine28
            // 
            this.xrLine28.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine28.Dpi = 254F;
            this.xrLine28.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine28.LocationFloat = new DevExpress.Utils.PointFloat(1132F, 307F);
            this.xrLine28.Name = "xrLine28";
            this.xrLine28.SizeF = new System.Drawing.SizeF(5F, 70F);
            // 
            // xrLine27
            // 
            this.xrLine27.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine27.Dpi = 254F;
            this.xrLine27.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine27.LocationFloat = new DevExpress.Utils.PointFloat(1040F, 307F);
            this.xrLine27.Name = "xrLine27";
            this.xrLine27.SizeF = new System.Drawing.SizeF(5F, 70F);
            // 
            // xrLine26
            // 
            this.xrLine26.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine26.Dpi = 254F;
            this.xrLine26.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine26.LocationFloat = new DevExpress.Utils.PointFloat(730F, 308F);
            this.xrLine26.Name = "xrLine26";
            this.xrLine26.SizeF = new System.Drawing.SizeF(5F, 69F);
            // 
            // xrLine25
            // 
            this.xrLine25.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine25.Dpi = 254F;
            this.xrLine25.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine25.LocationFloat = new DevExpress.Utils.PointFloat(269F, 308F);
            this.xrLine25.Name = "xrLine25";
            this.xrLine25.SizeF = new System.Drawing.SizeF(5F, 69F);
            // 
            // xrLine24
            // 
            this.xrLine24.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine24.Dpi = 254F;
            this.xrLine24.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine24.LocationFloat = new DevExpress.Utils.PointFloat(1402F, 157F);
            this.xrLine24.Name = "xrLine24";
            this.xrLine24.SizeF = new System.Drawing.SizeF(5F, 639F);
            // 
            // xrLine14
            // 
            this.xrLine14.Dpi = 254F;
            this.xrLine14.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine14.LocationFloat = new DevExpress.Utils.PointFloat(1856F, 156F);
            this.xrLine14.Name = "xrLine14";
            this.xrLine14.SizeF = new System.Drawing.SizeF(5F, 639F);
            // 
            // xrLine13
            // 
            this.xrLine13.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine13.Dpi = 254F;
            this.xrLine13.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine13.LocationFloat = new DevExpress.Utils.PointFloat(0F, 157F);
            this.xrLine13.Name = "xrLine13";
            this.xrLine13.SizeF = new System.Drawing.SizeF(5F, 639F);
            // 
            // xrLine10
            // 
            this.xrLine10.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine10.Dpi = 254F;
            this.xrLine10.LocationFloat = new DevExpress.Utils.PointFloat(2F, 788F);
            this.xrLine10.Name = "xrLine10";
            this.xrLine10.SizeF = new System.Drawing.SizeF(1862F, 15F);
            // 
            // xrLine9
            // 
            this.xrLine9.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine9.Dpi = 254F;
            this.xrLine9.LocationFloat = new DevExpress.Utils.PointFloat(2F, 440F);
            this.xrLine9.Name = "xrLine9";
            this.xrLine9.SizeF = new System.Drawing.SizeF(1861F, 15F);
            // 
            // xrLine8
            // 
            this.xrLine8.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine8.Dpi = 254F;
            this.xrLine8.LocationFloat = new DevExpress.Utils.PointFloat(2F, 370F);
            this.xrLine8.Name = "xrLine8";
            this.xrLine8.SizeF = new System.Drawing.SizeF(1861F, 15F);
            // 
            // xrLine7
            // 
            this.xrLine7.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine7.Dpi = 254F;
            this.xrLine7.LocationFloat = new DevExpress.Utils.PointFloat(2F, 300F);
            this.xrLine7.Name = "xrLine7";
            this.xrLine7.SizeF = new System.Drawing.SizeF(1861F, 15F);
            // 
            // xrLine6
            // 
            this.xrLine6.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine6.Dpi = 254F;
            this.xrLine6.LocationFloat = new DevExpress.Utils.PointFloat(2F, 230F);
            this.xrLine6.Name = "xrLine6";
            this.xrLine6.SizeF = new System.Drawing.SizeF(1862F, 15F);
            // 
            // xrLine5
            // 
            this.xrLine5.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine5.Dpi = 254F;
            this.xrLine5.LineWidth = 5;
            this.xrLine5.LocationFloat = new DevExpress.Utils.PointFloat(2F, 150F);
            this.xrLine5.Name = "xrLine5";
            this.xrLine5.SizeF = new System.Drawing.SizeF(1861F, 15F);
            // 
            // xrLabel14
            // 
            this.xrLabel14.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "boletos.instrucoes")});
            this.xrLabel14.Dpi = 254F;
            this.xrLabel14.Font = new System.Drawing.Font("Tahoma", 7.5F);
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(12F, 482F);
            this.xrLabel14.Multiline = true;
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(1384F, 299F);
            this.xrLabel14.Text = "xrLabel14";
            // 
            // panelRecibo
            // 
            this.panelRecibo.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.label6,
            this.label4,
            this.label5,
            this.label3,
            this.label1,
            this.label2,
            this.xrLabel4,
            this.xrLine38,
            this.labelBanco1,
            this.xrLine37,
            this.pictLogoBanco1,
            this.xrLine23,
            this.labelEspecie1,
            this.labelEspDoc1,
            this.xrLabel23,
            this.xrLabel22,
            this.xrLabel21,
            this.xrLabel20,
            this.xrLine22,
            this.xrLine21,
            this.xrLine20,
            this.xrLine19,
            this.labelDataDoc1,
            this.xrLabel18,
            this.labelAceite1,
            this.xrLabel15,
            this.xrLine18,
            this.labelAgCodCed1,
            this.xrLabel13,
            this.xrLine17,
            this.labelCedNome1,
            this.xrLabel11,
            this.labelValDoc1,
            this.labelNossoNum1,
            this.labelVenc1,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel5,
            this.labelNumDoc1,
            this.xrLabel3,
            this.xrLine16,
            this.xrLine15,
            this.xrLabel2,
            this.labelSacNome1,
            this.xrLine12,
            this.xrLine11,
            this.xrLine4,
            this.xrLine3,
            this.xrLine2,
            this.xrLine1});
            this.panelRecibo.Dpi = 254F;
            this.panelRecibo.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1210F);
            this.panelRecibo.Name = "panelRecibo";
            this.panelRecibo.SizeF = new System.Drawing.SizeF(1860F, 486F);
            // 
            // label6
            // 
            this.label6.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.label6.Dpi = 254F;
            this.label6.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.LocationFloat = new DevExpress.Utils.PointFloat(13.00002F, 325F);
            this.label6.Name = "label6";
            this.label6.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.label6.SizeF = new System.Drawing.SizeF(106F, 35.10437F);
            this.label6.StylePriority.UsePadding = false;
            this.label6.StylePriority.UseTextAlignment = false;
            this.label6.Text = "PAGADOR:";
            this.label6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.label4.Dpi = 254F;
            this.label4.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.label4.LocationFloat = new DevExpress.Utils.PointFloat(966.0001F, 325F);
            this.label4.Name = "label4";
            this.label4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.label4.SizeF = new System.Drawing.SizeF(156F, 35.10437F);
            this.label4.StylePriority.UsePadding = false;
            this.label4.StylePriority.UseTextAlignment = false;
            this.label4.Text = "CNPJ/CPF";
            this.label4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.label4.WordWrap = false;
            // 
            // label5
            // 
            this.label5.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.label5.Dpi = 254F;
            this.label5.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.label5.LocationFloat = new DevExpress.Utils.PointFloat(1122F, 325F);
            this.label5.Name = "label5";
            this.label5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 5, 0, 0, 254F);
            this.label5.SizeF = new System.Drawing.SizeF(317.1041F, 35.10413F);
            this.label5.StylePriority.UsePadding = false;
            this.label5.StylePriority.UseTextAlignment = false;
            this.label5.Text = "[cpf]";
            this.label5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.label5.WordWrap = false;
            // 
            // label3
            // 
            this.label3.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.label3.Dpi = 254F;
            this.label3.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.label3.LocationFloat = new DevExpress.Utils.PointFloat(119F, 395.1042F);
            this.label3.Name = "label3";
            this.label3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.label3.SizeF = new System.Drawing.SizeF(572F, 35F);
            this.label3.StylePriority.UsePadding = false;
            this.label3.StylePriority.UseTextAlignment = false;
            this.label3.Text = "Dois Lajeados - 99220000 - RS";
            this.label3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.label3.WordWrap = false;
            // 
            // label1
            // 
            this.label1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.label1.Dpi = 254F;
            this.label1.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.label1.LocationFloat = new DevExpress.Utils.PointFloat(119F, 325.1042F);
            this.label1.Name = "label1";
            this.label1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.label1.SizeF = new System.Drawing.SizeF(847F, 35F);
            this.label1.StylePriority.UsePadding = false;
            this.label1.StylePriority.UseTextAlignment = false;
            this.label1.Text = "[nome]";
            this.label1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.label1.WordWrap = false;
            // 
            // label2
            // 
            this.label2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.label2.Dpi = 254F;
            this.label2.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.label2.LocationFloat = new DevExpress.Utils.PointFloat(119F, 360.1042F);
            this.label2.Name = "label2";
            this.label2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.label2.SizeF = new System.Drawing.SizeF(706F, 35F);
            this.label2.StylePriority.UsePadding = false;
            this.label2.StylePriority.UseTextAlignment = false;
            this.label2.Text = "[endereco]";
            this.label2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.label2.WordWrap = false;
            // 
            // xrLabel4
            // 
            this.xrLabel4.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(1500F, 32F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(339F, 64F);
            this.xrLabel4.StylePriority.UsePadding = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "RECIBO DO PAGADOR";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // xrLine38
            // 
            this.xrLine38.Dpi = 254F;
            this.xrLine38.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine38.LineWidth = 5;
            this.xrLine38.LocationFloat = new DevExpress.Utils.PointFloat(513F, 41F);
            this.xrLine38.Name = "xrLine38";
            this.xrLine38.SizeF = new System.Drawing.SizeF(5F, 69F);
            // 
            // labelBanco1
            // 
            this.labelBanco1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelBanco1.Dpi = 254F;
            this.labelBanco1.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold);
            this.labelBanco1.LocationFloat = new DevExpress.Utils.PointFloat(370F, 32F);
            this.labelBanco1.Name = "labelBanco1";
            this.labelBanco1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.labelBanco1.SizeF = new System.Drawing.SizeF(142F, 64F);
            this.labelBanco1.StylePriority.UsePadding = false;
            this.labelBanco1.StylePriority.UseTextAlignment = false;
            this.labelBanco1.Text = "748-X";
            this.labelBanco1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.labelBanco1.WordWrap = false;
            // 
            // xrLine37
            // 
            this.xrLine37.Dpi = 254F;
            this.xrLine37.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine37.LineWidth = 5;
            this.xrLine37.LocationFloat = new DevExpress.Utils.PointFloat(360F, 41F);
            this.xrLine37.Name = "xrLine37";
            this.xrLine37.SizeF = new System.Drawing.SizeF(5F, 69F);
            // 
            // pictLogoBanco1
            // 
            this.pictLogoBanco1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.pictLogoBanco1.Dpi = 254F;
            this.pictLogoBanco1.Image = ((System.Drawing.Image)(resources.GetObject("pictLogoBanco1.Image")));
            this.pictLogoBanco1.LocationFloat = new DevExpress.Utils.PointFloat(2F, 20F);
            this.pictLogoBanco1.Name = "pictLogoBanco1";
            this.pictLogoBanco1.SizeF = new System.Drawing.SizeF(354F, 80F);
            this.pictLogoBanco1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // xrLine23
            // 
            this.xrLine23.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine23.Dpi = 254F;
            this.xrLine23.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine23.LocationFloat = new DevExpress.Utils.PointFloat(0F, 478F);
            this.xrLine23.Name = "xrLine23";
            this.xrLine23.SizeF = new System.Drawing.SizeF(1863F, 5F);
            // 
            // labelEspecie1
            // 
            this.labelEspecie1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelEspecie1.Dpi = 254F;
            this.labelEspecie1.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.labelEspecie1.LocationFloat = new DevExpress.Utils.PointFloat(512F, 274F);
            this.labelEspecie1.Name = "labelEspecie1";
            this.labelEspecie1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.labelEspecie1.SizeF = new System.Drawing.SizeF(241F, 37F);
            this.labelEspecie1.StylePriority.UsePadding = false;
            this.labelEspecie1.StylePriority.UseTextAlignment = false;
            this.labelEspecie1.Text = "R$";
            this.labelEspecie1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.labelEspecie1.WordWrap = false;
            // 
            // labelEspDoc1
            // 
            this.labelEspDoc1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelEspDoc1.Dpi = 254F;
            this.labelEspDoc1.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.labelEspDoc1.LocationFloat = new DevExpress.Utils.PointFloat(283F, 274F);
            this.labelEspDoc1.Name = "labelEspDoc1";
            this.labelEspDoc1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.labelEspDoc1.SizeF = new System.Drawing.SizeF(204F, 37F);
            this.labelEspDoc1.StylePriority.UsePadding = false;
            this.labelEspDoc1.StylePriority.UseTextAlignment = false;
            this.labelEspDoc1.Text = "DM";
            this.labelEspDoc1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.labelEspDoc1.WordWrap = false;
            // 
            // xrLabel23
            // 
            this.xrLabel23.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel23.Dpi = 254F;
            this.xrLabel23.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel23.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(1082F, 255F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(294F, 21F);
            this.xrLabel23.StylePriority.UsePadding = false;
            this.xrLabel23.Text = "VALOR";
            // 
            // xrLabel22
            // 
            this.xrLabel22.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel22.Dpi = 254F;
            this.xrLabel22.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel22.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(773F, 255F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(286F, 21F);
            this.xrLabel22.StylePriority.UsePadding = false;
            this.xrLabel22.Text = "QUANTIDADE";
            // 
            // xrLabel21
            // 
            this.xrLabel21.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel21.Dpi = 254F;
            this.xrLabel21.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel21.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(513F, 255F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(228F, 21F);
            this.xrLabel21.StylePriority.UsePadding = false;
            this.xrLabel21.Text = "MOEDA";
            // 
            // xrLabel20
            // 
            this.xrLabel20.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel20.Dpi = 254F;
            this.xrLabel20.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel20.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(283F, 255F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(204F, 21F);
            this.xrLabel20.StylePriority.UsePadding = false;
            this.xrLabel20.Text = "ESPÉCIE DOC";
            // 
            // xrLine22
            // 
            this.xrLine22.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine22.Dpi = 254F;
            this.xrLine22.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine22.LocationFloat = new DevExpress.Utils.PointFloat(1070F, 247F);
            this.xrLine22.Name = "xrLine22";
            this.xrLine22.SizeF = new System.Drawing.SizeF(5F, 70F);
            // 
            // xrLine21
            // 
            this.xrLine21.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine21.Dpi = 254F;
            this.xrLine21.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine21.LocationFloat = new DevExpress.Utils.PointFloat(760F, 247F);
            this.xrLine21.Name = "xrLine21";
            this.xrLine21.SizeF = new System.Drawing.SizeF(5F, 70F);
            // 
            // xrLine20
            // 
            this.xrLine20.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine20.Dpi = 254F;
            this.xrLine20.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine20.LocationFloat = new DevExpress.Utils.PointFloat(500F, 247F);
            this.xrLine20.Name = "xrLine20";
            this.xrLine20.SizeF = new System.Drawing.SizeF(5F, 70F);
            // 
            // xrLine19
            // 
            this.xrLine19.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine19.Dpi = 254F;
            this.xrLine19.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine19.LocationFloat = new DevExpress.Utils.PointFloat(270F, 247F);
            this.xrLine19.Name = "xrLine19";
            this.xrLine19.SizeF = new System.Drawing.SizeF(5F, 70F);
            // 
            // labelDataDoc1
            // 
            this.labelDataDoc1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelDataDoc1.Dpi = 254F;
            this.labelDataDoc1.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.labelDataDoc1.LocationFloat = new DevExpress.Utils.PointFloat(11.99999F, 274F);
            this.labelDataDoc1.Name = "labelDataDoc1";
            this.labelDataDoc1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.labelDataDoc1.SizeF = new System.Drawing.SizeF(182.7916F, 37F);
            this.labelDataDoc1.StylePriority.UsePadding = false;
            this.labelDataDoc1.StylePriority.UseTextAlignment = false;
            this.labelDataDoc1.Text = "[emissao]";
            this.labelDataDoc1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.labelDataDoc1.WordWrap = false;
            // 
            // xrLabel18
            // 
            this.xrLabel18.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel18.Dpi = 254F;
            this.xrLabel18.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(12F, 255F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(241F, 21F);
            this.xrLabel18.StylePriority.UsePadding = false;
            this.xrLabel18.Text = "DATA DO DOCUMENTO";
            // 
            // labelAceite1
            // 
            this.labelAceite1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelAceite1.Dpi = 254F;
            this.labelAceite1.Font = new System.Drawing.Font("Tahoma", 7.5F);
            this.labelAceite1.LocationFloat = new DevExpress.Utils.PointFloat(1315F, 204F);
            this.labelAceite1.Name = "labelAceite1";
            this.labelAceite1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.labelAceite1.SizeF = new System.Drawing.SizeF(73F, 37F);
            this.labelAceite1.StylePriority.UsePadding = false;
            this.labelAceite1.StylePriority.UseTextAlignment = false;
            this.labelAceite1.Text = "N";
            this.labelAceite1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.labelAceite1.WordWrap = false;
            // 
            // xrLabel15
            // 
            this.xrLabel15.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel15.Dpi = 254F;
            this.xrLabel15.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel15.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(1315F, 183F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 254F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(73F, 20F);
            this.xrLabel15.StylePriority.UsePadding = false;
            this.xrLabel15.Text = "ACEITE";
            // 
            // xrLine18
            // 
            this.xrLine18.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine18.Dpi = 254F;
            this.xrLine18.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine18.LocationFloat = new DevExpress.Utils.PointFloat(1303F, 177F);
            this.xrLine18.Name = "xrLine18";
            this.xrLine18.SizeF = new System.Drawing.SizeF(5F, 70F);
            // 
            // labelAgCodCed1
            // 
            this.labelAgCodCed1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelAgCodCed1.Dpi = 254F;
            this.labelAgCodCed1.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.labelAgCodCed1.LocationFloat = new DevExpress.Utils.PointFloat(873F, 205F);
            this.labelAgCodCed1.Name = "labelAgCodCed1";
            this.labelAgCodCed1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.labelAgCodCed1.SizeF = new System.Drawing.SizeF(418F, 37F);
            this.labelAgCodCed1.StylePriority.UsePadding = false;
            this.labelAgCodCed1.StylePriority.UseTextAlignment = false;
            this.labelAgCodCed1.Text = "[agencia].[posto].[cedente]";
            this.labelAgCodCed1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.labelAgCodCed1.WordWrap = false;
            // 
            // xrLabel13
            // 
            this.xrLabel13.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel13.Dpi = 254F;
            this.xrLabel13.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(873F, 184F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(418F, 21F);
            this.xrLabel13.StylePriority.UsePadding = false;
            this.xrLabel13.Text = "AGÊNCIA/CÓDIGO BENEFICIÁRIO";
            // 
            // xrLine17
            // 
            this.xrLine17.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine17.Dpi = 254F;
            this.xrLine17.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine17.LocationFloat = new DevExpress.Utils.PointFloat(859F, 177F);
            this.xrLine17.Name = "xrLine17";
            this.xrLine17.SizeF = new System.Drawing.SizeF(5F, 70F);
            // 
            // labelCedNome1
            // 
            this.labelCedNome1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelCedNome1.Dpi = 254F;
            this.labelCedNome1.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.labelCedNome1.LocationFloat = new DevExpress.Utils.PointFloat(12F, 204F);
            this.labelCedNome1.Name = "labelCedNome1";
            this.labelCedNome1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.labelCedNome1.SizeF = new System.Drawing.SizeF(840F, 37F);
            this.labelCedNome1.StylePriority.UsePadding = false;
            this.labelCedNome1.StylePriority.UseTextAlignment = false;
            this.labelCedNome1.Text = "ASSOCIAÇÃO DE ESTUDANTES PEQUENO PARAISO";
            this.labelCedNome1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.labelCedNome1.WordWrap = false;
            // 
            // xrLabel11
            // 
            this.xrLabel11.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel11.Dpi = 254F;
            this.xrLabel11.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(12F, 184F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(833F, 21F);
            this.xrLabel11.StylePriority.UsePadding = false;
            this.xrLabel11.Text = "BENEFICIÁRIO";
            // 
            // labelValDoc1
            // 
            this.labelValDoc1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelValDoc1.Dpi = 254F;
            this.labelValDoc1.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.labelValDoc1.LocationFloat = new DevExpress.Utils.PointFloat(1409F, 271F);
            this.labelValDoc1.Name = "labelValDoc1";
            this.labelValDoc1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.labelValDoc1.SizeF = new System.Drawing.SizeF(430F, 37F);
            this.labelValDoc1.StylePriority.UsePadding = false;
            this.labelValDoc1.StylePriority.UseTextAlignment = false;
            this.labelValDoc1.Text = "[valor!c2]";
            this.labelValDoc1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.labelValDoc1.WordWrap = false;
            // 
            // labelNossoNum1
            // 
            this.labelNossoNum1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelNossoNum1.Dpi = 254F;
            this.labelNossoNum1.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.labelNossoNum1.LocationFloat = new DevExpress.Utils.PointFloat(1409F, 204F);
            this.labelNossoNum1.Name = "labelNossoNum1";
            this.labelNossoNum1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.labelNossoNum1.SizeF = new System.Drawing.SizeF(430F, 37F);
            this.labelNossoNum1.StylePriority.UsePadding = false;
            this.labelNossoNum1.StylePriority.UseTextAlignment = false;
            this.labelNossoNum1.Text = "[nosso_numero]";
            this.labelNossoNum1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.labelNossoNum1.WordWrap = false;
            // 
            // labelVenc1
            // 
            this.labelVenc1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelVenc1.Dpi = 254F;
            this.labelVenc1.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.labelVenc1.LocationFloat = new DevExpress.Utils.PointFloat(1666.813F, 134F);
            this.labelVenc1.Name = "labelVenc1";
            this.labelVenc1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.labelVenc1.SizeF = new System.Drawing.SizeF(172.1875F, 37.00002F);
            this.labelVenc1.StylePriority.UsePadding = false;
            this.labelVenc1.StylePriority.UseTextAlignment = false;
            this.labelVenc1.Text = "[vencimento]";
            this.labelVenc1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.labelVenc1.WordWrap = false;
            // 
            // xrLabel7
            // 
            this.xrLabel7.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(1408F, 255F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(434F, 21F);
            this.xrLabel7.StylePriority.UsePadding = false;
            this.xrLabel7.Text = "(=) VALOR DOCUMENTO";
            // 
            // xrLabel6
            // 
            this.xrLabel6.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(1410F, 184F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(431F, 21F);
            this.xrLabel6.StylePriority.UsePadding = false;
            this.xrLabel6.Text = "NOSSO NÚMERO";
            // 
            // xrLabel5
            // 
            this.xrLabel5.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(1410F, 115F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(431F, 21F);
            this.xrLabel5.StylePriority.UsePadding = false;
            this.xrLabel5.Text = "VENCIMENTO";
            // 
            // labelNumDoc1
            // 
            this.labelNumDoc1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelNumDoc1.Dpi = 254F;
            this.labelNumDoc1.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.labelNumDoc1.LocationFloat = new DevExpress.Utils.PointFloat(963F, 134F);
            this.labelNumDoc1.Name = "labelNumDoc1";
            this.labelNumDoc1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.labelNumDoc1.SizeF = new System.Drawing.SizeF(431F, 37F);
            this.labelNumDoc1.StylePriority.UsePadding = false;
            this.labelNumDoc1.StylePriority.UseTextAlignment = false;
            this.labelNumDoc1.Text = "[sequencia_arquivo]/001";
            this.labelNumDoc1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.labelNumDoc1.WordWrap = false;
            // 
            // xrLabel3
            // 
            this.xrLabel3.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(963F, 115F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(434F, 21F);
            this.xrLabel3.StylePriority.UsePadding = false;
            this.xrLabel3.Text = "NÚMERO DO DOCUMENTO";
            // 
            // xrLine16
            // 
            this.xrLine16.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine16.Dpi = 254F;
            this.xrLine16.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine16.LocationFloat = new DevExpress.Utils.PointFloat(1400F, 110F);
            this.xrLine16.Name = "xrLine16";
            this.xrLine16.SizeF = new System.Drawing.SizeF(5F, 209F);
            // 
            // xrLine15
            // 
            this.xrLine15.Dpi = 254F;
            this.xrLine15.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine15.LocationFloat = new DevExpress.Utils.PointFloat(950F, 110F);
            this.xrLine15.Name = "xrLine15";
            this.xrLine15.SizeF = new System.Drawing.SizeF(5F, 68F);
            // 
            // xrLabel2
            // 
            this.xrLabel2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.Font = new System.Drawing.Font("Tahoma", 5F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(12F, 115F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 5, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(918F, 21F);
            this.xrLabel2.StylePriority.UsePadding = false;
            this.xrLabel2.Text = "PAGADOR";
            // 
            // labelSacNome1
            // 
            this.labelSacNome1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.labelSacNome1.Dpi = 254F;
            this.labelSacNome1.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.labelSacNome1.LocationFloat = new DevExpress.Utils.PointFloat(12F, 134F);
            this.labelSacNome1.Name = "labelSacNome1";
            this.labelSacNome1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 254F);
            this.labelSacNome1.SizeF = new System.Drawing.SizeF(918F, 37F);
            this.labelSacNome1.StylePriority.UsePadding = false;
            this.labelSacNome1.StylePriority.UseTextAlignment = false;
            this.labelSacNome1.Text = "[nome]";
            this.labelSacNome1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.labelSacNome1.WordWrap = false;
            // 
            // xrLine12
            // 
            this.xrLine12.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine12.Dpi = 254F;
            this.xrLine12.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine12.LocationFloat = new DevExpress.Utils.PointFloat(1856F, 107F);
            this.xrLine12.Name = "xrLine12";
            this.xrLine12.SizeF = new System.Drawing.SizeF(5F, 212F);
            // 
            // xrLine11
            // 
            this.xrLine11.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine11.Dpi = 254F;
            this.xrLine11.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine11.LocationFloat = new DevExpress.Utils.PointFloat(0F, 107F);
            this.xrLine11.Name = "xrLine11";
            this.xrLine11.SizeF = new System.Drawing.SizeF(5F, 211F);
            // 
            // xrLine4
            // 
            this.xrLine4.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine4.Dpi = 254F;
            this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(2F, 310F);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.SizeF = new System.Drawing.SizeF(1863F, 15F);
            // 
            // xrLine3
            // 
            this.xrLine3.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine3.Dpi = 254F;
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(2F, 240F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(1863F, 15F);
            // 
            // xrLine2
            // 
            this.xrLine2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine2.Dpi = 254F;
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(2F, 170F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(1860F, 15F);
            // 
            // xrLine1
            // 
            this.xrLine1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLine1.Dpi = 254F;
            this.xrLine1.LineWidth = 5;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(2F, 100F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(1862F, 15F);
            // 
            // panelObs
            // 
            this.panelObs.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.panelObs.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel16,
            this.labelSAC});
            this.panelObs.Dpi = 254F;
            this.panelObs.LocationFloat = new DevExpress.Utils.PointFloat(0F, 233F);
            this.panelObs.Name = "panelObs";
            this.panelObs.SizeF = new System.Drawing.SizeF(1860F, 973.6666F);
            this.panelObs.StylePriority.UseBorders = false;
            // 
            // xrLabel16
            // 
            this.xrLabel16.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel16.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel16.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "boletos.msgfrente")});
            this.xrLabel16.Dpi = 254F;
            this.xrLabel16.Font = new System.Drawing.Font("Tahoma", 8F);
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(17.99999F, 22.00002F);
            this.xrLabel16.Multiline = true;
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(1820F, 863.25F);
            this.xrLabel16.StylePriority.UseBorders = false;
            this.xrLabel16.Text = "xrLabel16";
            // 
            // labelSAC
            // 
            this.labelSAC.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.labelSAC.Dpi = 254F;
            this.labelSAC.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelSAC.LocationFloat = new DevExpress.Utils.PointFloat(21.00001F, 890.25F);
            this.labelSAC.Multiline = true;
            this.labelSAC.Name = "labelSAC";
            this.labelSAC.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.labelSAC.SizeF = new System.Drawing.SizeF(1820F, 77.41663F);
            this.labelSAC.StylePriority.UseBorders = false;
            this.labelSAC.StylePriority.UseFont = false;
            this.labelSAC.StylePriority.UseTextAlignment = false;
            this.labelSAC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // nomeRel
            // 
            this.nomeRel.Dpi = 254F;
            this.nomeRel.Font = new System.Drawing.Font("Tahoma", 8F);
            this.nomeRel.LocationFloat = new DevExpress.Utils.PointFloat(1643F, 2850.063F);
            this.nomeRel.Name = "nomeRel";
            this.nomeRel.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.nomeRel.SizeF = new System.Drawing.SizeF(200F, 30F);
            this.nomeRel.StylePriority.UseFont = false;
            this.nomeRel.Text = "boleto_envelope";
            this.nomeRel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.nomeRel.Visible = false;
            this.nomeRel.WordWrap = false;
            // 
            // xrPanel8
            // 
            this.xrPanel8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel8,
            this.xrLabel1});
            this.xrPanel8.Dpi = 254F;
            this.xrPanel8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPanel8.Name = "xrPanel8";
            this.xrPanel8.SizeF = new System.Drawing.SizeF(1860F, 212F);
            this.xrPanel8.StylePriority.UseBorders = false;
            // 
            // xrLabel8
            // 
            this.xrLabel8.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel8.Dpi = 254F;
            this.xrLabel8.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(21.00001F, 106F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(1439F, 37F);
            this.xrLabel8.StylePriority.UseBorders = false;
            this.xrLabel8.StylePriority.UsePadding = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "DOIS LAJEADOS - 99220000 - RS";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrLabel8.WordWrap = false;
            // 
            // xrLabel1
            // 
            this.xrLabel1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(21.00001F, 68.99999F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(1439F, 37F);
            this.xrLabel1.StylePriority.UseBorders = false;
            this.xrLabel1.StylePriority.UsePadding = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "RUA RAIMUNDO CORREA, 1020 - SALA 02 - CENTRO";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrLabel1.WordWrap = false;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 41F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 30F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // Boleto
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.Dpi = 254F;
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margins = new System.Drawing.Printing.Margins(124, 109, 41, 30);
            this.PageHeight = 2970;
            this.PageWidth = 2100;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.ShowPrintMarginsWarning = false;
            this.SnapGridSize = 25F;
            this.Version = "17.1";
            this.VerticalContentSplitting = DevExpress.XtraPrinting.VerticalContentSplitting.Smart;
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRPageBreak xrPageBreak2;
        private DevExpress.XtraReports.UI.XRPageBreak xrPageBreak1;
        private DevExpress.XtraReports.UI.XRLine xrLine36;
        private DevExpress.XtraReports.UI.XRBarCode bcCodBarras;
        private DevExpress.XtraReports.UI.XRPanel panelBoleto;
        private DevExpress.XtraReports.UI.XRLabel label7;
        private DevExpress.XtraReports.UI.XRLine xrLine52;
        private DevExpress.XtraReports.UI.XRLine xrLine51;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLine xrLine50;
        private DevExpress.XtraReports.UI.XRLabel labelAgCodCed2;
        private DevExpress.XtraReports.UI.XRLabel labelSacCidUF2;
        private DevExpress.XtraReports.UI.XRLine xrLine40;
        private DevExpress.XtraReports.UI.XRLine xrLine39;
        private DevExpress.XtraReports.UI.XRLabel labelBanco2;
        private DevExpress.XtraReports.UI.XRPictureBox pictLogoBanco2;
        private DevExpress.XtraReports.UI.XRLabel labelLinhaDigitavel2;
        private DevExpress.XtraReports.UI.XRLabel labelDataPgto1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel67;
        private DevExpress.XtraReports.UI.XRLabel xrLabel65;
        private DevExpress.XtraReports.UI.XRLabel xrLabel64;
        private DevExpress.XtraReports.UI.XRLabel xrLabel63;
        private DevExpress.XtraReports.UI.XRLabel labelSacCNPJ2;
        private DevExpress.XtraReports.UI.XRLabel labelSacEnd2;
        private DevExpress.XtraReports.UI.XRLabel labelSacNome2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel59;
        private DevExpress.XtraReports.UI.XRLabel xrLabel58;
        private DevExpress.XtraReports.UI.XRLabel xrLabel57;
        private DevExpress.XtraReports.UI.XRLabel xrLabel56;
        private DevExpress.XtraReports.UI.XRLabel xrLabel55;
        private DevExpress.XtraReports.UI.XRLabel xrLabel54;
        private DevExpress.XtraReports.UI.XRLine xrLine35;
        private DevExpress.XtraReports.UI.XRLine xrLine34;
        private DevExpress.XtraReports.UI.XRLine xrLine33;
        private DevExpress.XtraReports.UI.XRLine xrLine32;
        private DevExpress.XtraReports.UI.XRLabel labelValDoc2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel52;
        private DevExpress.XtraReports.UI.XRLabel labelNossoNum2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel50;
        private DevExpress.XtraReports.UI.XRLabel xrLabel49;
        private DevExpress.XtraReports.UI.XRLabel xrLabel48;
        private DevExpress.XtraReports.UI.XRLabel xrLabel47;
        private DevExpress.XtraReports.UI.XRLabel labelEspecie2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel45;
        private DevExpress.XtraReports.UI.XRLabel xrLabel43;
        private DevExpress.XtraReports.UI.XRLine xrLine31;
        private DevExpress.XtraReports.UI.XRLine xrLine30;
        private DevExpress.XtraReports.UI.XRPageInfo labelDataProces2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel44;
        private DevExpress.XtraReports.UI.XRLabel labelAceite2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel41;
        private DevExpress.XtraReports.UI.XRLabel xrLabel40;
        private DevExpress.XtraReports.UI.XRLabel labelEspDoc2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel labelNumDoc2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRLabel labelDataDoc2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel labelVenc2;
        private DevExpress.XtraReports.UI.XRLabel labelCedCNPJ1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel labelCedNome2;
        private DevExpress.XtraReports.UI.XRLine xrLine29;
        private DevExpress.XtraReports.UI.XRLine xrLine28;
        private DevExpress.XtraReports.UI.XRLine xrLine27;
        private DevExpress.XtraReports.UI.XRLine xrLine26;
        private DevExpress.XtraReports.UI.XRLine xrLine25;
        private DevExpress.XtraReports.UI.XRLine xrLine24;
        private DevExpress.XtraReports.UI.XRLine xrLine14;
        private DevExpress.XtraReports.UI.XRLine xrLine13;
        private DevExpress.XtraReports.UI.XRLine xrLine10;
        private DevExpress.XtraReports.UI.XRLine xrLine9;
        private DevExpress.XtraReports.UI.XRLine xrLine8;
        private DevExpress.XtraReports.UI.XRLine xrLine7;
        private DevExpress.XtraReports.UI.XRLine xrLine6;
        private DevExpress.XtraReports.UI.XRLine xrLine5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRPanel panelRecibo;
        private DevExpress.XtraReports.UI.XRLabel label6;
        private DevExpress.XtraReports.UI.XRLabel label4;
        private DevExpress.XtraReports.UI.XRLabel label5;
        private DevExpress.XtraReports.UI.XRLabel label3;
        private DevExpress.XtraReports.UI.XRLabel label1;
        private DevExpress.XtraReports.UI.XRLabel label2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLine xrLine38;
        private DevExpress.XtraReports.UI.XRLabel labelBanco1;
        private DevExpress.XtraReports.UI.XRLine xrLine37;
        private DevExpress.XtraReports.UI.XRPictureBox pictLogoBanco1;
        private DevExpress.XtraReports.UI.XRLine xrLine23;
        private DevExpress.XtraReports.UI.XRLabel labelEspecie1;
        private DevExpress.XtraReports.UI.XRLabel labelEspDoc1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLine xrLine22;
        private DevExpress.XtraReports.UI.XRLine xrLine21;
        private DevExpress.XtraReports.UI.XRLine xrLine20;
        private DevExpress.XtraReports.UI.XRLine xrLine19;
        private DevExpress.XtraReports.UI.XRLabel labelDataDoc1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel labelAceite1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLine xrLine18;
        private DevExpress.XtraReports.UI.XRLabel labelAgCodCed1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLine xrLine17;
        private DevExpress.XtraReports.UI.XRLabel labelCedNome1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel labelValDoc1;
        private DevExpress.XtraReports.UI.XRLabel labelNossoNum1;
        private DevExpress.XtraReports.UI.XRLabel labelVenc1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel labelNumDoc1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLine xrLine16;
        private DevExpress.XtraReports.UI.XRLine xrLine15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel labelSacNome1;
        private DevExpress.XtraReports.UI.XRLine xrLine12;
        private DevExpress.XtraReports.UI.XRLine xrLine11;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRPanel panelObs;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel labelSAC;
        private DevExpress.XtraReports.UI.XRLabel nomeRel;
        private DevExpress.XtraReports.UI.XRPanel xrPanel8;
        private DevExpress.XtraReports.UI.TopMarginBand topMarginBand1;
        private DevExpress.XtraReports.UI.BottomMarginBand bottomMarginBand1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
    }
}
