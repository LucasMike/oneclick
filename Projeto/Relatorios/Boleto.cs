﻿using DevExpress.XtraReports.UI;
using Projeto.Classes;
using System;
using System.Data;

namespace Projeto.Relatorios
{
    public partial class Boleto : DevExpress.XtraReports.UI.XtraReport
    {
        public Boleto(int iId)
        {
            InitializeComponent();

            DataSource = Consultar($@"select *
                                    from conta_receber c
                                    inner join associado a on (a.id = c.associado_id)
                                    inner join boleto b on (b.id = c.boleto_id)
                                    where c.id = {iId}");
            DataMember = "boleto";

            AjustarDados();
        }

        public DataSet Consultar(string sql)
        {
            DataSet ds = new DataSet();

            DataTable resultado = Conexao.Instancia.Consultar(sql);
            resultado.TableName = "boleto";
            ds.Tables.Add(resultado);
            return ds;
        }

        private void AjustarDados()
        {
            DataTable dataTable = ((DataSet)DataSource).Tables[0];
            dataTable.Columns.Add("linha_digitavel_bol");
            dataTable.Columns.Add("codigo_barras_bol");

            BancoBase b = BancoBase.ObterBancoBase(1);

            foreach (DataRow dr in dataTable.Rows)
            {
                string sCodBarras = b.Impressao_CodigoDeBarras(
                    dr["agencia"].ToString(), 
                    dr["nosso_numero"].ToString(),
                    dr["conta"].ToString(),
                    Convert.ToDecimal(dr["valor"]),
                    Convert.ToDateTime(dr["vencimento"]),
                    dr
                );
                dr["codigo_barras_bol"] = sCodBarras;
                dr["linha_digitavel_bol"] = b.Impressao_LinhaDigitavel(sCodBarras);
            }

        }

        public void SHowPreview()
        {
            using (ReportPrintTool printTool = new ReportPrintTool(this))
            {
                printTool.ShowPreviewDialog();
            }

        }
    }
}
