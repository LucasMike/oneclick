﻿namespace Projeto.Forms
{
    partial class FormPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPrincipal));
            this.rbMenu = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.btnCadUsuarios = new DevExpress.XtraBars.BarButtonItem();
            this.btnPermicoes = new DevExpress.XtraBars.BarButtonItem();
            this.skinRibbonGalleryBarItem2 = new DevExpress.XtraBars.SkinRibbonGalleryBarItem();
            this.btnCadAssociado = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonGalleryBarItem1 = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.btnCadTransportador = new DevExpress.XtraBars.BarButtonItem();
            this.btnCadCaixas = new DevExpress.XtraBars.BarButtonItem();
            this.btnCadCidade = new DevExpress.XtraBars.BarButtonItem();
            this.btnCadConfigBoletos = new DevExpress.XtraBars.BarButtonItem();
            this.btnContaReceber = new DevExpress.XtraBars.BarButtonItem();
            this.btnContaPagar = new DevExpress.XtraBars.BarButtonItem();
            this.btnCaixa = new DevExpress.XtraBars.BarButtonItem();
            this.btnCadDestino = new DevExpress.XtraBars.BarButtonItem();
            this.skinRibbonGalleryBarItem4 = new DevExpress.XtraBars.SkinRibbonGalleryBarItem();
            this.btnCadViagens = new DevExpress.XtraBars.BarButtonItem();
            this.rpCadastros = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup7 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup11 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpFinanceiro = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup9 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup10 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpUsuario = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rbUsuarios = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup8 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.skinRibbonGalleryBarItem1 = new DevExpress.XtraBars.SkinRibbonGalleryBarItem();
            this.xtcAbas = new DevExpress.XtraTab.XtraTabControl();
            this.skinRibbonGalleryBarItem3 = new DevExpress.XtraBars.SkinRibbonGalleryBarItem();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            ((System.ComponentModel.ISupportInitialize)(this.rbMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtcAbas)).BeginInit();
            this.SuspendLayout();
            // 
            // rbMenu
            // 
            this.rbMenu.DrawGroupCaptions = DevExpress.Utils.DefaultBoolean.False;
            this.rbMenu.DrawGroupsBorderMode = DevExpress.Utils.DefaultBoolean.False;
            this.rbMenu.ExpandCollapseItem.Id = 0;
            this.rbMenu.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.rbMenu.ExpandCollapseItem,
            this.btnCadUsuarios,
            this.btnPermicoes,
            this.skinRibbonGalleryBarItem2,
            this.btnCadAssociado,
            this.ribbonGalleryBarItem1,
            this.btnCadTransportador,
            this.btnCadCaixas,
            this.btnCadCidade,
            this.btnCadConfigBoletos,
            this.btnContaReceber,
            this.btnContaPagar,
            this.btnCaixa,
            this.btnCadDestino,
            this.skinRibbonGalleryBarItem4,
            this.btnCadViagens});
            this.rbMenu.Location = new System.Drawing.Point(0, 0);
            this.rbMenu.MaxItemId = 20;
            this.rbMenu.Name = "rbMenu";
            this.rbMenu.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpCadastros,
            this.rpFinanceiro,
            this.rpUsuario});
            this.rbMenu.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.rbMenu.Size = new System.Drawing.Size(820, 123);
            this.rbMenu.StatusBar = this.ribbonStatusBar;
            // 
            // btnCadUsuarios
            // 
            this.btnCadUsuarios.Caption = "Usuários";
            this.btnCadUsuarios.Id = 1;
            this.btnCadUsuarios.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCadUsuarios.ImageOptions.Image")));
            this.btnCadUsuarios.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnCadUsuarios.ImageOptions.LargeImage")));
            this.btnCadUsuarios.Name = "btnCadUsuarios";
            this.btnCadUsuarios.Tag = "usuario";
            this.btnCadUsuarios.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AoClicarBotaoMenu);
            // 
            // btnPermicoes
            // 
            this.btnPermicoes.Caption = "Permissões";
            this.btnPermicoes.Id = 2;
            this.btnPermicoes.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnPermicoes.ImageOptions.Image")));
            this.btnPermicoes.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnPermicoes.ImageOptions.LargeImage")));
            this.btnPermicoes.Name = "btnPermicoes";
            this.btnPermicoes.Tag = "permicoes";
            this.btnPermicoes.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AoClicarBotaoMenu);
            // 
            // skinRibbonGalleryBarItem2
            // 
            this.skinRibbonGalleryBarItem2.Caption = "Selecione um tema";
            this.skinRibbonGalleryBarItem2.Id = 5;
            this.skinRibbonGalleryBarItem2.Name = "skinRibbonGalleryBarItem2";
            // 
            // btnCadAssociado
            // 
            this.btnCadAssociado.Caption = "Associado";
            this.btnCadAssociado.Id = 7;
            this.btnCadAssociado.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCadAssociado.ImageOptions.Image")));
            this.btnCadAssociado.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnCadAssociado.ImageOptions.LargeImage")));
            this.btnCadAssociado.Name = "btnCadAssociado";
            this.btnCadAssociado.Tag = "associado";
            this.btnCadAssociado.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AoClicarBotaoMenu);
            // 
            // ribbonGalleryBarItem1
            // 
            this.ribbonGalleryBarItem1.Caption = "ribbonGalleryBarItem1";
            this.ribbonGalleryBarItem1.Id = 8;
            this.ribbonGalleryBarItem1.Name = "ribbonGalleryBarItem1";
            // 
            // btnCadTransportador
            // 
            this.btnCadTransportador.Caption = "Transportadora";
            this.btnCadTransportador.Id = 9;
            this.btnCadTransportador.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCadTransportador.ImageOptions.Image")));
            this.btnCadTransportador.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnCadTransportador.ImageOptions.LargeImage")));
            this.btnCadTransportador.Name = "btnCadTransportador";
            this.btnCadTransportador.Tag = "transportadora";
            this.btnCadTransportador.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AoClicarBotaoMenu);
            // 
            // btnCadCaixas
            // 
            this.btnCadCaixas.Caption = "Caixa";
            this.btnCadCaixas.Id = 10;
            this.btnCadCaixas.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCadCaixas.ImageOptions.Image")));
            this.btnCadCaixas.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnCadCaixas.ImageOptions.LargeImage")));
            this.btnCadCaixas.Name = "btnCadCaixas";
            this.btnCadCaixas.Tag = "caixa";
            this.btnCadCaixas.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AoClicarBotaoMenu);
            // 
            // btnCadCidade
            // 
            this.btnCadCidade.Caption = "Cidade";
            this.btnCadCidade.Id = 11;
            this.btnCadCidade.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCadCidade.ImageOptions.Image")));
            this.btnCadCidade.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnCadCidade.ImageOptions.LargeImage")));
            this.btnCadCidade.Name = "btnCadCidade";
            this.btnCadCidade.Tag = "cidade";
            this.btnCadCidade.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AoClicarBotaoMenu);
            // 
            // btnCadConfigBoletos
            // 
            this.btnCadConfigBoletos.Caption = "Config. Boletos";
            this.btnCadConfigBoletos.Id = 12;
            this.btnCadConfigBoletos.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCadConfigBoletos.ImageOptions.Image")));
            this.btnCadConfigBoletos.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnCadConfigBoletos.ImageOptions.LargeImage")));
            this.btnCadConfigBoletos.Name = "btnCadConfigBoletos";
            this.btnCadConfigBoletos.Tag = "boleto";
            this.btnCadConfigBoletos.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AoClicarBotaoMenu);
            // 
            // btnContaReceber
            // 
            this.btnContaReceber.Caption = "Conta a Receber";
            this.btnContaReceber.Id = 13;
            this.btnContaReceber.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnContaReceber.ImageOptions.LargeImage")));
            this.btnContaReceber.Name = "btnContaReceber";
            this.btnContaReceber.Tag = "conta_receber";
            this.btnContaReceber.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AoClicarBotaoMenu);
            // 
            // btnContaPagar
            // 
            this.btnContaPagar.Caption = "Conta a Pagar";
            this.btnContaPagar.Id = 14;
            this.btnContaPagar.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnContaPagar.ImageOptions.LargeImage")));
            this.btnContaPagar.Name = "btnContaPagar";
            this.btnContaPagar.Tag = "conta_pagar";
            this.btnContaPagar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AoClicarBotaoMenu);
            // 
            // btnCaixa
            // 
            this.btnCaixa.Caption = "Caixa";
            this.btnCaixa.Id = 15;
            this.btnCaixa.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCaixa.ImageOptions.Image")));
            this.btnCaixa.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnCaixa.ImageOptions.LargeImage")));
            this.btnCaixa.Name = "btnCaixa";
            this.btnCaixa.Tag = "caixa";
            this.btnCaixa.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AoClicarBotaoMenu);
            // 
            // btnCadDestino
            // 
            this.btnCadDestino.Caption = "Destino";
            this.btnCadDestino.Id = 16;
            this.btnCadDestino.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCadDestino.ImageOptions.Image")));
            this.btnCadDestino.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnCadDestino.ImageOptions.LargeImage")));
            this.btnCadDestino.Name = "btnCadDestino";
            this.btnCadDestino.Tag = "destino";
            this.btnCadDestino.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AoClicarBotaoMenu);
            // 
            // skinRibbonGalleryBarItem4
            // 
            this.skinRibbonGalleryBarItem4.Caption = "skinRibbonGalleryBarItem4";
            this.skinRibbonGalleryBarItem4.Id = 18;
            this.skinRibbonGalleryBarItem4.Name = "skinRibbonGalleryBarItem4";
            // 
            // btnCadViagens
            // 
            this.btnCadViagens.Caption = "Viagens";
            this.btnCadViagens.Id = 19;
            this.btnCadViagens.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCadViagens.ImageOptions.Image")));
            this.btnCadViagens.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnCadViagens.ImageOptions.LargeImage")));
            this.btnCadViagens.Name = "btnCadViagens";
            this.btnCadViagens.Tag = "destino_transportadora";
            this.btnCadViagens.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AoClicarBotaoMenu);
            // 
            // rpCadastros
            // 
            this.rpCadastros.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup2,
            this.ribbonPageGroup6,
            this.ribbonPageGroup5,
            this.ribbonPageGroup4,
            this.ribbonPageGroup7,
            this.ribbonPageGroup11});
            this.rpCadastros.KeyTip = " ";
            this.rpCadastros.Name = "rpCadastros";
            this.rpCadastros.Text = "Cadastros";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.btnCadAssociado);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "ribbonPageGroup2";
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.ItemLinks.Add(this.btnCadCidade);
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            this.ribbonPageGroup6.Text = "ribbonPageGroup6";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.btnCadDestino);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.Text = "ribbonPageGroup5";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.Glyph = ((System.Drawing.Image)(resources.GetObject("ribbonPageGroup4.Glyph")));
            this.ribbonPageGroup4.ItemLinks.Add(this.btnCadTransportador);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.Text = "ribbonPageGroup4";
            // 
            // ribbonPageGroup7
            // 
            this.ribbonPageGroup7.ItemLinks.Add(this.btnCadConfigBoletos);
            this.ribbonPageGroup7.Name = "ribbonPageGroup7";
            this.ribbonPageGroup7.Text = "ribbonPageGroup7";
            // 
            // ribbonPageGroup11
            // 
            this.ribbonPageGroup11.ItemLinks.Add(this.btnCadViagens);
            this.ribbonPageGroup11.Name = "ribbonPageGroup11";
            this.ribbonPageGroup11.Text = "ribbonPageGroup11";
            // 
            // rpFinanceiro
            // 
            this.rpFinanceiro.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup3,
            this.ribbonPageGroup9,
            this.ribbonPageGroup10});
            this.rpFinanceiro.Name = "rpFinanceiro";
            this.rpFinanceiro.Text = "Financeiro";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.btnContaReceber);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "ribbonPageGroup3";
            // 
            // ribbonPageGroup9
            // 
            this.ribbonPageGroup9.ItemLinks.Add(this.btnContaPagar);
            this.ribbonPageGroup9.Name = "ribbonPageGroup9";
            this.ribbonPageGroup9.Text = "ribbonPageGroup9";
            // 
            // ribbonPageGroup10
            // 
            this.ribbonPageGroup10.ItemLinks.Add(this.btnCaixa);
            this.ribbonPageGroup10.Name = "ribbonPageGroup10";
            this.ribbonPageGroup10.Text = "ribbonPageGroup10";
            // 
            // rpUsuario
            // 
            this.rpUsuario.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rbUsuarios,
            this.ribbonPageGroup8});
            this.rpUsuario.Name = "rpUsuario";
            this.rpUsuario.Text = "Opções";
            // 
            // rbUsuarios
            // 
            this.rbUsuarios.Glyph = ((System.Drawing.Image)(resources.GetObject("rbUsuarios.Glyph")));
            this.rbUsuarios.ItemLinks.Add(this.btnCadUsuarios);
            this.rbUsuarios.Name = "rbUsuarios";
            this.rbUsuarios.Text = "Usuarios";
            // 
            // ribbonPageGroup8
            // 
            this.ribbonPageGroup8.ItemLinks.Add(this.skinRibbonGalleryBarItem4);
            this.ribbonPageGroup8.Name = "ribbonPageGroup8";
            this.ribbonPageGroup8.Text = "ribbonPageGroup8";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 477);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.rbMenu;
            this.ribbonStatusBar.Size = new System.Drawing.Size(820, 31);
            // 
            // skinRibbonGalleryBarItem1
            // 
            this.skinRibbonGalleryBarItem1.Caption = "Selecione o Tema";
            this.skinRibbonGalleryBarItem1.Id = 4;
            this.skinRibbonGalleryBarItem1.Name = "skinRibbonGalleryBarItem1";
            // 
            // xtcAbas
            // 
            this.xtcAbas.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InAllTabPageHeaders;
            this.xtcAbas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtcAbas.Location = new System.Drawing.Point(0, 123);
            this.xtcAbas.Name = "xtcAbas";
            this.xtcAbas.Size = new System.Drawing.Size(820, 354);
            this.xtcAbas.TabIndex = 2;
            this.xtcAbas.CloseButtonClick += new System.EventHandler(this.xtcAbas_CloseButtonClick);
            // 
            // skinRibbonGalleryBarItem3
            // 
            this.skinRibbonGalleryBarItem3.Caption = "Selecione um tema";
            this.skinRibbonGalleryBarItem3.Id = 5;
            this.skinRibbonGalleryBarItem3.Name = "skinRibbonGalleryBarItem3";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.skinRibbonGalleryBarItem2);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Tema";
            // 
            // FormPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(820, 508);
            this.Controls.Add(this.xtcAbas);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.rbMenu);
            this.FormBorderEffect = DevExpress.XtraEditors.FormBorderEffect.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(480, 320);
            this.Name = "FormPrincipal";
            this.Ribbon = this.rbMenu;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "OneClick";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.rbMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtcAbas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl rbMenu;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpUsuario;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rbUsuarios;
        private DevExpress.XtraBars.BarButtonItem btnCadUsuarios;
        private DevExpress.XtraBars.BarButtonItem btnPermicoes;
        private DevExpress.XtraBars.SkinRibbonGalleryBarItem skinRibbonGalleryBarItem1;
        private DevExpress.XtraBars.SkinRibbonGalleryBarItem skinRibbonGalleryBarItem2;
        private DevExpress.XtraTab.XtraTabControl xtcAbas;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpCadastros;
        private DevExpress.XtraBars.BarButtonItem btnCadAssociado;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.RibbonGalleryBarItem ribbonGalleryBarItem1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpFinanceiro;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.BarButtonItem btnCadTransportador;
        private DevExpress.XtraBars.BarButtonItem btnCadCaixas;
        private DevExpress.XtraBars.BarButtonItem btnCadCidade;
        private DevExpress.XtraBars.BarButtonItem btnCadConfigBoletos;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup7;
        private DevExpress.XtraBars.BarButtonItem btnContaReceber;
        private DevExpress.XtraBars.BarButtonItem btnContaPagar;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup9;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup8;
        private DevExpress.XtraBars.SkinRibbonGalleryBarItem skinRibbonGalleryBarItem3;
        private DevExpress.XtraBars.BarButtonItem btnCaixa;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup10;
        private DevExpress.XtraBars.BarButtonItem btnCadDestino;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraBars.SkinRibbonGalleryBarItem skinRibbonGalleryBarItem4;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.BarButtonItem btnCadViagens;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup11;
    }
}