﻿using DevExpress.XtraEditors;
using System;
using System.Windows.Forms;

namespace Projeto.Forms
{
    public partial class FrmParametrosGerarContas : XtraForm
    {
        public DateTime DataInicio
        {
            get
            {
                return deInicio.DateTime;
            }
        }


        public DateTime DataFim
        {
            get
            {
                return deFim.DateTime;
            }
        }

        public DateTime DataVencimento
        {
            get
            {
                return deVencimento.DateTime;
            }
        }

        public FrmParametrosGerarContas()
        {
            InitializeComponent();

            DialogResult = DialogResult.Cancel;
        }

        private void bbiConfirmar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void bbiCancelar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }
    }
}
