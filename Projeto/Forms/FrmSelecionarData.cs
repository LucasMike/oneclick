﻿using DevExpress.XtraEditors;
using System;
using System.Windows.Forms;

namespace Projeto.Forms
{
    public partial class FrmSelecionarData : XtraForm
    {
        public DateTime DataInicio
        {
            get
            {
                return deInicio.DateTime;
            }
        }


        public DateTime DataFim
        {
            get
            {
                return deFim.DateTime;
            }
        }

        public FrmSelecionarData()
        {
            InitializeComponent();

            DialogResult = DialogResult.Cancel;
        }

        private void bbiConfirmar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void bbiCancelar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }
    }
}
