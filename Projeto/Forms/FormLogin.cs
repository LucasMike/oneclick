﻿using Projeto.Classes;
using System;
using System.Windows.Forms;

namespace Projeto.Forms
{
    public partial class FormLogin : Form
    {

        public FormLogin()
        {
            InitializeComponent();
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            if (Conexao.Instancia.Consultar(
             $@"select id
                from usuario
                where usuario = '{teusuario.Text}'
                and senha = '{tesenha.Text}'").Rows.Count == 1)
            { 
                FormPrincipal frm = new FormPrincipal();
                Hide();
                frm.ShowDialog();
                Close();
            }
            else
            {
                MessageBox.Show("Usuário ou senha inválido!");
            }
        }

        private void teusuario_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (teusuario.Text.Length > 0 && tesenha.Text.Length > 0)
                {
                    button1.PerformClick();
                }
                else if (teusuario.Text.Length > 0 && sender == teusuario)
                {
                    tesenha.Focus();
                }
                else if (tesenha.Text.Length > 0 && sender == tesenha)
                {
                    teusuario.Focus();
                }
            }
        }
    }
}
