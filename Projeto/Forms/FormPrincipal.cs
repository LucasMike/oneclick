﻿using DevExpress.DataAccess.Sql;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraNavBar;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using Projeto.Controles.Views;
using System.Drawing;
using System.Windows.Forms;

namespace Projeto.Forms
{
    public partial class FormPrincipal : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        #region Construtor

        public FormPrincipal()
        {
            InitializeComponent();
        }

        #endregion

        #region Eventos Controles

        private void AoClicarBotaoMenu(object sender, ItemClickEventArgs e)
        {
            string sTabela = e.Item.Tag.ToString();
            ViewBase vb = ViewBase.ObterViewReferenteTabela(sTabela);

            XtraTabPage tabPage = new XtraTabPage();
            tabPage.Text = vb.Titulo;
            xtcAbas.TabPages.Add(tabPage);
            tabPage.Controls.Add(vb);
            xtcAbas.SelectedTabPageIndex = xtcAbas.TabPages.Count - 1;
        }

        private void xtcAbas_CloseButtonClick(object sender, System.EventArgs e)
        {
            ClosePageButtonEventArgs arg = e as ClosePageButtonEventArgs;
            XtraTabPage page = (XtraTabPage)arg.Page;
            page.Controls[0].Dispose();
            xtcAbas.TabPages.Remove(page);
            page.Dispose();
        }

        #endregion

        #region Métodos Privados

        private void CriarNovaAba(string tabela)
        {
            
        }

        #endregion
    }
}